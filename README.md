# Practical guide to loop integration

*Practical guide to loop integration* is a short block course I have developed for the [University of Durham in 2022](https://yannickulrich.com/loop-integration/2022) and extended for the University of Bern in 2024.

Contact: [Yannick Ulrich](mailto:yannick.ulrich@cern.ch)

Please report mistakes as [issues](https://gitlab.com/yannickulrich/loop-integration/-/issues) or per email.

## Abstract

Loop integration is a vital part of any higher order calculation.
However, practical tools to actually compute these integrals are rarely covered in lectures.
In this course, I will cover some advanced tools that have been used in a number of multi-scale multi-loop calculations.

After introducing the problem, I will discuss integration-by-parts reduction to reduce the number of integrals.
Next, I will discuss the method of regions to reduce the number of scales of the integrals.
Finally, I will discuss the method of differential equations and in particular Auxilary Mass Flow to actually calculate the integrals.

The course will be composed of lectures introducing the techniques and practical, hands-on example at the two-loop level.

## Schedule

The course will take place every Tuesday from 16 April 2024 to 28 May 2024 at 14:00 CEST -- 16:00 CEST in [ExWi 119](https://ksl.unibe.ch/KSL/raumInfo?0&uni=Universit%C3%A4t+Bern&hrz=Exakte+Wissenschaften&gebaeude=Exakte+Wissenschaften,+ExWi&raumnummer=119) of the [University of Bern](https://maps.app.goo.gl/1jCnMaRjTt25GF6E9).

**Note:** Due to a seminar at University of Bern, we will swap the content of Lecture 3 and Lecture 4.

## Course material
 * Lecture notes: [here](https://yannickulrich.gitlab.io/loop-integration/notes.pdf)
 * Interactive plot: [here](https://yannickulrich.gitlab.io/loop-integration/parameter.html)
 * Code examples: [here](https://gitlab.com/yannickulrich/loop-integration/-/tree/root/code)
 * Zoom connection: [680 9294 3828, passcode: 856877](https://zoom.us/j/68092943828?pwd=bjdVNnppbzVEaFBVK1YrbTRtbkowQT09)
 * STL files: [stand](code/3dmodel-axes.stl), [inner part](code/3dmodel-inner.stl), [lower part](code/3dmodel-lower.stl), [upper part](code/3dmodel-upper.stl)

<table>
    <thead>
        <tr>
            <th>Date</th>
            <th>Topic</th>
            <th>Problem sheet</th>
            <th>Recordings</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>16 April</td>
            <td>Basics of loop integration</td>
            <td>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sheet1.pdf">Problems</a><br/>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sol1.pdf">Solutions</a>
            </td>
            <td>
                <a href="https://zoom.us/rec/share/vVXA8pIhC21T_WFzeCmTAbyj8cPj7F1jejxoNQY3wgah1geYzT9-O2qB8nR01y1n.TI0bpVeos-R5OURO">Recording</a>
            </td>
        </tr>
        <tr>
            <td>23 April</td>
            <td>Integration-by-parts relations</td>
            <td>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sheet2.pdf">Problems</a><br/>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sol2.pdf">Solutions</a>
            </td>
            <td>
                No recording, refer to 2022
            </td>
        </tr>
        <tr>
            <td>30 April</td>
            <td>The Method of Regions</td>
            <td>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sheet4.pdf">Problems</a><br/>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sol4.pdf">Solutions</a><br/>
            </td>
            <td>
                <a href="https://unibe-ch.zoom.us/rec/share/-1koO0Mx7ezVCics0e_NqCSpSqbV59KvssHsyV6UXYr8Cf4TGZvrDTdfhCZya6nd.xViz9Wzi2XS44F8o">Recording</a>
            </td>
        </tr>
        <tr>
            <td>7 May</td>
            <td>Differential equations</td>
            <td>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sheet3.pdf">Problems</a><br/>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sol3.pdf">Solutions</a><br/>
            </td>
            <td>
                <a href="https://unibe-ch.zoom.us/rec/share/m1hEYRL5qCw1739TuQ7vOvwwgpfTIGe1v8vgzMmLF9CVltDbOKJmcWuEhLfsXPii.AEl90MWvmFfe56Rw">Recording</a>
            </td>
        </tr>
        <tr>
            <td>14 May</td>
            <td>Mellin Barnes integration</td>
            <td>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sheet5.pdf">Problems</a><br/>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sol5.pdf">Solutions</a><br/>
            </td>
            <td>
                <a href="https://unibe-ch.zoom.us/rec/share/GNDeobfPh0KRUzXlvOLYjAoRN8-BnrVgxzD1rrVDow4MBIOlCXRJ6FfGZ6O9VQGC.61dfbCpTDodVs-Rz">Recording</a>
            </td>
        </tr>
        <tr>
            <td>21 May</td>
            <td>Series expansion of loop integrals</td>
            <td>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sheet6.pdf">Problems</a><br/>
                <a href="https://yannickulrich.gitlab.io/loop-integration/sol6.pdf">Solutions</a><br/>
            </td>
            <td>
                <a href="https://unibe-ch.zoom.us/rec/share/4ZAo5blEkC87zl_8O8TVZop7OYxbP_fKotgq-bXV6m_0pgeJOhqPpLKH4XwE2BQs.IY-2KrUo8a2KTUEE">Recording</a>
            </td>
        </tr>
        <tr>
            <td>28 May</td>
            <td>Auxiliary Mass Flow</td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>


## Course material from 2022
 * Lecture notes: [here](https://yannickulrich.com/loop-integration/2022/notes.pdf)
 * Interactive plot: [here](https://yannickulrich.com/loop-integration/2022/parameter.html)
 * Code examples: [here](https://gitlab.com/yannickulrich/loop-integration/-/tree/2022/code)

<table>
    <thead>
        <tr>
            <th>Date</th>
            <th>Topic</th>
            <th>Problem sheet</th>
            <th>Recordings</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>13 May</td>
            <td>Basics of loop integration</td>
            <td>
                <a href="https://yannickulrich.com/loop-integration/2022/sheet1.pdf">Problems</a><br/>
                <a href="https://yannickulrich.com/loop-integration/2022/sol1.pdf">Solutions</a>
            </td>
            <td>
                <a href="https://durham.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=ea182aa6-6813-4672-afd2-ae9400c00a08">Lecture</a>
            </td>
        </tr>
        <tr>
            <td>16 May</td>
            <td>Integration-by-parts relations</td>
            <td>
                <a href="https://yannickulrich.com/loop-integration/2022/sheet2.pdf">Problems</a>
                <a href="https://yannickulrich.com/loop-integration/2022/sol2.pdf">Solutions</a>
            </td>
            <td>
                <a href="https://durham.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=4fd2cda7-b916-4490-b97e-ae9700bf0649">Lecture</a>
            </td>
        </tr>
        <tr>
            <td>18 May</td>
            <td>The method of region</td>
            <td>
                <a href="https://yannickulrich.com/loop-integration/2022/sheet3.pdf">Problems</a>
                <a href="https://yannickulrich.com/loop-integration/2022/sol3.pdf">Solutions</a>
            </td>
            <td>
                <a href="https://durham.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=ff006d76-f165-42a3-9cb0-ae9900bd839c">Lecture</a>
            </td>
        </tr>
        <tr>
            <td>20 May</td>
            <td>Mellin-Barnes</td>
            <td>
                <a href="https://yannickulrich.com/loop-integration/2022/sheet4.pdf">Problems</a>
                <a href="https://yannickulrich.com/loop-integration/2022/sol4.pdf">Solutions</a>
            </td>
            <td>
                <a href="https://durham.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=a417d751-3adf-4ff7-bca1-ae9b00bed377">Lecture</a>
            </td>
        </tr>
    </tbody>
</table>

Most problems can be solved with pen and paper.
However, participants are encouraged to acquaint themselves with tools like Mathematica to avoid long manual calculations.


All material is licenced under [CC BY 4.0](https://gitlab.com/yannickulrich/loop-integration/-/blob/root/LICENCE).
You can find the source code and supplemental material [here](https://gitlab.com/yannickulrich/loop-integration).

## Further reading

 * S. Weinzierl: *Feynman Integrals*, [[2201.03593]](https://arxiv.org/abs/2201.03593)
 * V. A. Smirnov: *Analytic tools for Feynman integrals*, [*Springer* (2012)](https://doi.org/10.1007/978-3-642-34886-0)
 * V. A. Smirnov: *Feynman integral calculus*, [*Springer* (2006)](https://doi.org/10.1007/3-540-30611-0)
 * D. Urwyler: *Mellin-Barnes for two-loop integrals*, [University of Zurich (Bachelor thesis) (2019)](https://mule-tools.gitlab.io/__static/DU-bsc.pdf)
 * T. Engel: *Two-loop corrections to the muon decay*, [ETH Zurich (Master thesis) (2018)](https://mule-tools.gitlab.io/__static/TE-msc.pdf)
 * S. Borowka: *Evaluation of multi-loop multi-scale integrals and phenomenological two-loop applications*, [TU Munich (PhD thesis) (2014)](https://arxiv.org/abs/1410.7939)
