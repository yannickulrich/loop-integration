with import <nixpkgs> {};
let
  bibgen = stdenvNoCC.mkDerivation {
    pname = "bibgen";
    version = "latest";
    src = pkgs.fetchurl {
        url = "https://gitlab.com/yannickulrich/latex/-/raw/docker/image/bibgen.sh?ref_type=heads";
        sha256 = "sha256-DYLWxNBBOrd8RfOvvV5wlatm2moTchxz3JlgjG4kKGI=";
    };
    outputs = [ "out" ];
    phases = [ "installPhase" ];
    installPhase = "
        mkdir -p $out/bin
        install -m755 $src $out/bin/bibgen.sh
    ";

  };
  jhep = stdenvNoCC.mkDerivation {
    pname = "jhepbst";
    version = "2.18";
    src = pkgs.fetchurl {
        url = "https://jhep.sissa.it/jhep/help/JHEP/TeXclass/DOCS/JHEP.bst";
        sha256 = "sha256-fZRTcmW6EYX1jYWwUtkrrIVaB0Dd3bzb4zhiHMRkn0o=";
    };
    outputs = [ "out" "tex" ];
    passthru.tlDeps = with texlive; [ latex ];
    phases = [ "installPhase" ];
    unpackPhase = '''';
    installPhase = "
        mkdir -p $out
        mkdir -p $tex/bibtex/bst
        cp $src $tex/bibtex/bst/JHEP.bst
    ";
  };
  tex = (pkgs.texliveSmall.withPackages(
    ps: with ps; [
        cm-super
        preprint
        xstring
        amsmath
        jhep
        substr
        enumitem
        xifthen
        framed
        standalone
  ]));
in
stdenv.mkDerivation {
  name = "";
  buildInputs = [
    bibgen
    tex
    discount
    openscad
  ];
}
