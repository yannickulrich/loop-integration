 %PDFLATEX
\documentclass{homework}
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath}
\sheetnr{3}
\lecturer{Dr~Y.~Ulrich}
\lecture{Practical guide to loop integration}
\term{Summer Term 2024}
\website{https://yannickulrich.com/loop-integration}
\logo{bern.pdf}
\issued{30 April 2024}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing,positioning,decorations.pathreplacing,shapes,calc}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }
\tikzset{
    photon/.style={decorate, decoration={snake,amplitude=1pt,segment length=6pt}}
}

\usepackage{listings}
\lstset{ %
  basicstyle=\ttfamily,
  keywordstyle=\color{Aquamarine4},
  numberstyle=\color{DarkOrange},
  stringstyle=\color{SkyBlue4},
  commentstyle=\color{Blue2},
  morecomment=[l]{!\ },% Comment only with space after !
  escapeinside={(*@}{@*)},
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}
}
\lstdefinestyle{yaml}{
  basicstyle=\color{Aquamarine4}\ttfamily,
  rulecolor=\color{black},
  string=[s]{"}{"},
  stringstyle=\color{SkyBlue4},
  comment=[l]{:},
  commentstyle=\color{black},
  morecomment=[l]{-}
}

\newcommand\D{\mathrm{d}}
\newcommand\U{\mathcal{U}}
\newcommand\F{\mathcal{F}}

\begin{document}
\maketitle
\def\base{
    \coordinate (a0) at (-.75,0);
    \coordinate (a1) at (+.75,0);
    \coordinate (b1) at (0,+.75);
    \coordinate (b2) at (0,-.75);
    \draw[photon] (0,0) circle(.75);
    \draw (a0) -- (a1);
}

\exercise{Vacuum sunset}

Calculate the vacuum sunset integral
\begin{align}
I(a_1, a_2, a_3) =
\begin{gathered}
\begin{tikzpicture}[baseline=+1cm]
\base
\node at (b1) [above] {$a_1$};
\node at (0,0) [below] {$a_3$};
\node at (b2) [below] {$a_2$};
\end{tikzpicture}
\end{gathered}
= \int[\D k_1][\D k_2]\frac{1}
    {\big[k_1^2\big]^{a_1}
     \big[k_2^2\big]^{a_2}
     \big[(k_1-k_2)^2 - m^2\big]^{a_3}}
\end{align}
for the cases $I(1,2,1)$ and $I(1,1,2)$
Why is this an easy integral to do?

\begin{solution}
    We can easily cut-construct the integral.
    The $\U$ term is all 2-cuts, i.e. $\U = x_1x_2 + x_2x_3 + x_1x_3$.
    The this is a vacuum integral, the momentum transfer across the 3-cut vanishes and we are left with $\F=m^2\ x_3\ \U$.
    Hence,
    \begin{align}
        \begin{split}
            I(a_1,a_2,a_3) &=
            \Gamma (1-\epsilon)^2
            (-1)^{a_1+a_2+a_3}
            (m^2)^{-a_1-a_2-a_3+d}
            \frac{\Gamma(-d+a_1+a_2+a_3)}{\Gamma (a_1) \Gamma (a_2) \Gamma  (a_3)}
            \\&\qquad\qquad\times
            \int\D\vec x
                x_1^{a_1-1}
                x_2^{a_2-1}
                x_3^{-a_1-a_2+d-1}
                \U^{-d/2}\,.
        \end{split}
    \end{align}
    We can calculate the integral trivially
    \begin{align}
        \int\D\vec x
            x_1^{a_1-1}
            x_2^{a_2-1}
            x_3^{-a_1-a_2+d-1}
            \U^{-d/2}
        = \frac{\Gamma\big(\frac{d}{2}-a_1\big) \Gamma\big(\frac{d}{2}-a_2\big) \Gamma\big(-\frac{d}{2}+a_1+a_2\big)}{\Gamma\big(\frac{d}{2}\big)}\,.
    \end{align}
    Hence,
    \begin{align}
        I(1,2,1) &= (m^2)^{-2\epsilon}\frac{\Gamma(1-\epsilon)^3\Gamma(-\epsilon )\Gamma(2\epsilon) \Gamma (\epsilon +1)}{\Gamma (2-\epsilon )}\,,\\
        I(1,1,2) &= (m^2)^{-2\epsilon}\frac{\Gamma(1-\epsilon)^4\Gamma( \epsilon )\Gamma(2\epsilon)}{\Gamma(2-\epsilon )}\,.
    \end{align}
    This integral is very simple because it only has one scale, $m^2$.
    Since we could find the exponent of $m^2$ by dimensional analysis, the integral really is a function of $\epsilon$ and nothing else.
    The fact that it is a vacuum integral further simplifies matters as $\F\propto\U$.
\end{solution}

\exercise{Off-shell sunset}

Now consider the integral with an external leg
\begin{align}
I(a_1, a_2, a_3) =
\begin{gathered}
\begin{tikzpicture}[baseline=+1cm]
\base
\draw (-1.5, 0) -- (a0);
\draw (1.5, 0) -- (a1);
\node at (b1) [above] {$a_1$};
\node at (0,0) [below] {$a_3$};
\node at (b2) [below] {$a_2$};
\end{tikzpicture}
\end{gathered}
= \int[\D k_1][\D k_2]\frac{1}
    {\big[k_1^2\big]^{a_1}
     \big[k_2^2\big]^{a_2}
     \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}}\,,
\end{align}
with $p^2=s=x\ m^2$.

\begin{subtasks}
    \task
    Why can we not use the same calculation from the previous problem?

    \task
    Show that this problem is solved by using the master integrals $\vec I=(I(1,2,1), I(1,1,2))^T$.
    These integrals are both starting at $\epsilon^{-2}$.

    \task
    Derive the differential equation matrices and show that
    \begin{align}
        A_s = \begin{pmatrix}
             -\frac{(d-3) m^2+(d-4) s}{s (m^2-s)} & -\frac{(d-3) m^2}{s (m^2-s)} \\
             \frac{d-4}{2 s} & \frac{d-4}{2 s}
        \end{pmatrix}
        \qquad\text{and}\qquad
        A_{m^2} = \begin{pmatrix}
             \frac{2 d-7}{m^2-s} & \frac{d-3}{m^2-s} \\
             -\frac{d-4}{2 m^2} & \frac{d-4}{2 m^2}
        \end{pmatrix}\,.
    \end{align}

    \task
    Find the mass scaling of each integral.
    Then rescale each integral such that it is finite and has vanishing mass scaling.
    Is the resulting system canonical?
    \turnpage

    \task
    Show that the matrix $T$ achieves a canonical basis.
    \begin{align}
        T = \frac{(m^2)^{-2\epsilon}}{\epsilon^2\ x}\Bigg[
                \begin{pmatrix}-1&   1-x \\       x& 0\end{pmatrix}
                +\epsilon
                \begin{pmatrix} 2&-2(1-x)\\-1 - 3 x& 1 - x\end{pmatrix}
            \Bigg]\,.
    \end{align}
    
    \task
    Show that the boundary conditions $\vec J_0$ of the canonical system $\vec J = T^{-1}\vec I$ are
    \begin{align}
        J_{0,1} = J_{0,2} = \frac{
            \epsilon\Gamma(1-\epsilon)^3 \Gamma(\epsilon+1) \Gamma(2\epsilon-1)
        }{
            3 \epsilon-1
        }\,.
    \end{align}

    \task
    Solve the remaining system up to $\epsilon^0$ in $\vec I$.

\end{subtasks}

\begin{solution}\begin{subtasks}
    \task
    We now have a more complicated $\F$ which is no longer just $\F\propto\U$

    \task
    We perform the same IBP reduction we did in Sheet 2 with the family completed as
    \begin{align}
        I(a_1,a_2,a_3,a_4,a_5) = \int[\D k_1][\D k_2]\frac{1}
            {\big[k_1^2\big]^{a_1}
             \big[k_2^2\big]^{a_2}
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}
             \big[k_1\cdot p\big]^{a_4}
             \big[k_2\cdot p\big]^{a_5}
             }\,.
    \end{align}

    \task
    For $\partial_s$ we simplify have
    \begin{align}
        \frac{\partial}{\partial s} I(a_1,a_2,a_3,0,0) = \frac1{2s} &\int[\D k_1][\D k_2]
            p\cdot\frac{\partial}{\partial p}
            \frac{1}
            {\big[k_1^2\big]^{a_1}
             \big[k_2^2\big]^{a_2}
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}}\\
        \begin{split}
        = \frac{a_3}{s}\Bigg(
              &\int[\D k_1][\D k_2]\frac{2k_1\cdot p}
                {\big[k_1^2\big]^{a_1}
                 \big[k_2^2\big]^{a_2}
                 \big[(k_1-k_2-p)^2 - m^2\big]^{a_3+1}
                 \big[k_1\cdot p\big]^{-1}
                }\\&
              -\int[\D k_1][\D k_2]\frac{k_2\cdot p}
                {\big[k_1^2\big]^{a_1}
                 \big[k_2^2\big]^{a_2}
                 \big[(k_1-k_2-p)^2 - m^2\big]^{a_3+1}
                 \big[k_2\cdot p\big]^{-1}
                 }\\&
              -s\ \int[\D k_1][\D k_2]\frac{1}
                {\big[k_1^2\big]^{a_1}
                 \big[k_2^2\big]^{a_2}
                 \big[(k_1-k_2-p)^2 - m^2\big]^{a_3+1}}
        \Bigg)\,.
        \end{split}
    \end{align}
    Since $m$ only appears internally, $\partial_{m^2}$ cannot be written in terms of $\partial_p$, instead we have
    \begin{align}
        \frac{\partial}{\partial m^2} I(a_1,a_2,a_3) = a_3\int[\D k_1][\D k_2]
            \frac{1}
            {\big[k_1^2\big]^{a_1}
             \big[k_2^2\big]^{a_2}
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3+1}}\,.
    \end{align}
    We can re-write this in terms of raising and lowering operators
    \begin{align}
        \partial_s &= \frac{a_3}{s} {\bf 3}^+{\bf 4}^- - \frac{a_3}{s} {\bf 3}^+{\bf 5}^- - a_3 {\bf 3}^+\,,\\
        \partial_{m^2} &= a_3 {\bf 3}^+\,.
    \end{align}
    Applying this to our master integrals and performing the reduction yields
    \begin{align}
        \frac{\partial I_1}{\partial s  } &=-\frac{d m^2+d s-3 m^2-4 s}{s (m^2-s)}I_1 -m^2\frac{d-3}{s (m^2-s)}I_2 \\
        \frac{\partial I_2}{\partial m^2} &= \frac{2(d-7)             }{m^2-s    }I_1+\frac{d-3}{m^2-s}I_2\\
        \frac{\partial I_1}{\partial s  } &= \frac{d-4                }{2 s      }I_1+\frac{d-4}{2 s}  I_2\\
        \frac{\partial I_2}{\partial m^2} &=-\frac{d-4                }{2 m^2    }I_1+\frac{d-4}{2 m^2}I_2\,.
    \end{align}
    From this we can read of $A_s$ and $A_{m^2}$.

    \task
    Calculating $s A_s+m^2 A_{m^2}$ we have ${\rm diag}(-4 + d, -4 + d)$.
    Since both integrals start at $\epsilon^{-2}$, we need to multiply with $\epsilon^2(m^2)^{4-d}$.
    Choosing $T=\epsilon^{-2}(m^2)^{d-4}$ achieves this aim.
    The new matrices $B_s$ and $B_{m^2}$ are
    \begin{align}
        B_s = \begin{pmatrix}
             \frac{m^2 (2 \epsilon -1)+2 s \epsilon }{s (m^2-s)} & \frac{m^2 (2 \epsilon -1)}{s(m^2-s)} \\
             -\frac{\epsilon }{s} & -\frac{\epsilon }{s}
        \end{pmatrix}
        \quad\text{and}\quad
        B_{m^2} = \begin{pmatrix}
             \frac{-2 m^2 \epsilon +m^2-2 s \epsilon }{m^4-m^2 s} & \frac{1-2 \epsilon }{m^2-s} \\
             \frac{\epsilon }{m^2} & \frac{\epsilon }{m^2}
        \end{pmatrix}\,.
    \end{align}
    Setting $x=s/m^2$
    \begin{align}
        B_x = \begin{pmatrix}
             \frac{1-2 (x+1) \epsilon }{(x-1) x} & \frac{1-2 \epsilon }{(x-1) x} \\
             -\frac{\epsilon }{x} & -\frac{\epsilon }{x}
        \end{pmatrix}
    \end{align}
    is pre-canonical.

    \task
    We write
    \begin{align}
        B_x = T^{-1}\Big(A_s \frac{\partial s}{\partial x}\Big) T-T^{-1}\frac{\partial T}{\partial x}
         = \epsilon\Bigg[
            \frac1{\eta_1} \begin{pmatrix}-1& 1\\-2&  2\end{pmatrix} +
            \frac1{\eta_2} \begin{pmatrix} 0& 0\\ 2& -4\end{pmatrix}
        \Bigg]\,,
    \end{align}
    with $\eta_1=x$ and $\eta_2=x-1$.

    \task
    Consider $T^{-1}$ for $x=0$
    \begin{align}
        T^{-1}\Big|_{x=0} = (m^2)^{2\epsilon}\epsilon^2
            \begin{pmatrix}
                 -\frac{\epsilon }{6 \epsilon ^2-5 \epsilon +1} & \frac{1}{1-3 \epsilon } \\
                 -\frac{\epsilon }{6 \epsilon ^2-5 \epsilon +1} & \frac{1}{1-3 \epsilon }
            \end{pmatrix}\,.
    \end{align}
    Multiplying with $\vec I_0$ from the previous problem gives two times the same result
    \begin{align}
        J_{0,1} = J_{0,2} =
             \frac{\epsilon^2}{1-3 \epsilon}
             \frac{\Gamma(1-\epsilon)^4\Gamma( \epsilon)\Gamma(2\epsilon)                  }{\Gamma(2-\epsilon)}
            -\frac{\epsilon^3}{6\epsilon^2-5\epsilon+1}
             \frac{\Gamma(1-\epsilon)^3\Gamma(-\epsilon)\Gamma(2\epsilon)\Gamma(\epsilon+1)}{\Gamma(2-\epsilon)}
             \,,
    \end{align}
    which can easily be simplified to the required expression.

    \task
    By using the definition of the $G$ function, this is fairly simple
    \begin{align}
        \vec J^{(0)} &= \Big(\frac12, \frac12\Big)\,,\\
        \vec J^{(1)} &= \Big(\frac52, \frac52 - G(1,x)\Big)\,,\\
        \vec J^{(2)} &= \Big(
            \frac{19}{2}             + 2\zeta_2 -   G(0, 1, x),
            \frac{19}{2} - 5 G(1, x) + 2\zeta_2 - 2 G(0, 1, x) + 4 G(1, 1, x)
        \Big)\,.
    \end{align}
    And hence for $\vec I=T\vec J$
    \begin{align}
        \begin{split}
        (m^2)^{2\epsilon}\vec I = \Bigg\{&
            -\frac{1}{2 \epsilon^2}
            +\frac1\epsilon\Big(-\frac32 + \frac{x-1}{x} G(1, x)\Big)
            - \frac92 - 2\zeta_2 + 3 \frac{x-1}x G(1, x) \\&\qquad\qquad+ \frac{-1 + 2 x}x G(0, 1, x) - 4 \frac{x-1}x G(1, 1, x)
            + \mathcal{O}(\epsilon)\,,
        \end{split}\\&
            \frac1{2\epsilon^2}
            +\frac1{2\epsilon}
            -\frac12 + 2\zeta_2 + \frac{x-1}{x} G(1,x) - G(0,1,x) + \mathcal{O}(\epsilon)
        \Bigg\}\,.
    \end{align}
\end{subtasks}\end{solution}
\end{document}
