 %PDFLATEX
\documentclass{homework}
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath}
\sheetnr{2}
\lecturer{Dr~Y.~Ulrich}
\lecture{Practical guide to loop integration}
\term{Summer Term 2024}
\website{https://yannickulrich.com/loop-integration}
\logo{bern.pdf}
\issued{23 April 2024}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing,positioning,decorations.pathreplacing,shapes,calc}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }
\tikzset{
    photon/.style={decorate, decoration={snake,amplitude=1pt,segment length=6pt}}
}

\usepackage{listings}
\lstset{ %
  basicstyle=\ttfamily,
  keywordstyle=\color{Aquamarine4},
  numberstyle=\color{DarkOrange},
  stringstyle=\color{SkyBlue4},
  commentstyle=\color{Blue2},
  morecomment=[l]{!\ },% Comment only with space after !
  escapeinside={(*@}{@*)},
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}
}
\lstdefinestyle{yaml}{
  basicstyle=\color{Aquamarine4}\ttfamily,
  rulecolor=\color{black},
  string=[s]{"}{"},
  stringstyle=\color{SkyBlue4},
  comment=[l]{:},
  commentstyle=\color{black},
  morecomment=[l]{-}
}

\newcommand\D{\mathrm{d}}
\newcommand\U{\mathcal{U}}
\newcommand\F{\mathcal{F}}

\begin{document}
\maketitle
\def\base{
    \coordinate (a0) at (-.75,0);
    \coordinate (a1) at (+.75,0);
    \coordinate (b1) at (0,+.75);
    \coordinate (b2) at (0,-.75);
    \draw (-1.5, 0) -- (a0);
    \draw[photon] (0,0) circle(.75);
    \draw (a0) -- (a1);
    \draw (1.5, 0) -- (a1);
}

\exercise{Sunset diagram}
Consider the following integral
\begin{align}
I(a_1, a_2, a_3; \mathcal{N}) = \mathcal{N}\times
\begin{gathered}
\begin{tikzpicture}[baseline=+1cm]
\base
\node at (b1) [above] {$a_1$};
\node at (0,0) [below] {$a_3$};
\node at (b2) [below] {$a_2$};
\end{tikzpicture}
\end{gathered}
= \int[\D k_1][\D k_2]\frac{\mathcal{N}}
    {\big[k_1^2\big]^{a_1}
     \big[k_2^2\big]^{a_2}
     \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}}
\label{eq:family}
\end{align}
with $p^2=m^2\neq 0$ and an arbitrary numerator $\mathcal{N}$.

\begin{subtasks}
    \task
    Find a complete family. We know that it will need $\ell(1+\ell+2\rho)/2 = 5$ propagators.

    \task
    Identify the sectors in which all integrals vanish.

    \task
    Consider the IBP generated through $\partial_{k_1^\mu} (k_1^\mu I)$.
    Use it to show that
    \begin{align}
        \int[\D k_1][\D k_2]\frac{k_2\cdot p}
            {\big[k_1^2\big]
             \big[k_2^2\big]
             \big[(k_1-k_2-p)^2 - m^2\big]^{2}}
        =
        \frac{3-d}2
        \int[\D k_1][\D k_2]\frac{1}
            {\big[k_1^2\big]
             \big[k_2^2\big]
             \big[(k_1-k_2-p)^2 - m^2\big]}
    \end{align}

    \task
    Now find all six seed identities as a function of $a_1, \ldots, a_5$.

    \task
    Implement Laporta's algorithm to solve the system up to $r=4$ and $s=1$ for sector 7 and its subsectors.

\end{subtasks}

\begin{solution}
\begin{subtasks}
    \task
    We choose to complete the family as follow
    \begin{align*}
        I(a_1,a_2,a_3,a_4,a_5) = \int[\D k_1][\D k_2]
            \frac1{
             \big[k_1^2\big]^{a_1}
             \big[k_2^2\big]^{a_2}
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}
             \big[k_1\cdot p\big]^{a_4}
             \big[k_2\cdot p\big]^{a_5}
            }\,.
    \end{align*}
    Note that this is not a unique solution

    \task
    The sectors 1-6 are zero
    \begin{align*}
        I(a_1,0  ,0  ,0  ,0  ) &= \int[\D k_1][\D k_2]
            \frac1{
             \big[k_1^2\big]^{a_1}
            }\,,\\
        I( 0 ,a_2, 0 , 0 , 0 ) &= \int[\D k_1][\D k_2]
            \frac1{
             \big[k_2^2\big]^{a_2}
            }\,,\\
        I(a_1,a_2, 0 , 0 , 0 ) &= \int[\D k_1][\D k_2]
            \frac1{
             \big[k_1^2\big]^{a_1}
             \big[k_2^2\big]^{a_2}
            }\,,\\
        I( 0 , 0 ,a_3, 0 , 0 ) &= \int[\D k_1][\D k_2]
            \frac1{
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}
            }\,,\\
        I(a_1, 0 ,a_3, 0 , 0 ) &= \int[\D k_1][\D k_2]
            \frac1{
             \big[k_1^2\big]^{a_1}
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}
            }\,.
    \end{align*}

    \task
    From
    \begin{align*}
        0 = \int[\D k_1][\D k_2]
        \partial_{k_1^\mu}\Bigg(k_1^\mu
            \frac1{
             \big[k_1^2\big]^{a_1}
             \big[k_2^2\big]^{a_2}
             \big[(k_1-k_2-p)^2 - m^2\big]^{a_3}
             \big[k_1\cdot p\big]^{a_4}
             \big[k_2\cdot p\big]^{a_5}
            }
        \Bigg)
    \end{align*}
    we find
    \begin{align*}
        0 =
        (d -2 a_1 - a_3 - a_4)
        - a_3 {\bf 1}^-{\bf 3}^+
        + a_3 {\bf 2}^-{\bf 3}^+
        +2a_3 {\bf 5}^-{\bf 3}^+\,.
    \end{align*}
    with $a_1=a_2=a_3=1$ and $a_4=a_5=0$, we find our relation
    \begin{align*}
        0 &=
        (d - 3) I(1,1,1,0,0)
        -  I(0,1,2,0,0)
        +  I(1,0,2,0,0)
        +2 I(1,1,2,0,-1)\\
        \Rightarrow
        I(1,1,2,0,-1)
        &=
        \frac{3-d}2
        I(1,1,1,0,0)\,.
    \end{align*}

    \task
    We find
    \begin{align*}
    0&= -2 a_1-a_3 -a_4+d
        -  a_3{\bf 1}^- {\bf 3}^+
        +  a_3{\bf 2}^- {\bf 3}^+
        +2 a_3{\bf 5}^- {\bf 3}^+
        \\
    0&= -a_1+a_3
        -  a_1{\bf 2}^- {\bf 1}^+
        +  a_1{\bf 3}^- {\bf 1}^+
        +2 a_1{\bf 4}^- {\bf 1}^+
        -2 a_1{\bf 5}^- {\bf 1}^+
        -  a_3{\bf 1}^- {\bf 3}^+\\&\qquad\qquad
        +  a_3{\bf 2}^- {\bf 3}^+
        +2 a_3{\bf 4}^- {\bf 3}^+
        -  a_4{\bf 5}^- {\bf 4}^+
        \\
    0&= -2a_1{\bf 4}^- {\bf 1}^+
        -2a_3{\bf 4}^- {\bf 3}^+
        +2a_3{\bf 5}^- {\bf 3}^+
        +2 a_3 m^2{\bf 3}^+
        -  a_4 m^2{\bf 4}^+
        \\
    0&= -a_2+a_3
        -  a_2{\bf 1}^- {\bf 2}^+
        +  a_2{\bf 3}^- {\bf 2}^+
        +2 a_2{\bf 4}^- {\bf 2}^+
        -2 a_2{\bf 5}^- {\bf 2}^+
        +  a_3{\bf 1}^- {\bf 3}^+\\&\qquad\qquad
        -  a_3{\bf 2}^- {\bf 3}^+
        -2 a_3{\bf 5}^- {\bf 3}^+
        -  a_5{\bf 4}^- {\bf 5}^+
        \\
    0&= -2a_2-a_3 -a_5+d
        +  a_3{\bf 1}^- {\bf 3}^+
        -  a_3{\bf 2}^- {\bf 3}^+
        -2 a_3{\bf 4}^- {\bf 3}^+
        \\
    0&= -2  a_2{\bf 5}^- {\bf 2}^+
        +2  a_3{\bf 4}^- {\bf 3}^+
        -2  a_3{\bf 5}^- {\bf 3}^+
        -2  a_3 m^2{\bf 3}^+
        -   a_5 m^2{\bf 5}^+
    \end{align*}

    \task
    The implementation can be found online:\\
    \url{https://gitlab.com/yannickulrich/loop-integration/-/blob/root/code/sheet2.m}

\end{subtasks}
\end{solution}

\exercise{Sunset diagram using computer codes}

Consider again the same integral~\eqref{eq:family} but now with $p^2 = s\neq m^2$.
Perform the reduction using {\tt reduze} or {\tt kira} for all integrals with $r\le4$ and $s\le1$.

\begin{subtasks}
    \task
    Make a list of all 75 integrals you want to calculate using a computer program.

    \task
    Perform the reduction of all integrals without specifying a basis of master integrals.
    How many master integrals do you find?

    \task
    Consider the following possible choices of master integrals.
    Which ones do you prefer and why?
    \begin{align}
        \vec I_1 = \begin{pmatrix} I(1,1,1;1)\\ I(1,1,2;1)          \end{pmatrix}\,,\qquad
        \vec I_2 = \begin{pmatrix} I(1,1,1;1)\\ I(1,1,1;p\cdot k_1) \end{pmatrix}\,,\qquad
        \vec I_3 = \begin{pmatrix} I(1,2,1;1)\\ I(1,1,2;1)          \end{pmatrix}\,.
    \end{align}

\end{subtasks}

\begin{solution}
    \begin{subtasks}
        \task
        We can use the following Mathematica program
        \begin{lstlisting}[language=mathematica]
RST[sunset[a__]] := {
  (* r *) Total[Select[{a}, # > 0 &]],
  (* s *) -Total[Select[{a}, # < 0 &]],
  (* t *) Length[Select[{a}, # > 0 &]]
}
IsInRange[{r_, s_, t_}] := 0 <= r <= 4 && s <= 1 && t >= 2
request = Select[
  Flatten@Outer[
    sunset,
    {-1, 0, 1, 2}, (* propagator 1 *)
    {-1, 0, 1, 2}, (* propagator 2 *)
    {-1, 0, 1, 2}, (* propagator 3 *)
    {0, -1}, (* auxilary propagator 1 *)
    {0, -1} (* auxilary propagator 1 *)
  ],
  IsInRange@*RS
];
Export["req.txt", request]
        \end{lstlisting}

        \task
        To set up {\tt reduze} and {\tt kira} we need to first specify the kinematics as {\tt config/kinematics.yaml}
        \lstinputlisting[style=yaml]{../code/sunset/config/kinematics.yaml}
        and the integral family as {\tt config/kinematics.yaml}
        \lstinputlisting[style=yaml]{../code/sunset/config/integralfamilies.yaml}
        Next we specify the job we want to execute
        \lstinputlisting[style=yaml]{../code/sunset/jobs-reduze.yaml}

        \task
        To change the basis we specify {\tt preferred\_masters\_file} and rerun the reduction.
        Comparing the results we find that
        \begin{itemize}
            \item
                $\vec I_1$ introduces denominators of the form $d-4$ meaning that we need to know the masters higher than $\mathcal{O}(\epsilon^0)$

            \item
                $\vec I_2$ complicates the calculation of the masters

            \item
                $\vec I_3$ introduces complicated denominators of the form $(3-d) (8-3d)$ which is not ideal.
        \end{itemize}

    \end{subtasks}
\end{solution}
\end{document}
