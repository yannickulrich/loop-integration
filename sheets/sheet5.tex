 %PDFLATEX
\documentclass{homework}
\usepackage{amsmath}
\sheetnr{5}
\lecturer{Dr~Y.~Ulrich}
\lecture{Practical guide to loop integration}
\term{Summer Term 2024}
\website{https://yannickulrich.com/loop-integration}
\logo{bern.pdf}
\issued{14 May 2024}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing,positioning,decorations.pathreplacing,shapes,calc}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }
\tikzset{
    photon/.style={decorate, decoration={snake,amplitude=1pt,segment length=6pt}}
}


\newcommand\D{\mathrm{d}}
\newcommand\I{\mathrm{i}}
\newcommand\U{\mathcal{U}}
\newcommand\F{\mathcal{F}}
\DeclareMathOperator*{\res}{res}
\newmuskip\pFqmuskip

\newcommand*\pFq[6][8]{%
  \begingroup % only local assignments
  \pFqmuskip=#1mu\relax
  \mathchardef\normalcomma=\mathcode`,
  % make the comma maths active
  \mathcode`\,=\string"8000
  % and define it to be \pFqcomma
  \begingroup\lccode`\~=`\,
  \lowercase{\endgroup\let~}\pFqcomma
  % typeset the formula
  {}_{#2}F_{#3}{\left[\genfrac..{0pt}{}{#4}{#5};#6\right]}%
  \endgroup
}
\newcommand{\pFqcomma}{{\normalcomma}\mskip\pFqmuskip}

\begin{document}
\maketitle

You might find the Mathematica packages {\tt MB Tools} useful: \url{https://mbtools.hepforge.org/}.
Consider using especially {\tt MB.m}, {\tt MBresolve.m}, and {\tt barnesroutines.m}.

\exercise{Mellin Barnes expansion}

In this example, we will consider once again a sunset diagram

\begin{align}
\begin{gathered}
\begin{tikzpicture}[baseline=-2pt]
    \coordinate (a0) at (-.75,0);
    \coordinate (a1) at (+.75,0);
    \coordinate (b1) at (0,+.75);
    \coordinate (b2) at (0,-.75);
    \draw[dashed] (-1.5, 0) -- (a0);
    \centerarc[](0,0)(180:0:+0.75)
    \centerarc[photon](0,0)(180:0:-0.75)
    \draw[dashed] (a0) -- (a1);
    \draw[dashed] (1.5, 0) -- (a1);
\end{tikzpicture}
\end{gathered}
= \int[\D k_1][\D k_2]\frac1{\big[k_1^2-m^2\big]
                             \big[k_2^2-M^2\big]
                             \big[(k_1-k_2-p)^2\big]}
\end{align}
with $p^2=m^2$.

\begin{subtasks}
    \task
    Use a single MB split to solve the Feynman integrals.
    There is no need to sum the MB series yet.

\end{subtasks}

The MB will be of the form
\begin{align*}
    I = \int_{-\I\infty}^{+\I\infty} \D z
            \Big(\frac{m^2}{M^2}\Big)^{-z} f(z)
      = \sum_{n=0}^\infty \Big(\frac{m^2}{M^2}\Big)^n f'(n)
\end{align*}
with some $f(z)$ and $f'(n)$ assuming we have closed the contour on the correct side.

We can at this point decide that $0<m\ll M$ and expand the integral.
Conceptually, this is done by noting that the terms in the series of residues are suppressed by $(m^2/M^2)^n$.
To expand to any order in $m^2/M^2$ we just truncate the series.

\begin{subtasks}
    \setcounter{enumi}{1}
    \task
    Calculate the integral up to $\mathcal{O}(m^4)$.

    \task
    Solve the integral exactly in $m$ by calculating the full series, expand in $\epsilon$ using {\tt HypExp}.
    Finally expand in $m$ to verify your result.

    {\it (for the adventurous)}

\end{subtasks}

\begin{solution}
\begin{subtasks}
    \task
    We have
    \begin{align*}
        I = -\Gamma(1-\epsilon)^2\Gamma(2\epsilon-1)
                \int\D \vec x\delta(\cdots)
                \frac{\Big(x_1x_2+x_1x_3+x_2x_3\Big)^{-3+3\epsilon}}
                     {\Big(m^2x_1^2(x_2+x_3) + M^2x_2 (x_1x_2+x_1x_3+x_2x_3)\Big)^{-1+2\epsilon}}\,.
    \end{align*}
    We split at $m^2$
    \begin{align*}
        I = -(m^2)^{1-2\epsilon}\Gamma(1-\epsilon)^2
            \int_{-\I\infty}^{+\I\infty}&\D z \bigg(\frac{M^2}{m^2}\bigg)^z \Gamma(-z)\Gamma(2\epsilon-1+z)
            \int\D x_1\D x_2\D x_3\delta(\cdots)
                \\&
                x_1^{2-2z-4\epsilon}x_2^z(x_2+x_3)^{1-z-2\epsilon}
                (x_1x_2+x_1x_3+x_2x_3)^{-3+3\epsilon+z}
        \,.
    \end{align*}
    We can solve $x_1$ and $x_2$ and use the $\delta$ function for $x_3$
    \begin{align*}
        I= -(m^2)^{1-2\epsilon}\Gamma(1-\epsilon)^3
            \int_{-\I\infty}^{+\I\infty}&\D z \bigg(\frac{m^2}{M^2}\bigg)^{-z}
            \\&
            \frac{\Gamma(-z)\Gamma(3-4\epsilon-2z)\Gamma(1-\epsilon-z)\Gamma(\epsilon+z)\Gamma(2\epsilon-1+z)}
                 {\Gamma(3-3\epsilon-z)\Gamma(2-2\epsilon-z)}\,.
    \end{align*}
    Closing the contour around the left poles we have
    \begin{align*}
        I = M^2\Gamma(1&-\epsilon)^3
            \Bigg[
            \bigg(\frac{\mu^2}{mM}\bigg)^{2\epsilon}
                \sum_{n=0}^\infty
                \bigg(-\frac{m^2}{M^2}\bigg)^{n+1}
                \frac{\Gamma(2n-2\epsilon+3) \Gamma(-n+\epsilon-1) \Gamma(n+\epsilon)}
                     {\Gamma(n-2\epsilon+3) \Gamma(n-\epsilon+2)}
                  \\&
                  +
            \bigg(\frac{\mu^2}{M^2}\bigg)^{2\epsilon}
                \sum_{n=0}^\infty
                \bigg(-\frac{m^2}{M^2}\bigg)^{n}
                \frac{\Gamma(2 n+1) \Gamma(-n-\epsilon +1) \Gamma(n+\epsilon) \Gamma(n+2 \epsilon -1)}
                     {\Gamma(n+1)^2 \Gamma(n-\epsilon +2)}
             \Bigg]\,.
    \end{align*}

    \task
    Calculating the first few terms
    \begin{align*}
        I = \frac12\bigg(\frac{\mu^2}{M^2}&\bigg)^{2\epsilon}\Bigg[
            M^2\bigg(
                \frac1{\epsilon^2} + \frac3\epsilon+4\zeta_2+7
            \bigg)\\&
            + m^2\bigg(
                \frac1{\epsilon^2} + \frac1\epsilon\Big(\frac52-2\log\frac{m^2}{M^2}\Big)
                    + \frac{17}4 -4\log\frac{m^2}{M^2} + \log^2\frac{m^2}{M^2}
            \bigg)
            +\mathcal{O}(m^4)\,.
        \Bigg]
    \end{align*}


    \task
    We find
    \begin{align*}
        I = -M^{-4\epsilon}
            \frac{\Gamma(1-\epsilon)^3\Gamma(\epsilon)}{\Gamma(2-\epsilon)}
            &
            \Bigg[
                M^2\Gamma(1-\epsilon)\Gamma(2\epsilon-1)
                    \pFq21{\tfrac12,2\epsilon-1}{2-\epsilon}{\frac{4m^2}{M^2}}
                \\&
                + m^2 \bigg(\frac{m^2}{M^2}\bigg)^\epsilon\Gamma(\epsilon-1)
                    \pFq32{1,\tfrac32-\epsilon,\epsilon}{3-2\epsilon,2-\epsilon}{\frac{4m^2}{M^2}}
            \Bigg]
    \end{align*}
    using {\tt HypExp} we can expand this
    \begin{align*}
        I = \frac12 m^2 M^{-4\epsilon}\Bigg[
        &
                \frac1{\epsilon^2}\Big(3+\frac1x+x\Big)
               +\frac1{\epsilon  }\Big(\frac{17}2 + \frac3x + 3x + 4H_{-1}(x) - 2 H_0(x)\Big)
               \\&
               + \frac6x
               + \frac{59}4
               + 6 x
               + \frac4x\zeta_2
               + 8\zeta_2
               + 4\zeta_2x
               - \Big(8 + 4 x + x^2\Big) H_0(x)
               \\&
               + \Big(\frac1{x^2} + \frac4x + 16 + 4 x + x^2\Big) H_{-1}(x)
               + 4\Big(\frac1x + x\Big) H_{0,-1}(x)
               \\&
               - 8\Big(\frac1x + x\Big) H_{-1,-1}(x)
               + 4\Big(\frac1x + x\Big) H_{-1,0}(x)
               + 2 H_{0,0}(x)
        \Bigg]
    \end{align*}
    where we have defined
    \begin{align*}
        x = \frac{1-\beta}{1+\beta}
        \quad\text{with}\quad
        \beta=\sqrt{1-\frac{4m^2}{M^2}}\,.
    \end{align*}
\end{subtasks}
\end{solution}


\turnpage
\exercise{Multiple Mellin Barnes}

Consider the following non-planar integral
\begin{align*}
\begin{gathered}
\begin{tikzpicture}[baseline=-2pt]
    \draw[photon](-0.5,0) -- (0,0);
    \draw[      ](0,0) -- (+30:2);
    \draw[      ](0,0) -- (-30:2);
    \draw[photon](+30:1.0) -- (-30:1.5);
    \draw[photon](+30:1.5) -- (-30:1.0);
\end{tikzpicture}
\end{gathered}
= \int[\D k_1][\D k_2]
    \frac1{\big[k_1^2\big]
           \big[k_2^2\big]
           \big[(k_1-p-q)^2\big]
           \big[(k_1-k_2)^2\big]
           \big[(k_1-k_2-q)^2\big]
           \big[(k_2-p)^2\big]}
\end{align*}
with $p^2=q^2=0$ and $(p+q)^2=s$.

\begin{subtasks}
    \task
    Solve the Feynman integration.
    This can be done using two Mellin Barnes splits
    \begin{align*}
        \frac1{(A_1+A_2+A_3)^\lambda}
        =\frac1{\Gamma(\lambda)}
        \int_{-\I\infty}^{+\I\infty}\D z_1\D z_2
            A_1^{z_1}\Gamma(-z_1)
            A_2^{z_2}\Gamma(-z_2)
            A_3^{-\lambda-z_1-z_2} \Gamma(\lambda+z_1+z_2)
        \,.
    \end{align*}

    {\it Hint}: You might find the substitution $x_2\to x_6x_2$ useful.

    \task
    Resolve the singularities and expand in $\epsilon$ up to $\epsilon^0$.

    \task
    Use the Barnes Lemmas and PSLQ to solve the resulting integral


\end{subtasks}

\begin{solution}
\begin{subtasks}
    \task
    Setting $s=-1$, we have
    {\small
    \begin{align*}
        I = \Gamma(1-\epsilon)^2 \Gamma(2\epsilon+2)
            \int\D x_1\cdots\D x_6\delta(\cdots)
            \frac{
                \Big[
                    (x_4+x_5) (x_2+x_6) +(x_1+x_3) (x_2+x_4+x_5+x_6)
                \Big]^{3\epsilon}
            }{
                \Big[
                    x_2 x_3 x_4
                    +x_1 x_5 x_6
                    +x_1 x_3 (x_2+x_4+x_5+x_6)\Big]^{2 (\epsilon +1)}
            }
    \end{align*}}
    We use MB for the three terms of the denominator.
    Afterwards we can trivially solve $x_1$ and $x_3$ and arrive at
    {\small
    \begin{align*}
        I = \frac{\Gamma(1-\epsilon)^2}{\Gamma(-3\epsilon)}
            \int_{-\I\infty}^{+\I\infty}\D z_1\D z_2
            \int\D x_2\D x_4\D x_5\D x_6\delta(\cdots)
                \frac{
                    x_2^{z_1} x_4^{z_1} x_5^{z_2} x_6^{z_2}
                    (x_2+x_4+x_5+x_6)^{2\epsilon}
                }{
                    (x_4+x_5)^{2+z_1+z_2+\epsilon}
                    (x_2+x_6)^{2+z_1+z_2+\epsilon}
                }
                \\
                \times
                \Gamma(-z_1)
                \Gamma(-z_2)
                \Gamma(-z_1-2 \epsilon -1)
                \Gamma(-z_2-2 \epsilon -1)
                \Gamma( z_1+z_2+  \epsilon +2)
                \Gamma( z_1+z_2+2 \epsilon +2)
                \,.
    \end{align*}}
    By substituting $x_2\to x_6x_2$, we can turn $x_2^\alpha x_6^\beta (x_2+x_6)^\gamma$ into $x_2^\alpha x_6^{1+\alpha+\beta+\gamma} (1+x_2)^\gamma$.
    Afterwards, we can solve $x_6$, $x_2$, and $x_4$.
    Finally, we use the $\delta$ function for $x_5$
    \begin{align*}
    I =
    \frac{
        \Gamma (1-\epsilon )^2
        \Gamma (-\epsilon)^2
    }{
         \Gamma (-3 \epsilon )
         \Gamma (-2 \epsilon )
    }
    \int_{-\I\infty}^{+\I\infty}\D z_1\D z_2
    f(z_1,z_2)
    \end{align*}
    where we have defined
    \begin{align*}
    f(z_1,z_2)&=
    \frac{
        \Gamma (\epsilon +z_1+z_2+2)
        \Gamma (2 \epsilon +z_1+z_2+2)
    }{\Gamma (z_1+z_2+2)^2}
    \\&\qquad
        \Gamma (-z_1)
        \Gamma (z_1+1)^2
        \Gamma (-2 \epsilon -z_1-1)
        \Gamma (-z_2)
        \Gamma (z_2+1)^2
        \Gamma (-2 \epsilon -z_2-1)
    \end{align*}


    \task
    After resolving the poles we are left with
    \begin{align*}
        I = +\underbrace{
                \res_{z_2=-1-2\epsilon}\Big[\res_{z_1=-1-2\epsilon} f(z_1,z_2)\Big]
            }_{I^{(0)}}
            +\underbrace{
                 \int_{-1/3-\I\infty}^{-1/3+\I\infty}\D z_1
                 \int_{-3/2-\I\infty}^{-3/2+\I\infty}\D z_2 f(z_1,z_2)
            }_{I^{(2)}}
            \\
            +\underbrace{
                \int_{-2/3-\I\infty}^{-2/3+\I\infty}\D z_1 \res_{z_2=-1-2\epsilon} f(z_1,z_2)
            -
                \int_{-2/3-\I\infty}^{-2/3+\I\infty}\D z_2 \res_{z_1=-1-2\epsilon} f(z_1,z_2)
            }_{I^{(1)}}
        \,.
    \end{align*}
    We can now expand in $\epsilon$.
    $I^{(0)}$ and $I^{(1)}$ are divergent with $\epsilon^{-4}$ and $\epsilon^{-2}$ poles respectively.

    \task
    Let us focus on the finite double integral $I^{(2)}$ first
    {\small
    \begin{align*}
        I^{(2)} &= \underbrace{\Bigg[
                   \int_{-1/3-\I\infty}^{-1/3+\I\infty}\D z_1
                        \Gamma (-z_1) \Gamma (z_1+1)^2 \Gamma (-z_1-1)
                   \Bigg]}_{-\zeta_2}
                   \underbrace{\Bigg[
                   \int_{-3/2-\I\infty}^{-3/2+\I\infty}\D z_2
                        \Gamma (-z_2) \Gamma (z_2+1)^2 \Gamma (-z_2-1)
                   \Bigg]}_{+\zeta_2}\,.
    \end{align*}}
    The sign difference comes from the slightly different contour.
    The single integrals can not completely be solved with the Barnes Lemma though some headway can be made.
    We find for the poles
    \begin{align*}
        I^{(1)} = -\frac{\zeta_2}{2\epsilon^2} - \frac{\gamma_E\zeta_2-3\zeta_3}{2\epsilon}
            +
            \int_{-2/3-\I\infty}^{-2/3+\I\infty}\D z f'(z)\,.
    \end{align*}
    $f'$ is a complicated function involving $\psi^2$ and $\psi'$.
    We could further attempt the Barnes Lemmas.
    However, out of sheer lazyness we just PSLQ the entire thing using the basis $\{\gamma_E^2\zeta_2, \gamma_E\zeta_3, \zeta_4\}$
    \begin{align*}
        \int_{-2/3-\I\infty}^{-2/3+\I\infty}\D z f'(z) = 4.82717690481472
            = -\frac14\zeta_2 \gamma_E^2 + \frac32 \gamma_E \zeta_3 + \frac{29}8\zeta_4
    \end{align*}
    Finally,
    \begin{align*}
        I = \frac1{\epsilon^4} - \frac{5\zeta_2}{\epsilon^2} - \frac{27\zeta_3}\epsilon - \frac{115\zeta_4}2\,.
    \end{align*}
    Had we used PSLQ earlier and included the pre-factor the basis would have been a lot easier.
    Without much effort we could have calculated
    \begin{align*}
        I =  \frac{1.}{\epsilon^4} + \frac{0.}{\epsilon^3} - \frac{-8.224675}{\epsilon^2} - \frac{-32.4555}\epsilon - 62.2336
    \end{align*}
    and just guessed the answer.

\end{subtasks}
\end{solution}


\end{document}
