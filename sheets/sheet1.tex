 %PDFLATEX
\documentclass{homework}
\usepackage{amsmath}
\sheetnr{1}
\lecturer{Dr~Y.~Ulrich}
\lecture{Practical guide to loop integration}
\term{Summer Term 2024}
\website{https://yannickulrich.com/loop-integration}
\logo{bern.pdf}
\issued{16 April 2024}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{calc}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }


\newcommand\D{\mathrm{d}}
\newcommand\U{\mathcal{U}}
\newcommand\F{\mathcal{F}}

\begin{document}
\maketitle

\exercise{$\Gamma$ functions}

Use our basic integral
\begin{align}\label{eq:gamma}
\int_0^\infty \D x\ x^\alpha (a+b x)^\beta
  = \frac{\Gamma(1+\alpha)\Gamma(-1-\alpha-\beta)}{\Gamma(-\beta)}
        \frac{a^{1+\alpha+\beta}}{b^{1+\alpha}}
\end{align}
to calculate
\begin{subtasks}
    \task
    $\displaystyle I_1(\alpha,\beta,\gamma,\delta)
     = \int\D x_1\D x_2\D x_3\delta(\cdots)
        x_1^\alpha
        x_2^\beta
        x_3^\gamma
        \big(x_2 x_3 + x_1 x_2 + x_1 x_3\big)^\delta$

    \task
    $\displaystyle I_2(\alpha,\beta,\gamma)
       = \int \D x_1\D x_2\D x_3\D x_4\delta(\cdots)
        x_1^\alpha
        \big(x_2 x_3 + x_2 x_4 + x_3 x_4\big)^\beta
        \big(x_1 x_3 + x_2 x_3 + x_1 x_4 + x_2 x_4 + x_3 x_4\big)^\gamma
    $

    \task
    $I_2(-2\epsilon, -1-2\epsilon, -1+3\epsilon)$ up to $\mathcal{O}(\epsilon^0)$

\end{subtasks}


\begin{solution}\begin{subtasks}
    \task
    The order of integration does not matter in this example.
    Let us pick $x_1$, $x_2$, and finally $x_3$ with the $\delta$ function
    \begin{align*}
    I_1(\alpha,\beta,\gamma,\delta)
    &= \int\D x_1\D x_2\D x_3\delta(\cdots)
        x_1^\alpha
        x_2^\beta
        x_3^\gamma
        \big(x_2 x_3 + x_1 x_2 + x_1 x_3)\big)^\delta
        \\
    &= \frac{\Gamma(\alpha +1) \Gamma(-\alpha-\delta-1) }{\Gamma(-\delta)}
      \int\D x_2\D x_3 \delta(\cdots)
        (x_2+x_3)^{-\alpha-1}x_2^{\alpha+\beta+\delta+1}x_3^{\alpha+\delta+\gamma+1}
        \\
    &= \frac{\Gamma(-\alpha-\delta-1)\Gamma(-\beta-\delta-1)\Gamma(\alpha+\beta+\delta+2)}{\Gamma(-\delta)}
    \underbrace{
        \int\D x_3\delta(1-x_3)x_3^{\alpha+\beta+2\delta+\gamma+2}
    }_1
    \end{align*}

    \task
    This example is slightly more complicated.
    Right now we cannot integrate eg. $x_2$.
    By first solving $x_1$, we can then solve $x_2$, and finally use the $\delta$ function for either $x_3$ or $x_4$
    \begin{align*}
    I_2(\alpha,\beta,\gamma)
       & = \int \D x_1\D x_2\D x_3\D x_4\delta(\cdots)
        x_1^\alpha\\&\qquad\qquad
        \big(x_2 x_3 + x_2 x_4 + x_3 x_4\big)^\beta
        \big(x_1 x_3 + x_2 x_3 + x_1 x_4 + x_2 x_4 + x_3 x_4\big)^\gamma
    \\
       & = \frac{\Gamma(1+\alpha)\Gamma(-1-\alpha-\gamma)}{\Gamma(-\gamma)}\\&\qquad\qquad
       \int \D x_2\D x_3\D x_4\delta(\cdots)
        \big(x_3+x_4\big)^{-1-\alpha}
        \big(x_2 x_3 + x_2 x_4 + x_3 x_4\big)^{\beta+1+\alpha+\gamma}
    \\
       & = \frac{\Gamma(\alpha+1)\Gamma(-\alpha-\gamma-1)\Gamma(-\alpha-\beta-\gamma-2)}
                {\Gamma(-\gamma)\Gamma(-\alpha-\beta-\gamma-1)}\\&\qquad\qquad
            \int\D x_3\D x_4\delta(\cdots)
             x_3^{\alpha +\beta +\gamma +2} x_4^{\alpha +\beta +\gamma +2} (x_3+x_4)^{-\alpha -2}
    \\
       & = \frac{\Gamma(\alpha+1)\Gamma(-\alpha-\gamma-1)
                 \Gamma(-\beta-\gamma-1)\Gamma(-\alpha-\beta-\gamma-2)
                 \Gamma(\alpha+\beta+\gamma+3)
                }{\Gamma(\alpha+2)\Gamma(-\gamma)
                  \Gamma(-\alpha-\beta-\gamma-1)}
    \end{align*}


    \task
    \begin{align*}
        I_2(-2\epsilon, -1-2\epsilon, -1+3\epsilon)
        =-\frac{1}{\epsilon^2} - \frac2\epsilon - 8 + \frac{\pi^2}2
    \end{align*}
\end{subtasks} \end{solution}



\exercise{Graph polynomials}


\newcommand\drawcut[3]{
    \def\th{#2}
    \def\r{#3}
    \def\w{0.1}
    \fill[white]  (#1) ++ (\th:\r)
                     --++ (\th+90:\w)
                     --++ (\th:-2*\r)
                     --++ (\th+90:-2*\w)
                     --++ (\th:2*\r) -- cycle;
    \draw[dashed] (#1) ++ (\th:\r) --+ (\th:-2*\r);
}
\newcommand\cutO[3]{
    \begin{gathered}
    \begin{tikzpicture}
    #1
    \drawcut{#2}{#3}{0.3}
    \end{tikzpicture}
    \end{gathered}
}
\newcommand\cutD[4]{
    \begin{gathered}
    \begin{tikzpicture}
    #1
    \drawcut{$(#2)!0.5!(#3)$}{#4}{0.6}
    \end{tikzpicture}
    \end{gathered}
}
\newcommand\cutT[3]{
    \begin{gathered}
    \begin{tikzpicture}
    #1
    \drawcut{#2}{#3}{0.9}
    \end{tikzpicture}
    \end{gathered}
}

\def\baseA{
    \coordinate (o0) at (0,0);
    \coordinate (o1) at (180:0.5);
    \coordinate (a1) at (+45:1);
    \coordinate (b1) at (-45:1);
    \coordinate (a2) at ($(a1) + (0.5,0)$);
    \coordinate (b2) at ($(b1) + (0.5,0)$);
    \coordinate (AB) at ($(a1)!0.5!(b1)$);
    \coordinate (AO) at ($(a1)!0.5!(o0)$);
    \coordinate (BO) at ($(b1)!0.5!(o0)$);
    \draw (o1) -- (o0);
    \draw[dashed] (o0) -- (a1) -- (a2);
    \draw[dashed] (o0) -- (b1) -- (b2);
    \draw[dashed] (a1) -- (b1);
}

\def\baseB{
    \coordinate (a0) at (-.75,0);
    \coordinate (a1) at (+.75,0);
    \coordinate (b1) at (0,+.75);
    \coordinate (b2) at (0,-.75);
    \draw (-1.5, 0) -- (a0);
    \draw[dashed] (0,0) circle(.75);
    \draw[dashed] (a0) -- (a1);
    \draw (1.5, 0) -- (a1);
}

\def\baseC{
    \coordinate (o0) at (0,0);
    \coordinate (o1) at (180:0.5);
    \coordinate (a1) at (+45:1);
    \coordinate (b1) at (-45:1);
    \coordinate (a2) at ($(a1) + (0.5,0)$);
    \coordinate (b2) at ($(b1) + (0.5,0)$);
    \coordinate (AB) at ($(a1)!0.5!(b1)$);
    \coordinate (AO) at ($(a1)!0.5!(o0)$);
    \coordinate (BO) at ($(b1)!0.5!(o0)$);
    \draw (o1) -- (o0);
    \draw[dashed] (o0) -- (a1) -- (a2);
    \draw[dashed] (o0) -- (b1) -- (b2);
    \draw[dashed] (b1) arc (-45:45:1);
    \draw[dashed] (b1) arc (225:135:1);
}

\def\baseD{
    \coordinate (a0) at (-1,0);
    \coordinate (a1) at (+1,0);
    \coordinate (b1) at (0,+1);
    \coordinate (b2) at (0,-1);
    \draw (-1.5, 0) -- (a0);
    \draw[dashed] (0,0) circle(1);
    \draw[dashed] (a1) arc (270:180:1);
    \draw (1.5, 0) -- (a1);
}


Find the graph polynomials and calculate the loop integrals
\begin{subtasks}
    \task
    \begin{tikzpicture}[baseline=-2pt]
        \baseA
    \end{tikzpicture}

    \task
    \begin{tikzpicture}[baseline=-2pt]
        \baseB
    \end{tikzpicture}

    \task
    \begin{tikzpicture}[baseline=-2pt]
        \baseC
    \end{tikzpicture}

    \task
    \begin{tikzpicture}[baseline=-2pt]
        \baseD
    \end{tikzpicture}

\end{subtasks}
Dashed lines represent massless propagators and solid lines massive ones.

\begin{solution}\begin{subtasks}
    \task
    We use the graphical method.
    At $\ell=1$ this means one cut for $\U$ and two for $\F$
    \begin{align*}
        \U &
        = \underbrace{\cutO{\baseA}{AB}{0}  }_{x_1}
        + \underbrace{\cutO{\baseA}{AO}{-45}}_{x_2}
        + \underbrace{\cutO{\baseA}{BO}{+45}}_{x_3}\,,
        \\
        \F &
        = \underbrace{\cutD{\baseA}{AB}{AO}{-30}}_{p^2 x_1x_2}
        + \underbrace{\cutD{\baseA}{AB}{BO}{+30}}_{q^2 x_1x_3}
        + \underbrace{\cutD{\baseA}{AO}{BO}{+90}}_{ -s x_2x_3}
        = -s x_2x_3\,.
    \end{align*}
    Hence,
    \begin{align*}
        I_1 &
        = -\Gamma(1+\epsilon)\Gamma(1-\epsilon)
            \int\D x_1\D x_2\D x_3\delta(\cdots)
            \frac{(x_1 + x_2 + x_3)^{-1+2\epsilon}}
                 {(-s)^{1+\epsilon}x_2^{1+\epsilon}x_3^{1+\epsilon}}
        \\&
        = -(-s)^{-1-\epsilon}
            \frac{\Gamma(1-\epsilon)\Gamma(1+\epsilon)\Gamma(-\epsilon)^2}
                 {\Gamma(1-2\epsilon)}\,.
    \end{align*}

    \task
    At $\ell=2$, we need two cuts for $\U$ and three for $\F$
    \begin{align*}
        \U &
        = \underbrace{\cutD{\baseB}{b1}{0,0}{ 90}}_{x_1x_2}
        + \underbrace{\cutD{\baseB}{b2}{0,0}{ 90}}_{x_2x_3}
        + \underbrace{
            \begin{gathered}
            \begin{tikzpicture}
            \baseB
            \drawcut{b1}{90}{0.3}
            \drawcut{b2}{90}{0.3}
            \end{tikzpicture}
            \end{gathered}
        }_{x_1x_3}\,,
        \\
        \F &
        = \underbrace{\cutT{\baseB}{0,0}{90}}_{-s x_1x_2x_3}\,.
    \end{align*}
    Hence, the integral is
    \begin{align*}
        I_2 &
        = -\Gamma(1-\epsilon)^2\Gamma(2\epsilon-1)
            \int\D x_1\D x_2\D x_3\delta(\cdots)
            \frac{(x_1x_2+x_2x_3+x_1x_3)^{-3+3\epsilon}}
                 {(-s)^{1+2\epsilon}x_2^{1+2\epsilon}x_3^{1+2\epsilon}}
        \\&
        = -(-s)^{1-2\epsilon}
            \frac{\Gamma(1-\epsilon)^5\Gamma(-1+2\epsilon)}
                 {\Gamma(3-3\epsilon)}\,.
    \end{align*}

    \task
    Using the same methods as above,
    \begin{align*}
    \U &= x_1 x_2 + x_1 x_3 + x_2 x_3 + x_1 x_4 + x_2 x_4\,,\\
    \F &= (-s) x_1 x_3 x_4 + (-s) x_2 x_3 x_4\,.
    \end{align*}
    We set $\delta(1-x_1)$ and integrate first $x_3$, then $x_4$, and finally $x_2$
    \begin{align*}
        I_3 &
        = \Gamma(1-\epsilon)^2\Gamma(2\epsilon)
            \int\D x_1\D x_2\D x_3\D x_4\delta(\cdots)
            \frac{(x_1 x_2 + x_1 x_3 + x_2 x_3 + x_1 x_4 + x_2 x_4)^{-2+3\epsilon}}
                 {(-s)^{2\epsilon}(x_1+x_2)^{2\epsilon}x_3^{2\epsilon}x_4^{2\epsilon}}
        \\&
        = (-s)^{-2\epsilon}
            \frac{\Gamma(1-2\epsilon)^2\Gamma(1-\epsilon)^4
                    \Gamma(\epsilon)\Gamma(2\epsilon)}
                 {\Gamma(2-3\epsilon)\Gamma(2-2\epsilon)}\,.
    \end{align*}

    \task
    \begin{align*}
    \U &= x_1 x_3 + x_2 x_3 + x_1 x_4 + x_2 x_4 + x_3 x_4\,,\\
    \F &= (-s) x_1 x_2 x_3 + (-s) x_1 x_2 x_4 (-s) x_1 x_3 x_4\,.
    \end{align*}
    We set $\delta(1-x_4)$ and integrate first $x_1$, then $x_2$, and finally $x_3$
    \begin{align*}
        I_4 &
        = \Gamma(1-\epsilon)^2\Gamma(2\epsilon)
            \int\D x_1\D x_2\D x_3\D x_4\delta(\cdots)
            \frac{(x_1 x_3 + x_2 x_3 + x_1 x_4 + x_2 x_4 + x_3 x_4)^{-2+3\epsilon}}
                 {(-s)^{2\epsilon}x_1^{2\epsilon}
                    (x_2 x_3 + x_2 x_4 + x_3 x_4)^{2\epsilon}}
        \\&
        = (-s)^{-2\epsilon}
            \frac{\Gamma(1-2\epsilon)\Gamma(1-\epsilon)^5
                    \Gamma(\epsilon)\Gamma(2\epsilon)}
                 {\Gamma(2-3\epsilon)\Gamma(2-2\epsilon)\Gamma(1+\epsilon)}\,.
    \end{align*}
\end{subtasks}\end{solution}

\turnpage

\exercise{Sunset integral}

For general masses the sunset integral of 2b cannot be solved at two-loop.
However, in the example above with massless internal lines and $p^2\neq 0$ it was rather easy.

\begin{subtasks}
    \task
    This is also true at three loop. Show that
    \begin{align*}
    I_{\ell=3} = \begin{gathered}
    \begin{tikzpicture}[baseline=-2pt]
        \draw (-1.5, 0) -- (-1,0);
        \draw[dashed] (0,0) circle(1);
        \draw[dashed] (-1,0) arc (120:60:2);
        \draw[dashed] (-1,0) arc (60:120:-2);
        \draw (1.5, 0) -- (+1,0);
    \end{tikzpicture}
    \end{gathered}
    = (-p^2)^{2-3\epsilon}
      \frac{\Gamma(1-\epsilon)^7\Gamma(-2+3\epsilon)}{\Gamma(4-4\epsilon)}\,.
    \end{align*}

    \task
    Find a recurrence relation for the $\ell$-loop graph polynomials $\F_\ell$ and $\U_\ell$ as a function of $\F_{\ell-1}$ and $\U_{\ell-1}$.
    Then construct the $\ell$-loop sunset $I_\ell$, integrate over one Feynman parameter, and relate the result to $I_{\ell-1}$.
    Finally, solve the resulting recurence relation to obtain
    \begin{align*}
        I_{\ell} = \begin{gathered}
        \begin{tikzpicture}[baseline=-2pt]
            \draw (-1.5, 0) -- (-1,0);
            \draw[dashed] (0,0) circle(1);
            \draw[dashed] (-1,0) arc (30:150:{-2/sqrt(3)});
            \draw[dashed] (-1,0) arc (150:30:{+2/sqrt(3)});
            \node at (0,0) {$\vdots$};
            \draw (1.5, 0) -- (+1,0);
        \end{tikzpicture}
        \end{gathered}
        = (-1)^{\ell+1}
          (-p^2)^{\ell-1-\ell\epsilon}
          \frac{\Gamma(1-\epsilon)^{2\ell+1}
                \Gamma\big(1-\ell(1-\epsilon)\big)}
               {\Gamma\big((1+\ell)(1-\epsilon)\big)}
    \end{align*}

    \task
    Write code to calculate $I_\ell$ using only the algorithm for $\U$ and $\F$ and the master formula.
    {\it (for the adventurous)}

    \task
    Use the optical theorem to relate $\Im I_\ell$ to the phase space volume for a $1\to n$ decay
    \begin{align*}
        \int\D{\rm PS}^{1\to n} = \frac{s^{n-2}}{2(4\pi)^{2n-3}\Gamma(n)\Gamma(n-1)}
        \,.
    \end{align*}
\end{subtasks}


\begin{solution}\begin{subtasks}
    \task
    We have
    \begin{align*}
        \U_{\ell=3} &= x_1 x_2 x_3 + x_1 x_2 x_4 + x_1 x_3 x_4 + x_2 x_3 x_4\,,\\
        \F_{\ell=3} &= (-p^2) x_1 x_2 x_3 x_4\,.
    \end{align*}
    For simplicity we set $p^2=-1$.
    Hence,
    \begin{align*}
        I_{\ell=3}
           = \Gamma(1-\epsilon)^3\Gamma(-2+3\epsilon)
              \int\D x_1\D x_2\D x_3\D x_4\delta(\cdots)
              \U^{-4+4\epsilon}\F^{2-3\epsilon}
            = \frac{\Gamma(1-\epsilon)^7\Gamma(-2+3\epsilon)}
                   {\Gamma(4-4\epsilon)}\,.
    \end{align*}

    \task
    From what we have seen we can guess that
    \begin{align*}
    \U_\ell &= \prod_{i=1}^{\ell} x_i + \U_{\ell-1}x_{\ell+1}\,,\\
    \F_\ell &= \prod_{i=1}^{\ell+1} x_i\,
    \end{align*}
    Hence, using our master formula with $r=\ell+1$
    \begin{align*}
        I_\ell &
        = (-1)^{\ell+1} \Gamma(1-\epsilon)^\ell
                        \Gamma\big(1-(1-\epsilon)\ell\big)
            \int\D x_1\cdots \D x_{\ell+1} \delta(\cdots)
            \U^{(1+\ell)(\epsilon-1)}\F^{-1 + (1 - \epsilon) \ell}
        \\&
        = (-1)^{\ell+1} \Gamma(1-\epsilon)^\ell
                        \Gamma\big(1-(1-\epsilon)\ell\big)
            \int
            \Bigg(
                \prod_{i=2}^{\ell+1} \D x_i\,x_i^{-1 + (1 - \epsilon) \ell}
            \Bigg)
            \Bigg(
                \prod_{i=2}^{\ell} x_i + \U_{\ell-1}x_{\ell+1}
            \Bigg)^{(1+\ell)(\epsilon-1)}
        \,,
    \end{align*}
    where we have used $\delta(1-x_1)$.
    Since $\U_{\ell-1}$ does not depend on $x_{\ell+1}$, we can solve this integration
    \begin{align*}
        I_\ell &
        = (-1)^{\ell+1} \Gamma\big(1-(1-\epsilon)\ell\big)
          \frac{\Gamma(1-\epsilon)^{\ell+1}\Gamma(\ell-\ell \epsilon)}
               {\Gamma\big((1-\epsilon)(1+\ell)\big)}
          \int
            \Bigg(
                \prod_{i=2}^\ell \D x_i\,x_i^{(1+\ell)(\epsilon-1)}
            \Bigg)
            \U_{\ell-1}^{\ell(\epsilon-1)}
        \\&
        = - \Gamma(1-\epsilon)^2
           \underbrace{
               \frac{\Gamma\big(1-\ell(1-\epsilon)\big)}
                    {\Gamma\big((1+\ell)(1-\epsilon)\big)}
           }_{a_\ell}
           \underbrace{
               \frac{\Gamma(\ell-\ell\epsilon)}
                    {\Gamma\big(1+(1-\ell)(1-\epsilon)\big)}
           }_{1/a_{\ell-1}}
           I_{\ell-1}\,.
    \end{align*}
    This pattern of $a_\ell/a_{\ell-1}$ means that we can telescope the recurrence relation and find
    \begin{align*}
        I_\ell &
        = - \Gamma(1-\epsilon)^2\frac{a_\ell}{a_{\ell-1}}I_{\ell-1}
        = -(-1)^\ell\Gamma(1-\epsilon)^{2\ell-2}
           \frac{a_{\ell  }}{a_{\ell-1}}
           \frac{a_{\ell-1}}{a_{\ell-2}}
           \cdots
           \frac{a_{2     }}{a_{     1}}
           I_1
        \\&
        = -(-1)^\ell\Gamma(1-\epsilon)^{2\ell-2}
          \frac{a_\ell}{a_1} I_1\,.
    \end{align*}
    With $I_1 = \Gamma(1-\epsilon)^3\Gamma(\epsilon)/\Gamma(2-2\epsilon)$ we arrive at
    \begin{align*}
        I_\ell &
        = (-1)^{\ell+1}
          (-p^2)^{\ell-1-\ell\epsilon}
          \frac{\Gamma(1-\epsilon)^{2\ell+1}
                \Gamma\big(1-\ell(1-\epsilon)\big)}
               {\Gamma\big((1+\ell)(1-\epsilon)\big)}
    \end{align*}
    after restoring the mass dimension of the integral.

    \task
    We have the optical theorem
    \begin{align*}
        \int\D{\rm PS}^{1\to (\ell+1)} = -2 \Im I_\ell\,.
    \end{align*}
    Restoring the $1/(16\pi^2)$ per loop order
    \begin{align*}
        \int\D{\rm PS}^{1\to (\ell+1)} =
          -2\Big(\frac1{16\pi^2}\Big)^\ell
          \Im\Bigg(
          (-s)^{\ell-1-\ell\epsilon}
          \frac{\Gamma(1-\epsilon)^{2\ell+1}
                \Gamma\big(1-\ell(1-\epsilon)\big)}
               {\Gamma\big((1+\ell)(1-\epsilon)\big)}
          \Bigg)_{\epsilon\to0}
          \,.
    \end{align*}
    With $\Im(-s)^{a\epsilon} \sim a\pi \epsilon$ and $\Gamma(1-\ell(1-\epsilon)) \sim (-1)^{\ell+1}/\epsilon/\Gamma(1+\ell)$
    \begin{align*}
        \int\D{\rm PS}^{1\to (\ell+1)} =
          2\Big(\frac1{16\pi^2}\Big)^\ell
          \ell\pi\epsilon(-s)^{\ell-1}
          \frac{(-1)^{\ell+1}}{\epsilon \Gamma(1+\ell)^2}
        = \frac{s^{\ell-1}}{2(4\pi)^{2\ell-1}\Gamma(1+\ell)\Gamma(\ell)}
    \end{align*}

\end{subtasks}\end{solution}
\end{document}
