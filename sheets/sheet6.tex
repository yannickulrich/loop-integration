 %PDFLATEX
\documentclass{homework}
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath}
\sheetnr{6}
\lecturer{Dr~Y.~Ulrich}
\lecture{Practical guide to loop integration}
\term{Summer Term 2024}
\website{https://yannickulrich.com/loop-integration}
\logo{bern.pdf}
\issued{21 May 2024}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing,positioning,decorations.pathreplacing,shapes,calc}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }
\tikzset{
    photon/.style={decorate, decoration={snake,amplitude=1pt,segment length=6pt}}
}

\usepackage{listings}
\lstset{ %
  basicstyle=\ttfamily,
  keywordstyle=\color{Aquamarine4},
  numberstyle=\color{DarkOrange},
  stringstyle=\color{SkyBlue4},
  commentstyle=\color{Blue2},
  morecomment=[l]{!\ },% Comment only with space after !
  escapeinside={(*@}{@*)},
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}
}
\lstdefinestyle{yaml}{
  basicstyle=\color{Aquamarine4}\ttfamily,
  rulecolor=\color{black},
  string=[s]{"}{"},
  stringstyle=\color{SkyBlue4},
  comment=[l]{:},
  commentstyle=\color{black},
  morecomment=[l]{-}
}

\newcommand\D{\mathrm{d}}
\newcommand\U{\mathcal{U}}
\newcommand\F{\mathcal{F}}

\begin{document}
\maketitle

\exercise{Multidimensional Frobenius method}
In this problem we will explore the Frobenius method.
Consider the following system of differential equations
\begin{align}
    \frac{\partial \vec f}{\partial x} =
    \underbrace{
        \begin{pmatrix}
            \frac{2 - x}{1 - x} & -\frac{1}{1 - x}\\
            -x & \frac{x}{1 - x}
        \end{pmatrix}
    }_{M}
    \vec f\,.
    \label{eq:eq}
\end{align}
A solution of this is given by
\begin{align}
    \vec f = \Big(\frac{\log(1-x)}{1-x}, \frac1{1-x}+\log(1-x)\Big)^T\,.
    \label{eq:sol}
\end{align}
You may use this solution step-by-step to verify your working though in a real-life example, you would obviously not know it.

\begin{subtasks}
    \task
    Construct the matrices $M^{(j)}$ for $j=0$, $j=1$, and $j=2$.
    Write down the matrices $\bar M$ and $\tilde M$

    \task
    Find a vector $\vec c$ from the left-nullspace of $\tilde M$ such that $\vec c\tilde M = 0$.

    \task
    Make an ansatz for $f_1 = x^r\sum x^i a_i$ and calculate $\vec c\cdot(f_1, \partial_x f_1, \partial_x^2 f_1)$.
    Find the biggest $r$, $a_1$, $a_2$, and $a_3$.

    \task
    Construct $\vec f$ by inverting $\tilde M$.

\end{subtasks}

\begin{solution}
    We begin by verifying that \eqref{eq:sol} is indeed a valid solution of \eqref{eq:eq}.
    \begin{subtasks}
        \task
        Using the recursive definition
        \begin{align}
            M^{(0)} = 1
            \qquad\text{and}\qquad
            M^{(j)} = \frac{\partial M^{(j-1)}}{\partial x} - M^{(j-1)}M\,,
        \end{align}
        we have
        \begin{align}
            M^{(0)} &= \begin{pmatrix}1&0\\0&1\end{pmatrix}
            \\
            M^{(1)} &= M\\
            M^{(2)} &= \begin{pmatrix}
                \frac{5-3x}{(1-x)^2} & -\frac{3}{(1-x)^2} \\
                \frac{1+x}{1-x} & \frac{1+x}{(1-x)^2}
            \end{pmatrix}\,.
        \end{align}
        With this we can just read of
        \begin{align}
            \bar M &= \begin{pmatrix}
                1&0\\\frac{2-x}{1-x} & \frac1{x-1}
            \end{pmatrix}\\
            \tilde M &= \begin{pmatrix}
                1&0\\\frac{2-x}{1-x} & \frac1{x-1}\\
                \frac{5-3x}{(1-x)^2} & -\frac{3}{(1-x)^2}
            \end{pmatrix}\,.
        \end{align}
        At this point we may verify that
        \begin{align}
            \begin{split}
                \frac{\partial^j \vec f}{\partial x^j} &= M^{(j)} \vec f\,,\\
                \Big(f_1, \partial_x f_1, \partial_x^2 f_1\Big) &= \tilde M \vec f\,.
            \end{split}
        \end{align}

        \task
        It is easy to see that
        \begin{align}
            \vec c = \begin{pmatrix}
                \frac{1}{1-2x+x^2} \\
                \frac{-3+3x}{1-2x+x^2}
                \\ 1
            \end{pmatrix}
        \end{align}
        is a valid and properly normalised solution.
        You may want to verify that $\vec c\cdot(f_1, \partial_x f_1, \partial_x^2 f_1) = 0$.

        \task
        Expanding and collecting terms we have with $a_0=1$
        \begin{align}
            x^r\Bigg(
                \frac{(r-1)r}{x^2}
                + \mathcal{O}(x^{-1})
            \Bigg) = 0\,.
        \end{align}
        The valid solutions are $r=0$ or $r=1$.
        We now expand higher with $r=1$
        \begin{align}
            (-3 + 2 a_1)
            + (-2 - 6 a_1 + 6 a_2) x 
            + (-1 - 5 a_1 - 9 a_2 + 12 a_3) x^2
            + \mathcal{O}(x^3) = 0
        \end{align}
        to see that $a_1=3/2$, $a_2=11/6$, and $a_3 = 25/12$.
        We can also explitly verify that expanding \eqref{eq:sol} gives these expansions coefficients up to an overall sign.

        \task
        The inverse matrix is
        \begin{align}
            \tilde M^{-1} =
                \begin{pmatrix}1&0\\2-x&-1+x\end{pmatrix}\,.
        \end{align}
        And therefore $\vec f=\tilde M^{-1} (f_1,\partial_x f_1)^T$
        \begin{align}
            f_2 = -1-\frac{x^2}2-\frac{2x^3}3\,.
        \end{align}

    \end{subtasks}
\end{solution}

\exercise{Two-mass sunset diagram in $d=2-2\epsilon$}

Consider the following integral
\begin{align}
    I_{\alpha\beta\gamma} =
    \begin{gathered}
    \begin{tikzpicture}[scale=2/3]
        \draw[thin] (0,0) circle(1);
        \draw[thick] (-2, 0) -- (2,0);
    \end{tikzpicture}
    \end{gathered}
    = \int\frac{[\D k_1][\D k_2]}{
        \big[k_1^2-m_1^2\big]^\alpha
        \big[k_2^2-m_1^2\big]^\beta
        \big[(k_1+k_2+p)^2-m_2^2\big]^\gamma
    }\,,
\end{align}
with $p^2=m_2^2\neq m_1^2$ in $d=2-2\epsilon$ dimensions.
We use the integrals
\begin{align}
    \vec I = \big(m_2^2\big)^{4-d}\epsilon^2
        \Big(
            z^{-4}I_{220},
            z^{-2}I_{202},
            z^{-4}I_{211},
            z^{-1}I_{112}
        \Big)^T
\end{align}
and the variable $z=m_2/m_1$.

\begin{subtasks}
    \task
    Show that the differential equation matrix is precanonical
    \begin{align}
        \partial_z\vec I &= \begin{pmatrix}
             \frac{4\epsilon}{z} & 0 & 0 & 0 \\
             0 & \frac{2\epsilon}{z} & 0 & 0 \\
             \frac{1}{z(1-z^2)} &  -\frac1{z(1-z^2)} & \frac{z^2 + (6-4z^2)\epsilon}{z(1-z^2)} & -\frac{1+2\epsilon}{z^2(1-z^2)} \\
             -\frac{z^2}{1-z^2} & \frac{1}{1-z^2} &-\frac{z^2(1+2\epsilon)}{1-z^2} & \frac{z^2 + 2\epsilon}{z(1-z^2)}
        \end{pmatrix}\vec I\,.
        \label{eq:original}
    \end{align}
\end{subtasks}
\turnpage

The boundary conditions can be fixed at $z=1$
\begin{subequations}\label{eq:boundary}
    \begin{align}
        I_1 = I_2 &=
            1+2\epsilon+(1+2\zeta_2)\epsilon^2 + \mathcal{O}(\epsilon^3)\,,\\
        \begin{split}
            I_3 = I_4 &=
                \frac{3}{8}\zeta_2 + \frac14
                +\Big(
                    + \frac{21}{16}\zeta_3
                    - \frac{9}{4}\log2\zeta_2
                    + \frac{9}{8}\zeta_2
                    - \frac{1}{2}
                \Big)\epsilon
                +\Big(
                    - \frac{63}{16}\zeta_4
                    + 9{\rm Li}_4(\tfrac12)
                    + \frac92\log2^2\zeta_2
                    + \frac38\log2^4\\&\qquad
                    + \frac{63}{16}\zeta_3
                    - \frac{27}{4}\log2\zeta_2
                    + \frac12 \zeta_2
                    + 1
                    \Big)\epsilon^2+ \mathcal{O}(\epsilon^3)\,.
        \end{split}
    \end{align}
\end{subequations}

We will now try to derive a series expression around $z=1$.
\begin{subtasks}
    \setcounter{enumi}{1}
    \task
        Show~\eqref{eq:boundary}.
        {\it (for the adventurous)}

    \task
        Consider first the homogenous subsystem for $\vec I'=(I_3,I_4)$ with $x=1-z$
        \begin{align}
            \partial_x \vec I' = \begin{pmatrix}
                   \frac{ 1-x   }{(x-2) x} & -\frac{1    }{(x-2)(x-1)^2 x} \\
                  -\frac{(1-x)^2}{(x-2) x} &  \frac{1-x}{(x-2)          x}
            \end{pmatrix}
            \vec I'\,.
            \label{eq:hom}
        \end{align}
        Construct $\tilde M$ and $\bar M$
        \begin{align}
            \bar M = \begin{pmatrix}
                1 & 0 \\
                 \frac{1-x}{(x-2) x} & -\frac{1}{(x-2) (x-1)^2 x} \\
                 \frac{2(x^2-2 x+2)}{(x-2)^2 x^2} & \frac{2(3 x^2-6
   x+2)}{(x-2)^2 (x-1)^3 x^2} \\
            \end{pmatrix}
        \end{align}

    \task
        Find a solution $\vec c$ and write down the second-order differential equation for $I_3$.
        Make a Frobenius ansatz.
        What values of $r$ are allowed?

    \task
        Build a series solution for $r=0$.

    \task
        Construct the first-order differential equation to find the other solution for
        \begin{align}
            \vec c_2 = \Bigg(\frac4x \frac{1-x}{2-x},1\Bigg)\,.
        \end{align}

    \task
        Build a series solution for $r=-2$ and write down the full homogenous solution for the original differential equation.

    \task
        Now we can consider the inhomogeneity.
        Show that for the $k$-th order in $\epsilon$, it becomes
        \begin{align}
            \small
            \vec{\mathcal{I}}^{(k)} =
                \frac1{x(x-2)}\Bigg[
                \begin{pmatrix}
                    \frac1{1-x} & -\frac1{1-x}\\
                    -(1-x)^2 & 1
                \end{pmatrix}\cdot\Big(I_1^{(k)},I_2^{(k)}\Big)
                +
                \begin{pmatrix}
                     \frac{4 x^2-8 x-2}{x-1} & -\frac{2}{(x-1)^2} \\
                     -2 (x-1)^2 & -\frac{2}{x-1} \\
                \end{pmatrix}\cdot\Big(I_3^{(k-1)},I_4^{(k-1)}\Big)
                \Bigg]
                \,.
        \end{align}

    \task
        Now construct a solution up to $\mathcal{O}(\epsilon^2)$.
        Evaluate the result at $z=0.5$.
\end{subtasks}

\begin{solution}
A Mathematica calculation of this problem is given on the website.
    \begin{subtasks}
        \task
            It is trivial to derive a differential equation for eg. $m_2^2$ using {\tt reduze}.
            From there we write
            \begin{align}
                T = (m_2^2)^{d-4} \epsilon^{-2} {\rm diag}\Big(
                    \frac{m_2^4}{m_1^4} ,
                    \frac{m_2^2}{m_1^2} ,
                    \frac{m_2^4}{m_1^4} ,
                    \frac{m_2}{m_1} \Big)
            \end{align}
            and hence $A_z$.

        \task
            It is clear that $I_1=I_2$ and $I_3=I_4$ from symmetry for $z=1$.
            $I_1$ is just a tadpole and trivially just
            \begin{align}
                I_1=I_2\Big|_{z=1} = (1 + \epsilon)^2\Gamma[1 - \epsilon]^2 \Gamma[1 + \epsilon]^2\,.
            \end{align}
            For $I_3$ we calculate the graph polynomials
            \begin{align}
                \mathcal{U} &= x_1 x_2+x_3 x_2+x_1 x_3\,,\\
                \mathcal{F} &= -(x_1+x_2)\mathcal{U} - (x_1+x_2)x_3^2\,.
            \end{align}
            Therefore the integral is
            \begin{align}
                I_3&
                = (-1)^{2\epsilon}\Gamma(1-\epsilon)^2\Gamma(2+2\epsilon)\int\D\vec x\ x_1
                    \frac{\mathcal{U}^{1+3\epsilon}}{\mathcal{F}^{2+2\epsilon}}\\&
                = \Gamma(1-\epsilon)^2\Gamma(2+2\epsilon)\int\D\vec x\ x_1\mathcal{U}^{1+3\epsilon}(x_1+x_2)^{-2-2\epsilon}(\mathcal{U}+x_3^2)^{-2-2\epsilon}\,.
            \end{align}
            Using the Mellin-Barnes theorem to split $\mathcal{U}+x_3^2$
            \begin{align}
                I_3&
                = \Gamma(1-\epsilon)^2\int_{-i\infty}^{+i\infty}\D\sigma\int\D\vec x
                    \Gamma(-\sigma)\Gamma(2+\sigma+2\epsilon)\,\,
                    x_1
                    x_3^{2\sigma}
                    (x_1+x_2)^{-2-2\epsilon}
                    \mathcal{U}^{-1-\sigma+\epsilon}\,.
            \end{align}
            We can now solve the $x_3$ and $x_2$ integrals
            \begin{align}
                I_3 = \Gamma (1-\epsilon )^2\int_{-i\infty}^{+i\infty}\D\sigma
                \frac{\Gamma (-\sigma ) \Gamma (2 \sigma +1)  \Gamma (-\epsilon -\sigma ) \Gamma (\epsilon
   +\sigma +1) \Gamma (\epsilon +\sigma +2) \Gamma (2 \epsilon +\sigma +2)}{\Gamma (-\epsilon +\sigma +1) \Gamma
   (2 \epsilon +2 \sigma +3)}\,.
            \end{align}
            This integral can be solved in terms of ${}_3F_2$ functions by considering the residues at $\sigma=n$ and $\sigma=n-\epsilon$.
            Alternatively, we can solve it numerically for $\sigma=1/3 + i\,x$
            \begin{align}
                \begin{split}
                I_3 =
                    &+{ 0.8668502750680849136771556874922594459571062129525494141508343360137528}\\
                    &+{ 0.3628423366548035363008187167277704240129460044840854565583099812335021}\epsilon\\
                    &+{ 2.897979437584699485862761102760735004697735930030264157054679832034451 }\epsilon^2\\
                    &+\mathcal{O}(\epsilon^3)\,.
                \end{split}
            \end{align}
            We have evaluated the integral to 70 digits which is required to perform the rational number fit.

        \task
            It is trivial to find the homogenous matrix $M$ as given in~\eqref{eq:hom}.
            From there, constructing $\bar M$ is just a matter of calculating deriviatives.

        \task
            We find $p=2$
            \begin{align}
                \vec c = \bigg(
                    \frac2{x-2} - \frac2x,
                    \frac2{x-2} + \frac2{x-1} + \frac2x,
                    1\bigg)\,.
            \end{align}
            Making the ansatz and expanding to the first order, we find $r(1+r)=0$.
            We hence choose $r=0$.

        \task
            It is easy to see that $a_i=1$ for all $i$ and hence $f_1 = 1/(1-x)$ is a solution.

        \task
            We have
            \begin{align}
                \begin{split}
                (c_2)_j &= \sum_{n=0}^{p-1-j}\binom{p-n}{1+j} c_{p-n+1} \frac{\partial^{p-n-j-1} f_1}{\partial x^{p-n-j-1}}\\
                &= \Bigg(\frac{6 x^2-12 x+4}{(1-x) x (x^2-3 x+2)}+\frac{2}{(1-x)^2},\frac{1}{1-x}\Bigg)\,,
                \end{split}
            \end{align}
            which is equivalent to the given $\vec c_2$.

        \task
            The solution for $\vec c_2$ is slightly more complicated but can still be written in closed form
            \begin{align}
                \begin{split}
                f_1' &=
                \frac{1}{x^2}
                +\frac{1}{x}
                +\frac{3}{4}
                +\frac{x}{2}
                +\frac{5 x^2}{16}
                +\frac{3 x^3}{16}
                +\frac{7 x^4}{64}
                +\frac{x^5}{16}
                +\frac{9 x^6}{256}
                +\frac{5 x^7}{256}
                +\frac{11 x^8}{1024}
                +\frac{3 x^9}{512}
                +\mathcal{O}(x^{10})\\&
                    = \sum_{i=0}^\infty \frac{i+1}{2^i}x^{i-2} = \frac{4}{x^2(x-2)^2}
                \end{split}
            \end{align}
            The integrated $f_1'$ can also be found easily
            \begin{align}
                \begin{split}
                f_1 &= f_0\int\D x f_1' = f_0 \int\D x = \frac1{1-x}\Big(-\frac1x + \log x+\sum_{i=1}^\infty\frac{2+i}{2i}\frac{x^i}{2^i}\Big) \\
                &= -\frac{1}{x}
                   +\log x-1
                    +x \Big(\log x-\frac{1}{4}\Big)
                    +x^2 \log x
                    +x^3 \Big(\log (x)+\frac{5}{48}\Big)
                    +x^4 \Big(\log x+\frac{29}{192}\Big)
                    +...
                \end{split}
            \end{align}
            The solution matrix $F$ can be found as
            \begin{align}
                \small
                F = \begin{pmatrix}
                     1+x+x^2+x^3 &
                     -\frac{1}{x}+\log(x)-1+x \big(\log x-\frac{1}{4}\big)+x^2 \log x+x^3\big(\log x+\frac{5}{48}\big) \\
                     1+O\big(x^4\big) &
                     \frac{1}{x}+\log(x)-1+\frac{x}{4}-\frac{x^3}{48}
                \end{pmatrix}
                +\mathcal{O}(x^4)\,.
            \end{align}

        \task
            There are two contributions to $\mathcal{I}$
            \begin{itemize}
                \item
                    the lower sector contributions from $I_1$ and $I_2$ that are governed by the slices of $(A_x)_{3..4, 1..2}$.
                    These can be read of from the $\epsilon^0$ part of \eqref{eq:original} after substituting $z\to1-x$.

                \item
                    the lower order contribution from $I_3$ and $I_4$ that are governed by the $\mathcal{O}(\epsilon)$ part of $(A_x)_{3..4,3..4}$.

            \end{itemize}

        \task
            We calculate
            \begin{align}
                G^{(k)} = F\cdot \Bigg(
                    \int\D x F^{-1}
                        \frac12\Big(\vec{\mathcal{I}}^{(k)},\vec{\mathcal{I}}^{(k)}\Big)
                        + {\rm diag}(c_1^{(k)}, c_2^{(k)})\Bigg)
            \end{align}
            order-by-order and then obtain $I_3=G_{11}+G_{12}$ and $I_4=G{21}+G_{22}$.
            We can then take the limit $x\to0$ to match the integration constants.
            Since we have a series expansion, this is fairly trivial.
            For $k=0$,
            \begin{align}
                G^{(0)} = \begin{pmatrix}
                    c_1^{(0)} & -\frac{c_2^{(0)}}{x} + c_2^{(0)} (\log x-1) \\
                    c_1^{(0)} &  \frac{c_2^{(0)}}{x} + c_2^{(0)} (\log x-1)
                \end{pmatrix} + \mathcal{O}(x)\,.
            \end{align}
            Since the limit $x\to0$ is finite, $c_2^{(0)}=0$.
            Matching~\eqref{eq:boundary}, we then find $c_1^{(0)} = \frac14+\frac{\pi^2}{16}$.

            Pluggin in numbers we find
            \begin{align}
                \vec I =
                \begin{pmatrix}
                    1     \\
                    1     \\
                    1.3581\\
                    0.4669\\
                \end{pmatrix} + \begin{pmatrix}
                    -0.7725\\
                     0.6137\\
                     0.4189\\
                     0.3800\\
                \end{pmatrix} \epsilon + \begin{pmatrix}
                     2.5883\\
                     2.4781\\
                     3.3107\\
                     1.1768\\
                \end{pmatrix}\epsilon^2 + \begin{pmatrix}
                    -1.1793\\
                     2.1104\\
                     1.9080\\
                     1.2225\\
                \end{pmatrix}\epsilon^3+\mathcal{O}(\epsilon^4)
            \end{align}
    \end{subtasks}
\end{solution}
\end{document}
