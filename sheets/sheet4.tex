 %PDFLATEX
\documentclass{homework}
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath}
\sheetnr{4}
\lecturer{Dr~Y.~Ulrich}
\lecture{Practical guide to loop integration}
\term{Summer Term 2024}
\website{https://yannickulrich.com/loop-integration}
\logo{bern.pdf}
\issued{7 May 2024}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing,positioning,decorations.pathreplacing,shapes,calc}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }
\tikzset{
    photon/.style={decorate, decoration={snake,amplitude=1pt,segment length=6pt}}
}

\usepackage{listings}
\lstset{ %
  basicstyle=\ttfamily,
  keywordstyle=\color{Aquamarine4},
  numberstyle=\color{DarkOrange},
  stringstyle=\color{SkyBlue4},
  commentstyle=\color{Blue2},
  morecomment=[l]{!\ },% Comment only with space after !
  escapeinside={(*@}{@*)},
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}
}

\newcommand\D{\mathrm{d}}
\newcommand\U{\mathcal{U}}
\newcommand\F{\mathcal{F}}

\begin{document}
\maketitle

\exercise{top quark decay}

Consider the following integral that appears in the two-loop calculation of the top decay
\begin{align}
\begin{gathered}
\begin{tikzpicture}[baseline=+1cm]
    \draw[photon] (-0.5,0) node[left] {$(p+q)^2=s$} -- (0,0);
    \draw[dashed](0,0) -- (30:2) node [right] {$q^2\ll s,p^2$};
    \draw        (0,0) -- (-30:2) node [right] {$p^2$};
    \centerarc[](0,0)(210:150:-1.7);
    \centerarc[]({1.7*sqrt(3)},0)(210:150:1.7);
    \fill (30:0.85) circle(3.5pt);
\end{tikzpicture}
\end{gathered}
\end{align}
The dashed line corresponds to light $b$ quark and the solid line to the heavy $t$ quark.

\begin{subtasks}
    \task
    Find the momentum regions that contribute to this process.

    \task
    Show that the hard region does in fact equal the same calculation with massless $b$ quarks, i.e. $m=0$.

    \task
    The result for $m=0$ can be found in the literature as
    \begin{align}\begin{split}
        \begin{gathered}
        \begin{tikzpicture}[baseline=+1cm]
            \draw[photon] (-0.5,0) node[left] {$s$} -- (0,0);
            \draw[dashed](0,0) -- (30:2) node [right] {$0$};
            \draw        (0,0) -- (-30:2) node [right] {$M^2$};
            \centerarc[](0,0)(210:150:-1.7);
            \centerarc[]({1.7*sqrt(3)},0)(210:150:1.7);
            \fill (30:0.85) circle(3.5pt);
        \end{tikzpicture}
        \end{gathered}
        = &\frac{(M^2)^{-2\epsilon}}{M^2-s}
            \Bigg[
                \frac1{\epsilon^2}
                +\frac1\epsilon\frac{1+y}yH_1(y)
                +4
                \\&
                +\frac{2(1+y)}y\Big(H_1(y)+H_{1,1}(y)\Big)
                +\frac2y\frac{1+y}{1-y}\Big(H_{0,1}(y)-y\zeta_2\Big)
            \Bigg]\,.
            \label{eq:hard}
    \end{split}\end{align}
    Show by explicit calculation of the remaining regions that the complete integral has no $1/\epsilon^2$ pole but a $\log(m^2/M^2)$.

\end{subtasks}

\begin{solution}
We write the integral as
\begin{align*}
    I = \int[\D k_1][\D k_2]\frac1{
        \big[k_1^2+2k_1\cdot p\big]
        \big[k_1^2-2k_1\cdot q\big]^2
        \big[k_2^2-M^2\big]
        \big[(k_1-k_2)^2-M^2\big]
    }
\end{align*}
with $p^2=M^2$ and $q^2=m^2\sim\lambda^2$.
\begin{subtasks}
    \task
    We have two loop momenta that can scale differently.
    Hence, a region is fully specified by six numbers
    \begin{align*}
        k_1\sim(\lambda^a, \lambda^b, \lambda^c)
        \quad\text{and}\quad
        k_2\sim(\lambda^d, \lambda^e, \lambda^f)\,.
    \end{align*}
    We obviously have a hard region $k_1\sim k_2\sim(1,1,1)$.
    We further have a collinear-hard region $k_1\sim(\lambda^2,1,\lambda)$ and $k_2\sim(1,1,1)$.
    All other regions are scaleless.
    For example the collinear-collinear region $k_1\sim k_2\sim(\lambda^2,1,\lambda)$
    \begin{align*}
        I_{cc} = \frac{\lambda^4}{M^4}\int[\D k_1][\D k_2]
            \frac1{
                \big[2k_1\cdot p_+\big]
                \big[k_1^2 - 2k_1\cdot q\big]
            }=0
    \end{align*}

    \task
    We have with $q=q_-+q_\perp$ with $q_-\sim1$ and $q_\perp\sim\lambda$
    \begin{align*}
        I_{hh} &= \int[\D k_1][\D k_2]\frac1{
            \big[k_1^2+2k_1\cdot p\big]
            \big[k_1^2-2k_1\cdot (q_-+\lambda q_\perp)\big]^2
            \big[k_2^2-M^2\big]
            \big[(k_1-k_2)^2-M^2\big]
        }\\
        &= \int[\D k_1][\D k_2]\frac1{
            \big[k_1^2+2k_1\cdot p\big]
            \big[k_1^2-2k_1\cdot q_-\big]^2
            \big[k_2^2-M^2\big]
            \big[(k_1-k_2)^2-M^2\big]
        } + \mathcal{O}(\lambda)\,.
    \end{align*}
    Since $q_-^2=0$, we now have the massless result evaluated at $q_-$ instead of $q$.

    \task
    The other region, $I_{ch}$, is
    {\footnotesize
    \begin{align*}
        I_{ch} &= \int[\D k_1][\D k_2]\frac1{
            \big[2k_{1-}\cdot p_+\big]
            \big[k_1^2-2(q_-\cdot k_{1+} + q_\perp\cdot k_{1\perp})\big]^2
            \big[k_2^2-M^2\big]
            \big[k_2^2-2k_{1-}\cdot k_{2+}-M^2\big]
        } + \mathcal{O}(\lambda)\,.
    \end{align*}}
    In the scalar products between $q$ and $k_1$ we can just take the full $k_1$ because the $q$ will project out the necessary component.
    The $k_{1-}\cdot k_{2+}$ is, however, a problem.
    {\small
    \begin{align*}
        I_{ch} &= \int[\D k_1][\D k_2]\frac1{
            \big[2k_1\cdot p_+\big]
            \big[k_1^2-2k_1\cdot(q_-+ q_\perp)\big]^2
            \big[k_2^2-M^2\big]
            \big[k_2^2-2k_{1-}\cdot k_{2+}-M^2\big]
        } + \mathcal{O}(\lambda)\,.
    \end{align*}
    }
    By solving first the $k_2$ integration, we write $k_{1-}\cdot k_{2+}=k_{1-}\cdot k_2$
    {\small
    \begin{align*}
        I_{ch} &= (M^2)^{-\epsilon}\Gamma(1-\epsilon)\Gamma(\epsilon)
                \int\D x_1\D x_2
                    \frac{\delta(\cdots)}{(x_1+x_2)^{2}}
                    \int[\D k_1]
                    \frac1{
                        \big[2k_1\cdot p_+\big]
                        \big[k_1^2-2k_1\cdot(q_-+ q_\perp)\big]^2
                    }
             + \mathcal{O}(\lambda)\\
        &= 2^\epsilon(M^2)^{-\epsilon}
                \Gamma(1-\epsilon)^2\Gamma(\epsilon)\Gamma(1+\epsilon)
            \int\D x_1\D x_2 \D y_1\D y_2
                    \frac{\delta(\cdots)\delta(\cdots)}{(x_1+x_2)^{2}}\\&\qquad
                    y_1^{-1+\epsilon}
                    \big(2m^2y_1+(m^2+M^2-s)y_2\big)^{-1-\epsilon}\\
        &= -(M^2)^{-2\epsilon}
            \Big(\frac{m^2}{M^2}\Big)^{-\epsilon}
            \frac{\Gamma(1-\epsilon)^2\Gamma(\epsilon)^2}{m^2+M^2-s}
         = -(M^2)^{-2\epsilon}
            \Big(\frac{m^2}{M^2}\Big)^{-\epsilon}
            \frac{\Gamma(1-\epsilon)^2\Gamma(\epsilon)^2}{M^2-s}
             + \mathcal{O}(\lambda)\\
        &= \frac{(M^2)^{2\epsilon}}{M^2-s}
            \Bigg[-\frac1{\epsilon^2}
                  + \frac1\epsilon\log\frac{m^2}{M^2}
                  + \mathcal{O}(\epsilon^0)
            \Bigg] + \mathcal{O}(\lambda)\,.
    \end{align*}}
    The $1/\epsilon^2$ pole cancels exactly the one in \eqref{eq:hard}

\end{subtasks}
\end{solution}


\exercise{Soft approximation}
\newcommand\rn[1]{\vec r^{\,(#1)}}
\def\base{
    \coordinate (a1) at ( 150:2);
    \coordinate (b1) at (-150:2);
    \coordinate (a2) at ( 150:1.5);
    \coordinate (b2) at (-150:1.5);
    \coordinate (a3) at ( 150:.75);
    \coordinate (b3) at (-150:.75);
    \coordinate (c2) at (1,0);

    \draw[photon](c2) -- (0,0);
    \draw(a1) -- (0,0) -- (b1);
    \draw[photon] (a2) -- (b2);
    \draw[photon] (a3) --+ (30:1) node (c1){};
}

Consider the following diagram
\begin{align}
\begin{gathered}
\begin{tikzpicture}[baseline=+1cm]
\base
\node at (a1) [left] {$p_1$};
\node at (b1) [left] {$p_2$};
\node at (c1) [right] {$p_3$};
\node at (c2) [right] {$Q$};
\end{tikzpicture}
\end{gathered}
= \int[\D k] \frac1{
    \big[k^2\big]
    \big[(k-p_1)^2-m^2\big]
    \big[(k-p_1+p_3)^2-m^2\big]
    \big[(k+p_2)^2-m^2\big]
}
\end{align}
\turnpage
with the energy of the photon soft compared to $s=Q^2$ and $p_1^2 = p_2^2 = m^2 \ll s$.
This can be phrased in a Lorentz invariant way by requiring that all invariants
\begin{align}
\sigma_{i3} = 2p_i\cdot p_3 \sim m \sim \lambda\,.
\end{align}
\begin{subtasks}
    \task
    Use the method of regions in the parametric representation to find all six regions that contribute to this integral.

    \task
    Consider the region $\rn1=(0,-1,-1,1)$, i.e. $\mathcal{P}_1\sim 1$, $\mathcal{P}_2\sim \lambda^{-1}$, $\mathcal{P}_3\sim \lambda^{-1}$, $\mathcal{P}_4\sim \lambda$.
    Show that this integral is not finite in dimensional regularisation.

\end{subtasks}

Such behaviour is not uncommon and usually points to a broken symmetry eg. in SCET.
It is usually addressed by using analytic regularisation, i.e.
\begin{align}
\begin{split}
\int[\D k] \frac1{
    \big[k^2\big]
    \big[(k-p_1)^2-m^2\big]
    \big[(k-p_1+p_3)^2-m^2\big]
    \big[(k+p_2)^2-m^2\big]
}
\\\to
\big(-\nu^2\big)^\eta
\int[\D k] \frac1{
    \big[k^2\big]
    \big[(k-p_1)^2-m^2\big]
    \big[(k-p_1+p_3)^2-m^2\big]^{1+\eta}
    \big[(k+p_2)^2-m^2\big]
}
\,.
\end{split}
\end{align}
We have introduced an additional regulator $\eta$ that we will take to zero as soon as we have added all regions.
Crucially, $\eta\to0$ needs to be done \emph{before} $\epsilon\to0$.

\begin{subtasks}
    \setcounter{enumi}{2}
    \task
    Calculate the integral, up to $\mathcal{O}(\lambda^0)$.
    Add all regions and set $\eta\to0$ and finally $\epsilon\to0$.
\end{subtasks}


\begin{solution}
We have
\begin{align*}
    I = \Gamma(1-\epsilon) \Gamma(2+\epsilon)&
        \int\D x_1\D x_2\D x_3\D x_4\delta(\cdots)
        \big(x_1+x_2+x_3+x_4\big)^{2\epsilon}
        \Big[
            -s (x_2+x_3) x_4\\&
            +m^2 (x_2+x_3+x_4)^2
            +\sigma_{13} (x_1 x_3+2 x_4 x_3+x_2 x_4)
            +\sigma_{23} (x_2+2 x_3) x_4
           \Big]^{-\epsilon -2}
\end{align*}
\begin{subtasks}
    \task
    Using \href{https://www.ttp.kit.edu/~asmirnov/data/asy2.1.m}{\tt asy} and \href{http://www.qhull.org/download/}{\tt QHull}
    \begin{lstlisting}[language=mathematica]
Get["/path/to/asy2.1.m"];
SetOptions[QHull, Executable -> "/path/to/qhull"]
props = {
  k1^2,
  (k1 - p1)^2 - m^2,
  (k1 - p1 + p3)^2 - m^2,
  (k1 + p2)^2 - m^2
};
onshell = {
  p1^2 -> m^2, p2^2 -> m^2, p3^2 -> 0,
  p1 p2 -> 1/2 (-2 m^2 + s - (*@$\sigma$@*)13 - (*@$\sigma$@*)23),
  p1 p3 -> (*@$\sigma$@*)13/2,
  p2 p3 -> (*@$\sigma$@*)23/2
};

AlphaRepExpand[{k1},
  props,
  onshell,
  {(*@$\sigma$@*)13 -> x, (*@$\sigma$@*)23 -> x, m -> x, s -> 1}
]
(* Asy2.1 *)
(* Variables for UF: {k1, p1, p2, p3} *)
(* {{0, -1, -1, 1}, {0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 2}, {0, 2, 2, 0}, {0, 3, 3, 1}} *)
    \end{lstlisting}
    we find
    \begin{align*}
        \rn1 &= (0,-1,-1,1)\,,\\
        \rn2 &= (0, 0, 0,0)\,,\\
        \rn3 &= (0, 0, 0,1)\,,\\
        \rn4 &= (0, 0, 1,2)\,,\\
        \rn5 &= (0, 2, 2,0)\,,\\
        \rn6 &= (0, 3, 3,1)\,.
    \end{align*}

    \task
    In $\rn1$, we have $x_1\sim1$, $x_2\sim x_3\sim\lambda^{-1}$, $x_4\sim m\sim\sigma_{13}\sim\sigma_{23}\sim\lambda$.
    Hence up to a global pre-factor of $\lambda$
    \begin{align*}
    I_1 \propto&
        \int\D x_1\D x_2\D x_3\D x_4\delta(\cdots)
        \big(x_2+x_3\big)^{2\epsilon}
        \Big[
            -s (x_2+x_3) x_4
            +m^2 (x_2+x_3)^2
            +\sigma_{13} x_1 x_3
           \Big]^{-\epsilon -2}\\
    &\propto \int\D x_2\D x_3\delta(\cdots)x_3^{-1}(x_2+x_3)^{-1}\,.
    \end{align*}
    This integral integrates to $\Gamma(0)$ which is not finite.

    \task
    We find after setting the power of the third propagator to $1+\eta$

    \small
    \begin{align*}
        I_1 &= (-\nu^2)^\eta \frac{\Gamma(1-\epsilon) \Gamma(\epsilon+\eta+2)}{\Gamma(\eta+1)}\\&\qquad
            \int\D\vec x\, x_3^\eta  (x_2+x_3)^{\eta+2\epsilon}\Big[m^2 (x_2+x_3)^2-s x_4 (x_2+x_3)+\sigma_{13} x_1 x_3\Big]^{-\eta-\epsilon-2}
        \\
        I_2 &= (-\nu^2)^\eta (-s)^{-\eta -\epsilon -2} \frac{ \Gamma(1-\epsilon)\Gamma(\epsilon+\eta+2)}{\Gamma(\eta+1)}\\&\qquad
            \int\D\vec x\,x_3^\eta x_4^{-\eta-\epsilon-2} (x_2+x_3)^{-\eta-\epsilon-2} (x_1+x_2+x_3+x_4)^{\eta +2 \epsilon }
        \\
        I_3 &= (-\nu^2)^\eta \frac{\Gamma(1-\epsilon) \Gamma(\epsilon+\eta+2)}{\Gamma(\eta +1)}
            \int\D\vec x\,x_3^\eta (x_1+x_2+x_3)^{\eta+2\epsilon} \Big[\sigma_{13} x_1 x_3-s (x_2+x_3) x_4\Big]^{-\eta-\epsilon}
        \\
        I_4 &= (-\nu^2)^\eta \frac{\Gamma(1-\epsilon) \Gamma(\epsilon+\eta+2)}{\Gamma(\eta +1)}
            \int\D\vec x\,x_3^\eta (x_1+x_2)^{\eta+2\epsilon} \Big[m^2 x_2^2-s x_4 x_2+\sigma_{13} x_1 x_3\Big]^{-\eta-\epsilon-2}
        \\
        I_5 &= (-\nu^2)^\eta \frac{\Gamma(1-\epsilon)\Gamma(\epsilon+\eta+2)}{\Gamma(\eta+1)}
            \int\D\vec x\,x_3^\eta  x_4^{-\eta-\epsilon-2} (x_1+x_4)^{\eta+2\epsilon}  \Big[-s(x_2+x_3)+m^2 x_4\Big]^{-\eta-\epsilon-2}
        \\
        I_6 &= (-\nu^2)^\eta \frac{\Gamma(1-\epsilon) \Gamma(\epsilon+\eta+2) }{\Gamma(\eta+1)}
            \int\D\vec x\,x_3^\eta  x_1^{\eta+2\epsilon} \Big[m^2 x_4^2-s (x_2+x_3) x_4+\sigma_{13} x_1 x_3\Big]^{-\eta -\epsilon -2}
    \end{align*}
    All of these integrals can be trivially solved
    \begin{align*}
        I_1 &= \frac1{(-s)\sigma_{13}}\Big(\frac{-\nu^2}{m^2        }\Big)^\eta \Big(\frac{\mu^2}{m^2                         }\Big)^\epsilon\frac{\Gamma(\eta) \Gamma(1-\epsilon) \Gamma(\epsilon+\eta)}{  \Gamma(\eta+1)^2}\\
        I_2 &= \frac1{(-s)(-s)       }\Big(\frac{-\nu^2}{-s         }\Big)^\eta \Big(\frac{\mu^2}{-s                          }\Big)^\epsilon\frac{\Gamma(1-\epsilon) \Gamma(-\epsilon) \Gamma(-\epsilon-\eta-1) \Gamma(\epsilon+\eta+2)}{\Gamma(\eta+2) \Gamma(-2 \epsilon-\eta)}\\
        I_3 &= \frac1{(-s)\sigma_{13}}\Big(\frac{-\nu^2}{\sigma_{13}}\Big)^\eta \Big(\frac{\mu^2}{\sigma_{13}                 }\Big)^\epsilon\frac{\Gamma(-\epsilon)^2 \Gamma(-\epsilon-\eta) \Gamma(\epsilon+\eta+1)}{ \Gamma(\eta+1) \Gamma(-2 \epsilon-\eta)}\\
        I_4 &= \frac1{(-s)\sigma_{13}}\Big(\frac{-\nu^2}{\sigma_{13}}\Big)^\eta \Big(\frac{\mu^2}{m^2                         }\Big)^\epsilon\frac{\Gamma(-\eta) \Gamma(1-\epsilon) \Gamma(-2 \epsilon) \Gamma(\epsilon)}{ \Gamma(-2 \epsilon-\eta)}\\
        I_5 &= \frac1{(-s)(-s)       }\Big(\frac{-\nu^2}{-s         }\Big)^\eta \Big(\frac{\mu^2}{m^2                         }\Big)^\epsilon\frac{\Gamma(1-\epsilon) \Gamma(\epsilon) \Gamma(-2 \epsilon-\eta-1)}{\Gamma(-2 \epsilon-\eta)}\\
        I_6 &= \frac1{(-s)\sigma_{13}}\Big(\frac{-\nu^2}{\sigma_{13}}\Big)^\eta \Big(\frac{\mu^2}{m^2}\frac{s^2}{\sigma_{13}^2}\Big)^\epsilon\frac{\Gamma(1-\epsilon) \Gamma(-2 \epsilon) \Gamma(\epsilon) \Gamma(2 \epsilon+\eta+1)}{ \Gamma(\eta+1)}
    \end{align*}
    Clearly, $I_1$, $I_3$, $I_4$, and $I_6$ start at $\lambda^{-1}$ and need to be expanded one order higher.
    This results in
    \begin{align*}
    I&= \frac1\lambda\frac1{-s \sigma_{13}}
        \Bigg(
            \frac1\epsilon\log\frac{m^2}{-s}
            +\log\frac{m^2}{-s}\log\frac{-s\mu^2}{\sigma_{13}^2}-\zeta_2
        \Bigg)
        \\&\quad
        -\frac{\sigma_{13}+\sigma_{23}}{\sigma_{13}}
        \frac1{s^2}
        \Bigg(
            \frac1\epsilon\Big(\log\frac{m^2}{-s}+1\Big)
            +\log\frac{m^2}{-s}\Big(\log\frac{-s\mu^2}{\sigma_{13}^2}-4\Big)
            -\zeta_2
            +2\log\frac{m\mu}{\sigma_{13}}-2
        \Bigg)
    \end{align*}

\end{subtasks}
\end{solution}

\end{document}
