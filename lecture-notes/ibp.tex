%!TEX root=notes

\section{Integration-by-parts reduction}

Real world calculations often involve many hundreds of integrals; computing these one by one is clearly infeasible.
Instead we require a method to reduce the number of integrals to a manageable number of so-called master integrals that we can then calculate.

\subsection{Organising integrals}

\begin{definition}[Integral family]
If we have $\rho$ independent external momenta (after applying momentum conservation), we have
\begin{align}
\mbinom{\rho}{2} = \binom{\rho+1}{2} = \frac{(\rho+1)\rho}2
\end{align}
possible ways to build scalar products (including masses $p_i^2$).
Here, we have defined the multichoose function
\begin{align}
    \mbinom nk = \binom{n+k-1}{k} = \frac{(n+k-1)!}{k!(n-1)!}\,,
\end{align}
that counts the number of ways one can pick $k$ unordered elements from a set of $n$ elements, allowing for repetition.

If we have $\ell$ loop momenta, there are
\begin{align}
    p=\mbinom{\rho+\ell}{2} - \mbinom{\rho}{2} = \ell\frac{1+\ell+2\rho}2
\end{align}
possible scalar products of involving at least one loop momentum.
A set of propagators of this size that allows all scalar products to be written through propagators is called a family.
These propagators rarely all belong to the actual diagram and some might be fictitious, added purely to bring up the numbers.
\end{definition}

\begin{definition}[Reducible scalar integral]
When calculating matrix elements, we often have to find integrals with numerators.
Since the family is complete, we can always write these as propagators and bring the matrix element into the following form
\begin{align}
    \sum_n C_n \times \int \prod_{j=1}^{\ell}[\D k_j] \frac{1}{
        \prop{1,n}^{\alpha_{1,n}}\cdots\prop{p,n}^{\alpha_{p,n}}}\,.
\end{align}
The powers $\alpha_i$ of the propagators $\mathcal{P}_i$ may be negative or zero and the $C_n$ are functions of the external kinematics and the dimension $d$.
These integrals are referred to as reducible scalar integrals.

If we have scalar products with other momenta or even vector integrals we first have to use Passarino-Veltman decomposition as we would at one-loop.
\end{definition}

\begin{definition}[{\tt reduze} organisation~\cite{vonManteuffel:2012np}]
    \label{def:reduze}
In virtually no case are all $\alpha_i>0$.
Hence, for a given scalar integral we define
\begin{itemize}
    \item
    $t$: the number of $\alpha_i>0$

    \item
    $\displaystyle r=\sum_{\alpha_i>0} \alpha_i$
    the sum of denominator powers

    \item
    $\displaystyle s=-\sum_{\alpha_i<0} \alpha_i$
    the sum of numerator powers

    \item
    the sector ID
    \begin{align}
        {\rm ID} = \sum_{k=1}^t 2^{i_k-1} \quad\text{with}\quad \alpha_{i_1},...,\alpha_{i_t} > 0\,.
    \end{align}
\end{itemize}
Obviously $s\ge0$ and $r\ge t$.
This serves to organise integrals because as soon as one integral in a sector can be calculated all integrals of the sector can be calculated, at least in principle.
\end{definition}

\begin{definition}[Corner integral]
For a given sector, we call an integral that has only unit powers the corner integral of that sector.
It obviously has $r=t$.
\end{definition}



\begin{figure}
    \centering
    \renewcommand{\figurename}{Listing}
    \lstinputlisting[language=mathematica,firstline=50,lastline=57]{../code/laporta.m}
    \caption{Mathematica implementation of $t$, $r$, and $s$ given an integral}
\end{figure}

\subsection{Seed identities}
To reduce the number of scalar integrals, we are using integration-by-parts (IBP) identities
\begin{align}
    \int\D x\ u\ v' = u\ v - \int\D x\ u'\ v\,.
\end{align}

\begin{theorem}[IBP for loop integral]
Since the loop integration goes from $-\infty$ to $\infty$, one can show that the surface term $u\ v$ vanishes in dimensional regularisation.
In the language of loop integrals
\begin{align}\label{eq:IBPIdentities}
    \int
        \prod_{j=1}^{\ell}[\D k_j]
        \frac{\partial}{\partial k_i} \cdot\Bigg(
            q \ \frac{1}{
                \prop1^{\alpha_1}\cdots\prop{t}^{\alpha_t}
            } \Bigg)=0\,, \quad i=1,...,\ell\, ,
\end{align}
where $q$ represents either a loop or an external momentum.
Note that $q$ is inside the derivative s.t.
if $q=k_i$ the product rule has to be used on the integrand with
\begin{align}
    \frac{\partial}{\partial k}\cdot k \equiv
    \frac{\partial}{\partial k_\mu}k^\mu = d\,.
\end{align}
IBP relations now allow us to get identities between different integrals.
\end{theorem}

\begin{example}[Heavy quark bubble]
\label{ex:hqseed}
Consider the following example
\begin{align}
    I(a,b) = \int [\D k] \frac{1}{
        \big[k^2\big]^{a}
        \big[(k-p)^2-M^2\big]^{b}
    } \quad\text{with}\quad p^2=M^2\,.
\end{align}
We can relate integrals with different $a$ and $b$ through
\begin{align}\label{eq:seed}\begin{split}
    q=k:\qquad 0 &= (         d  -          2  a - b) I(a,b)-b I(a-1,b+1) \,,\\
    q=p:\qquad 0 &= (\phantom{d} - \phantom{2} a + b) I(a,b)-b I(a-1,b+1)
        + a I(a+1,b-1) + 2bM^2 I(a,b+1)\,.
\end{split}\end{align}
\end{example}

\begin{definition}[Seed identity \& compact notation]
\label{def:seed}
We call identities of the form \eqref{eq:seed} seed identities because we will be using them to create more identities by choosing $a$ and $b$.

We can write seed identities using the short-hand notation of~\cite{Smirnov:2012gma}.
${\bf n}^\pm$ indicates that the power of the $n$-th propagator is raised (lowered) by one.
\begin{align}
    \begin{split}
        0&%
        = d - 2 a - b - b\ {\bf 1}^- {\bf 2}^+\,, \\
        0&%
        =\phantom{d}-\phantom2 a + b - b\ {\bf 1}^- {\bf 2}^+ + a\ {\bf 2}^-\, {\bf 1}^+ + 2 b M^2\ {\bf 2}^+\,.
    \end{split}
\end{align}
\end{definition}

\begin{proofof}{Observation \ref{ex:hqseed}}

Setting $q=k$, we apply~\eqref{eq:IBPIdentities}
\begin{align}\begin{split}
        i(a,b) &%
        = \frac{\partial}{\partial k_\mu}\Bigg(
            k^\mu \ \frac{1}{
                \big[k^2\big]^{a}
                \big[(k-p)^2-M^2\big]^{b}
            } \Bigg)\\%
        &
        = k^\mu\frac{\partial}{\partial k_\mu}\Bigg(
            \frac{1}{
                \big[k^2\big]^{a}
                \big[(k-p)^2-M^2\big]^{b}
            } \Bigg)
        + \Bigg(\frac{\partial}{\partial k_\mu}k^\mu\Bigg)
            \frac{1}{\big[k^2\big]^{a}\big[(k-p)^2-M^2\big]^{b}} \\%
        &
        = -2k^\mu\Bigg(
            \frac{a\, k^\mu }
                {\big[k^2\big]^{1+a}\big[(k-p)^2-M^2\big]^{ b}}
            +\frac{b\,(k^\mu-p^\mu)}
            {\big[k^2\big]^{ a}\big[(k-p)^2-M^2\big]^{1+b}}
        \Bigg)\\%
        &%
        \qquad\qquad + \frac{d}{
            \big[k^2\big]^{a}\big[(k-p)^2-M^2\big]^{b}
        } \\%
        &
        = \frac{(d-2a-2b)k^2 + 2(2a+b-d)k\cdot p}{\big[k^2\big]^{a}\big[(k-p)^2-M^2\big]^{b+1}} \,.
    \end{split}
\end{align}
We now turn this expression back into scalar integrals of the form $I(a',b')$.
After loop integration and setting $\int[\D k]\ i(a,b)=0$ we finally have our first seed identity
\begin{align}
    0 = -b I(a-1,b+1) + (d - 2 a - b) I(a,b)\,.
\end{align}
With $q=p$, we find the other identity.

\end{proofof}

\begin{figure}
    \centering
    \renewcommand{\figurename}{Listing}
    \lstinputlisting[language=mathematica,firstline=34,lastline=38]{../code/laporta.m}
    \caption{Mathematica implementation of the generation of seed identities}
\end{figure}

\subsection{Laporta algorithm}
We now can use identities like the ones above to reduce integrals.
However, doing this by hand is not practical.
Instead, we use a modified version of Gaussian elimination to solve a
very large though sparse system of linear equations by focussing on the difficult integrals first.

\begin{definition}[Lexicographic ordering]
Given two integrals, we need a way to decide which one is more complicated.
The exact specification of this ordering does not matter as long as it is consistent.
We will be using the ordering of Figure~\ref{fig:ordering}:
given $I_1(t_1, r_1, s_1)$ and $I_2(t_2, r_2, s_2)$, it prefers small $t$, $s$, and $r$ in that order.
\end{definition}

\begin{figure}
    \centering
    \scalebox{0.8}{
    \begin{tikzpicture}[node distance=9em]
        \node (inp) [data] {$I_1\prec\succ I_2$?};
        \node (t) [logic, right of=inp] {$t_1 <> t_2$?};
        \node (s) [logic, right of=t  ] {$s_1 <> s_2$?};
        \node (r) [logic, right of=s  ] {$r_1 <> r_2$?};

        \node (0) [data , right of=r  ] {$I_1\equiv I_2$};
        \node (1) [data , above of=0  ] {$I_1 \succ I_2$};
        \node (2) [data , below of=0  ] {$I_1 \prec I_2$};

        \draw[arrow] (inp) edge (t)
                     (t) edge node [near start, above] {$=$} (s)
                     (s) edge node [near start, above] {$=$} (r)
                     (r) edge node [near start, above] {$=$} (0);

        \draw[arrow] (t) |- node [near start,right] {$>$} (1);
        \draw[arrow] (s) |- node [near start,right] {$>$} (1);
        \draw[arrow] (r) |- node [near start,right] {$>$} (1);
        \draw[arrow] (t) |- node [near start,right] {$<$} (2);
        \draw[arrow] (s) |- node [near start,right] {$<$} (2);
        \draw[arrow] (r) |- node [near start,right] {$<$} (2);

    \end{tikzpicture}}

    \label{fig:ordering}
    \caption{A common lexicographical ordering}
\end{figure}

\def\imax{{i_\text{max}}}
\def\jmax{{j_\text{max}}}
\def\rmax{{r_\text{max}}}
\def\smax{{s_\text{max}}}

\begin{theorem}[Laporta's algorithm]
We can now reduce a set of irreducible integral to a (hopefully) small set of master integrals using Laporta's algorithm~\cite{Laporta:2000dsw, Laporta:1996mq} (Figure~\ref{fig:laporta} and Listing~\ref{lst:laporta})
\begin{enumerate}
    \item
    Generate $\imax$ seed identities of Definition \ref{def:seed}

    \item
    List all $\jmax$ reducible integrals up to some cut-off $\rmax$ and $\smax$ and order them using the lexicographical ordering

    \item\label{step:start}
    Apply an identity to an integral and call the resulting relation $\mathcal{R}$

    \item
    Substitute all known relations into $\mathcal{R}$, obtaining a
    relation $\mathcal{R}' = \sum_i c_i I_i = 0$

    \item
    If $\mathcal{R}'$ is trivial, i.e. $c_i=0$ and hence $0=0$, go back to Step~\ref{step:start}.
    Otherwise, solve $\mathcal{R}'$ for the most complicated integral and remember this relation.

    \item
    Go back to Step~\ref{step:start} until all integrals and seed identities have been used.

    \item
    Re-substitute and clean up the relations
\end{enumerate}

This algorithm converges because the cut-off points $\rmax$ and
$\smax$.
The system is naturally over-determined because the number of new
integrals grows slower than the number of equations, resulting in
many trivial relations along the way.

\end{theorem}

\begin{figure}
    \scalebox{0.85}{\begin{tikzpicture}[node distance=9em]
        \def\IBP{{\tt IBP}}

        \node (inp)     [data]
            {$\rmax$, $\smax$};

        \node (genSeed) [block, below of=inp]
            {$\{\IBP\}_i$ for {\small$i=1,\cdots,\imax$}};
        \node (lstInt)  [block, left of=genSeed]
            {$\{I(\vec\alpha)\}_j$ for {\small $j=1,\cdots,\jmax$, $r\le \rmax$, $s\le \smax$}};
        \node (init)    [block, left of=lstInt]
            {$i=j=1$, ${\tt rels} = \{\}$};
        \node (apply)   [block, below of=init]
            {Use $\IBP_i$ on $I(\vec\alpha)_j$ $\to$ $\mathcal{R}$};

        \node (simpl)   [block, right of=apply]
            {$\mathcal{R}/.{\tt rels} = \sum c_k I_k=\mathcal{R}'$};

        \node (testTr)  [logic, right of=simpl]
            {$c_k=0\forall k$?};

        \node (solve)   [block, right of=testTr]
            {solve $\mathcal{R}'$ for most complicated $I_0$};

        \node (add)     [block, right of=solve]
            {add $I_0 = -\sum_1 \frac{b_k}{b_0} I_k$ to {\tt rels}};

        \node (testDone)[logic, below of=add]
            {$j = \jmax$?};

        \node (clean)    [block, right of=testDone]
            {re-sub \& clean};

        \node (done)    [data, below of=clean]
            {\tt rels};

        \node (testI) [logic, below of=testTr]
            {$i<\imax$};

        \node (incI) [block, below of=simpl]
            {$i{\tt ++}$};

        \node (incJ) [block, below of=incI]
            {$i=1$, $j{\tt ++}$};

        \draw [arrow]
            (inp) edge (genSeed)
            (genSeed) edge (lstInt)
            (lstInt) edge (init)
            (init) edge (apply)
            (apply) edge (simpl)
            (simpl) edge (testTr)
            (testTr) edge  node[above] {N} (solve)
            (solve) edge (add)
            (add) edge (testDone)
            (testDone) edge node[above] {Y} (clean)
            (clean) edge (done);

        \draw [arrow]
            (testTr) edge node[right] {Y} (testI)
            (testI) |- node[right] {N} (incJ)
            (incJ) -| (apply);

        \draw [arrow]
            (testDone) edge node[above] {N} (testI)
            (testI) edge node[above] {Y} (incI)
            (incI) -| (apply);
    \end{tikzpicture}}
    \caption{Flowchart for Laporta's algorithm}
    \label{fig:laporta}

\end{figure}



\begin{figure}
    \centering
    \renewcommand{\figurename}{Listing}
    \lstinputlisting[language=mathematica,firstline=83,lastline=125]{../code/laporta.m}
    \caption{Mathematica implementation of Laporta's algorithm}
    \label{lst:laporta}
\end{figure}


\begin{observation}
The implementation provided here is only for educational purposes as it is horribly inefficient.
Instead, one should use one of many public codes such as {\tt AIR}~\cite{Anastasiou:2004vj}, {\tt FIRE}~\cite{Smirnov:2019qkx}, {\tt Kira}~\cite{Maierhofer:2017gsa}, or {\tt reduze}~\cite{ vonManteuffel:2012np}.
These codes essentially all implement Laporta's algorithm albeit much cleverer.
For example, {\tt Kira} uses finite-field sampling to improve the performance, especially for the back-substitution which can quickly become a bottleneck.
Still, the IBP reduction is often a major bottleneck of higher order calculations and can run for months on a large cluster.

\end{observation}
