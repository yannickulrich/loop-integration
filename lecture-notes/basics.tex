%!TEX root=notes

\section{Basics of loop integration}
The calculation of Feynman integrals is a core problem in the calculation of higher order problems.
Over the last decades, many techniques have been developed, both analytic and numeric.
Many resources are available for commonly used techniques such as differential equations or sector decomposition.
In this course, we will cover some more niche techniques that are useful for specialist application.

\begin{definition}[Scalar loop integral]
We will almost exclusively consider integrals of form
\begin{align}
    \int\frac{\D^dk_1   }{(2\pi)^d}
        \cdots
        \frac{\D^dk_\ell}{(2\pi)^d}
        \frac{1}{
            \prop1^{\alpha_1}
            \cdots
            \prop t^{\alpha_t}
        }
    \,,
\end{align}
with $\ell$ loop momenta $k_i$.
The powers $\alpha_i$ of the propagators $\mathcal{P}_i$ may be positive, negative, or zero.
The propagators themselves are either linear or quadratic in the loop momenta.
For example,
\begin{align}
    \prop i =
    \begin{cases}
        (k_i + p_i)^2 - m_i^2 + \io \\
        2 k_i \cdot p_i - m_i^2 + \io
    \end{cases}\,,
\end{align}
where $\io$ denotes the small imaginary part of the Feynman prescription required to ensure causality.
\end{definition}

\begin{definition}[Loop measure]
When solving loop integrals, we often encounter series expansions of the $\Gamma$ function.
This means that our intermediary results for the virtual matrix
elements will contain terms proportional to the Euler constant
$\gamma_E=-\Gamma(1)' = 0.5772...$.
This is completely unphysical and will cancel once the virtual and
real corrections are combined.
It is, however, convenient to already drop these at the integral level.
Hence, we re-define the loop measure as
\begin{align}
    [\D k] = \Gamma(1-\epsilon) \mu^{2\epsilon} \frac{\D^dk}{\I\pi^{d/2}}\,.
\end{align}
In what follows, I will often set $\mu=1$ unless I want to make a
specific point.
Other definitions are used in the literature.
If you are reusing results by other people make sure to check the conventions they have used.
\end{definition}

\begin{theorem}[Tadpole integral]
For $\ell=t=1$ we have the tadpole integral
\begin{align}\label{eq:base}
    \int[\D k] \frac1{\big[k^2-m^2 + \io\big]^n}
        = (-1)^n \big(m^2\big)^{2-n-\epsilon}
            \frac{\Gamma(1-\epsilon)\Gamma(n-2+\epsilon)}{\Gamma(n)}
\end{align}
\begin{proof}
Our integrand has poles at $k_0=\pm\sqrt{\vec k^2+m^2}\mp\io$ as shown in Figure~\ref{fig:wick}.
Since the poles are in the top-left and lower-right quadrant, the integral over the drawn contour vanishes.
Since the integrand falls quickly enough, the integrals over the real and imaginary axis are equal up to a sign
\begin{align}
0 = \oint\D k_0 = \Bigg(\int_{-\infty}^{\infty} + \int_{-\I\infty}^{\I\infty} + \text{arcs}\Bigg)\,.
\label{eq:wick}
\end{align}
Substituting $k_0\to\I k_0$ so that $k^2 = -k_0^2-\vec k^2 = -k_E^2$ where $k_E$ is a Euclidean momentum.
This process in known as a Wick rotation, transforming our expression to
\begin{align}\begin{split}
\int[\D k] \frac1{\big[k^2-m^2 + \io\big]^n}
    &= \frac{\Gamma(1-\epsilon)}{\pi^{d/2}} (-1)^n\int \D^dk_E
            \frac1{\big[k_E^2+m^2 + \io\big]^n}\\
    &= \frac{\Gamma(1-\epsilon)}{\pi^{d/2}} (-1)^n
        \int \D\Omega_d
        \int_0^\infty \D k_E
            \frac{k_E^{d-1}}{\big[k_E^2+m^2\big]^n}\,.
\end{split}\end{align}
At this point we can set $\io=0$ since the denominator is always strictly bigger than zero.
Using that the $d$-dimensional sphere has a volume of
\begin{align}
    \int\D\Omega_d = \frac{2\pi^{d/2}}{\Gamma(\tfrac d2)}
\end{align}
and the basic integral (which is nothing but the definition of the $B$ function)
\begin{align}\label{eq:gamma}
\int_0^\infty \D x\ x^\alpha (a+b x)^\beta
  = \frac{\Gamma(1+\alpha)\Gamma(-1-\alpha-\beta)}{\Gamma(-\beta)}
        \frac{a^{1+\alpha+\beta}}{b^{1+\alpha}}
    \,,
\end{align}
we find
\begin{align}\begin{split}
\int[\D k] \frac1{\big[k^2-m^2 + \io\big]^n}
    &= \frac{\Gamma(1-\epsilon)}{\pi^{d/2}} (-1)^n
        \frac{2\pi^{d/2}}{\Gamma(\tfrac d2)}
        \big(m^2\big)^{d/2-n}
        \frac{\Gamma(\tfrac d2)\Gamma(n-\tfrac d2)}
             {2\Gamma(n)} \\
    &= (-1)^n
        \big(m^2\big)^{2-n-\epsilon}
        \frac{\Gamma(1-\epsilon) \Gamma(n-2+\epsilon)}
             {\Gamma(n)} \,.
\end{split}\end{align}

\begin{figure}
    \centering
    \begin{tikzpicture}
        %Coordinate system
        \draw[-latex] (-4.5,0)--(4.5,0) node [below right] {$\mathrm{Re}(k_0)$};
        \draw[-latex] (0,-4.5)--(0,4.5) node [below right] {$\mathrm{Im}(k_0)$};

        \draw[fill=black,draw=none] (3,-1.5) circle (2.25pt) node[below,yshift=-5] {$+\sqrt{\vec k^2-m^2}-\io$};
        \draw[fill=black,draw=none] (-3,1.5) circle (2.25pt) node[below,yshift=-5] {$-\sqrt{\vec k^2-m^2}+\io$};

        \def\dt{2}
        \def\r{4.}
        \draw (180-\dt:\r) -- (\dt:\r)
              --++ (0,0) arc (\dt:90-\dt:\r)
              -- (270+\dt:\r)
              --++ (0,0) arc (270+\dt:180-\dt:\r);
        % \draw () -- (\r,\de) --++ (0,0) arc(0:80:\r) -- (\de,-\r);

    \end{tikzpicture}
    \caption{The poles of the $k_0$ integration and the contour of~\eqref{eq:wick}}
    \label{fig:wick}
\end{figure}

\end{proof}
\end{theorem}


To solve any loop integral with more propagators than loops, we could either solve the loops one by one or all in one go.
Both methods are equivalent though difficult to relate in practical examples.
We will be focusing on the latter case as the former can be viewed as a sub-class.
\begin{lemma}[Feynman parametrisation \& Cheng-Wu theorem~\cite{Cheng:1987ga}]
We write
\begin{align}\label{eq:feynman}
    \frac{1}{\prop1^{\alpha_1}\cdots\prop t^{\alpha_t}} = \frac{\Gamma(r)}{\prod_j\Gamma(\alpha_j)} \int_0^\infty \prod_{j=1}^t \D x_j\ x_j^{\alpha_j-1} \delta\Bigg(\sum_{i\in\nu}x_i-1\Bigg) \frac1{\Big(\mathcal{P}_1 x_1+...+\mathcal{P}_tx_t\Big)^r}\,.
\end{align}
where $r=\sum_j\alpha_j$ and $\nu$ a non-empty subset of $\{1,...,t\}$ (Cheng-Wu theorem).
\end{lemma}
Note that most books on QFT will assume $\nu=\{1,...,t\}$, reducing the integration region to $[0,1]\times[1,1-x_1]\times...$.
However, for analytic calculations we have found that having just one element, say $i=1$, in $\nu=\{1\}$ is a better choice, setting one $x_1=1$ and keeping the integration bounds at $[0,\infty]^{t-1}$.


\def\U{\mathcal{U}}
\def\F{\mathcal{F}}
\begin{theorem}
We can calculate a general $\ell$-loop integral in terms of graph polynomials or Symanzik polynomials
\begin{align}
        I = (-1)^r \Gamma(1-\epsilon)^\ell &%
        \frac{\Gamma(r-\ell d/2)}{\prod_j \Gamma(\alpha_j)}
            \int_0^\infty \prod_{j=1}^t \D x_j\ x_j^{\alpha_j-1}
            \delta\Big(x_i-1\Big)
            \underbrace{
                \frac{\U^{r-(\ell+1)d/2}}{(\F-\io)^{r-\ell d/2}}
            }_{\mathcal{G}}
            \,.
        \label{eq:feynman:int}
\end{align}
$\U$ and $\F$ are the graph polynomials or Symanzik polynomials that can be computed analytically (see~\eqref{eq:UFdet}), algorithmically (see Observation~\ref{obs:UFalg}), or graph-theoretically (see Observation~\ref{obs:UFgraph}).

Further, $\U>0$ so its prescription does not matter.
$\F$, however, can be both positive and negative.
It is hence important to properly include its prescription as $\F\to\F-\io$.
\begin{proof}
We use \eqref{eq:feynman} to write the dominator as
\begin{align}\label{eq:denpara}
    D=\mathcal{P}_1 x_1+...+\mathcal{P}_tx_t = k^T\cdot M(x_i)\cdot k - 2Q(x_i,q_j)^Tk+J(x_i,s_{jk}) + \io\,,
\end{align}
with a $\ell\times\ell$ matrix $M$, $\ell$-vectors $k=(k_1,...,k_\ell)$ and $Q$, depending on the Feynman parameters $x_i$, external momenta $q_j$ and invariants $s_{jk}=2q_j\cdot q_k$.
By shifting $k\to k+M^{-1}Q$ we cancel the linear term so that after diagonalising $M$ (with eigenvalues $\lambda_i$) we have
\begin{align}
    D = k^T\cdot \mathrm{diag}(\lambda_i)\cdot k - \Delta \quad\text{with}\quad \Delta = Q^TM^{-1}Q-J-\io\,.
\end{align}
$\Delta$ has a $-\io$ prescription since we started with $J+\io$ in \eqref{eq:denpara}.
Next, we rescale $k_i\to\lambda_i^{-1/2}k_i$ to factorise the loop integrations
\begin{align}
    I &=
    \frac{\Gamma(r)}{\prod_j\Gamma(\alpha_j)}
        \int_0^\infty \prod_{j=1}^t \D x_j\ x_j^{\alpha_j-1}
        \delta(\cdots)
        \underbrace{
            \int \lambda_1^{-d/2}[\D k_1]\cdots\lambda_\ell^{-d/2}[\D k_\ell]
            \frac1{
                \big[k^T\cdot k - \Delta\big]^r
            }
        }_{I'}
\end{align}
We can extend~\eqref{eq:base} to cover this case
\begin{align}
\begin{split}
    I' &= \Bigg(\frac{\Gamma(1-\epsilon)}{\pi^{d/2}}\Bigg)^\ell (-1)^r
            \int\D^dk_{1,E}\cdots\D^dk_{\ell,E}
                \frac1{\big[k_{1,E}^2+\cdots+k_{\ell,E}^2+\Delta\big]^r}
\\&
       = (-1)^r\Delta^{\ell d/2-r}
            \frac{\Gamma(1-\epsilon)^\ell\Gamma(r-\ell d/2)}{\Gamma(r)}
\,.
\end{split}
\end{align}
With this, we can write
\begin{align}
    I &=
    \frac{\Gamma(r)}{\prod_j\Gamma(\alpha_j)}
        \int_0^\infty \prod_{j=1}^t \D x_j\ x_j^{\alpha_j-1}\lambda_i^{-d/2}
        \delta(\cdots)
        (-1)^r \Delta^{-r+\ell d/2}
            \frac{\Gamma(1-\epsilon)^\ell\Gamma(r-\ell d/2)}{\Gamma(r)}
\,.
\end{align}
Here we have used~\eqref{eq:base} to find the general Feynman-parametrised form of the $\ell$-loop integral.
We now identify
\begin{align}\label{eq:UFdet}
    \mathcal{U}=\det M = \prod_j\lambda_j\,,\ \qquad \mathcal{F}=\det M\times\Delta\,,
\end{align}
to arrive at \eqref{eq:feynman:int}.
\end{proof}
\end{theorem}


\begin{observation}[Algorithmic calculation of $\U$ and $\F$]
\label{obs:UFalg}
We can calculate $\U$ and $\F$ directly without having to manually find the eigenvalues $\lambda_i$.
Instead, we calculate $\Delta$ and $\det M$ directly from \eqref{eq:denpara}.
Collecting terms of $k_1$
\begin{align}
\eqref{eq:denpara} \equiv D_1 = k_1^2\ t_1^{(2)} + k_1\cdot t_1^{(1)} + t_1^{(0)}
\,,
\end{align}
we identify $t_1^{(2)}\equiv \lambda_1$ and set
\begin{align}
D_2 = t_1^{(0)} - \frac{t_1^{(1)}\cdot t_1^{(1)}}{4t_1^{(2)}}\,.
\end{align}
We now repeat this and identify $t_2^{(i)}$
\begin{align}
D_2 = k_2^2\ t_2^{(2)} + k_2\cdot t_2^{(1)} + t_2^{(0)}
\end{align}
in order to construct $D_3$.
Eventually, we will reach $D_{\ell+1} = -\Delta$.
This can be efficiently implemented in Mathematica as shown in Listing~\ref{lst:UF}.
\end{observation}

\begin{figure}
    \centering
    \renewcommand{\figurename}{Listing}
    \lstinputlisting[language=mathematica,firstline=1,lastline=17]{../code/UF.m}
    \caption{Mathematica implementation of $\U$ and $\F$}
    \label{lst:UF}
\end{figure}


\begin{observation}[Graph-theoretical determination of $\U$ and $\F$]
\label{obs:UFgraph}
\def\base{
    \coordinate (o0) at (0,0);
    \coordinate (a1) at (+45:1);
    \coordinate (b1) at (-45:1);
    \coordinate (a2) at ($(a1) + (0.5,0)$);
    \coordinate (b2) at ($(b1) + (0.5,0)$);
    \coordinate (AB) at ($(a1)!0.5!(b1)$);
    \coordinate (AO) at ($(a1)!0.5!(o0)$);
    \coordinate (BO) at ($(b1)!0.5!(o0)$);
    \draw (180:0.5) -- (o0);
    \draw[thick] (o0) -- (a1) -- (a2);
    \draw[thick] (o0) -- (b1) -- (b2);
    \draw[thin] (a1) -- (b1);
}
\newcommand\drawcut[3]{
    \def\th{#2}
    \def\r{#3}
    \def\w{0.1}
    \fill[white]  (#1) ++ (\th:\r)
                     --++ (\th+90:\w)
                     --++ (\th:-2*\r)
                     --++ (\th+90:-2*\w)
                     --++ (\th:2*\r) -- cycle;
    \draw[dashed] (#1) ++ (\th:\r) --+ (\th:-2*\r);
}
\newcommand\cutO[2]{
    \begin{gathered}
    \begin{tikzpicture}
    \base
    \drawcut{#1}{#2}{0.3}
    \end{tikzpicture}
    \end{gathered}
}
\newcommand\cutT[3]{
    \begin{gathered}
    \begin{tikzpicture}
    \base
    \drawcut{$(#1)!0.5!(#2)$}{#3}{0.6}
    \end{tikzpicture}
    \end{gathered}
}

$\U$ and $\F$ can also be determined just by considering the graph of the loop integral in question.
\eqref{eq:denpara} assigns a Feynman parameter $x_i$ to each edge of the graph.
For example, consider the following one-loop integral
\begin{align}
\begin{gathered}
\begin{tikzpicture}
\base
\node at (AB) [xshift= 10,yshift=  0]{$x_1$};
\node at (AO) [xshift=- 5,yshift=  5]{$x_2$};
\node at (BO) [xshift=- 5,yshift=- 5]{$x_3$};
\node at (a2) [xshift=15] {$\to p$};
\node at (b2) [xshift=15] {$\to q$};
\end{tikzpicture}
\end{gathered}
= \int[\D k_1]
    \frac1{k_1^2}             % x_1
    \frac1{(k_1+p)^2-m^2}     % x_2
    \frac1{(k_1-q)^2-m^2}     % x_3
\end{align}
$\U$ is then determined by adding the products $x_i$ for each $\ell$ line cuts of propagators that results in a tree-level
\begin{align}\begin{split}
\U &= \cutO{AB}{0} + \cutO{AO}{-45} + \cutO{BO}{45}\\
   &=  x_1         + x_2            + x_3\,.
\end{split}\end{align}
Similarly, for massless propagators, $\F$ is found by adding all $(\ell+1)$ line cuts with the negative square of the momentum flowing through the resulting tree
\begin{align}\begin{split}
\F_0 &= \cutT{AB}{AO}{-30} + \cutT{AB}{BO}{+30} + \cutT{AO}{BO}{90}
 \\&= -p^2 x_1x_2         - q^2 x_1x_3         - (p+q)^2 x_2 x_3\,.
\end{split}\end{align}
To account for massive propagators we write
\begin{align}
\F = \F_0 + \sum x_i m_i^2 \U\,,
\end{align}
where $m_i$ is the mass of the propagator associated to $x_i$.
\end{observation}

\begin{theorem}[Master formula for loop integrals]
An arbitrary integral, including numerators, can be calculated as
\begin{align}
    \begin{split}
        I = (-1)^r \Gamma(1-\epsilon)^\ell &%
        \Gamma(r-s-\ell d/2) \int_0^\infty \delta\Big(x_i-1\Big) \Bigg( \prod_{j=1}^t \D x_j\ \frac{x_j^{\alpha_j-1}}{\Gamma(\alpha_j)} \Bigg)\\%
        &
        \Bigg( \prod_{j=t+1}^{p} \frac{\partial^{-\alpha_j}}{\partial
x_j^{-\alpha_j}} \Bigg) \frac{\U^{r-s-(\ell+1)d/2}}{(\F-\io)^{r-s-\ell d/2}} \Bigg|_{x_{t+1}=...=x_p=0}\,,
        \label{eq:masterform}
    \end{split}
\end{align}
with $r$ ($s$) the sum of positive (negative) indices, $t$ the number of positive indices and $p$ the length of the family as defined above.
This implies that
\begin{align}
    \alpha_1,...,\alpha_t > 0 \quad\text{and}\quad \alpha_{t+1},...,\alpha_p \le 0\,.
\end{align}

\begin{proof}
As discussed above, we implement numerators in integrals by setting some $\alpha_i<0$.
However, that would make the Feynman parametrisation ill-defined because the $\Gamma$ function diverges for negative integers.
To solve this problem, we note an identity for Mellin transforms called Ramanujan's master theorem.
In our language it states that the Mellin transform of a function $f(x)$ evaluated at negative integers $-n$ can be written as the $n$-th derivative of $f$
\begin{align}
    \{\mathcal{M}f\}(-n)=\int\D x\,x^{-n-1}\,\,f(x) =\Gamma(-n)f^{(n)}(0) \,.
\end{align}
Now the $\Gamma(-n)$ cancels, finally leading to our master formula.
\end{proof}
\end{theorem}

\begin{lemma}[Lee-Pomeransky representation]
    An alternative form of \eqref{eq:feynman:int} is the Lee-Pomeransky representation
    \begin{align}
        I = \frac{\Gamma(d/2)}{\Gamma(\tfrac{l+1}2d-r)\prod_j\Gamma(\alpha_j)}
        \int
        \bigg(\prod_j\frac{\D x_j}{x_j} x_j^{\alpha_j}\bigg)
        \Big(\U+\F\Big)^{-d/2}\,.
        \label{eq:leepomeransky}
    \end{align}
\end{lemma}
