%!TEX root=notes
%PDFLATEX

\subsection{Auxiliary mass flow}

As we have seen it is possible to solve arbitrarily complicated Feynman integrals using series expansions once we have the differential equation matrix and the boundary conditions.
Laporta's algorithm allows us to, at least in principle, find the differential equation matrix for any set of integrals.
However, the boundary condition still need to be computed manually.
While we could use a tool like {\tt pySecDec}~\cite{Borowka:2017idc,Borowka:2018goh,Heinrich:2021dbf,Heinrich:2023til} to evaluate these numerical using sector decomposition, this can be numerically challenging.
Instead, we can use the method of region in a particularly clever way~\cite{Liu:2017jxz,Liu:2021wks}.

\begin{observation}[Expansion in large masses]
    We saw in Example~\ref{ex:morheavy} that expanding in a heavy mass results in two regions.
    In the hard region $h$ all other masses and kinematic invariants are set to zero.
    In the soft region $s$ the massive propagator is replace with $1/M^2$.
    Either way, the resulting integral becomes a lot simpler.
\end{observation}

\begin{theorem}[Auxilary mass]
    Consider an integral where a large \emph{auxiliary mass} $\eta$ is added to each propagator, i.e.
    \begin{align}
        I = \int\frac{[\D k_1]\cdots [\D k_\ell]}{
            \mathcal{D}_1
            \mathcal{D}_2
            \cdots
            \mathcal{D}_p
        }
        \to
        I_\eta = \int\frac{[\D k_1]\cdots [\D k_\ell]}{
            (\mathcal{D}_1-\eta)
            (\mathcal{D}_2-\eta)
            \cdots
            (\mathcal{D}_p-\eta)
        }\,,
    \end{align}
    with $\mathcal{D}_i = (k_i+p_i)^2-m_i^2$.
    For $\eta\gg m_i^2,s_{ij}$ this integral can be considered known and used as a boundary condition.
\end{theorem}
\begin{proof}
    Applying the method of regions means that we either have all loop momenta hard, i.e. $h^\ell$, or at least one loop momentum soft, i.e. $sX^{\ell-1}$.
    \begin{itemize}
        \item
            For $h^\ell$ the propagators become $\mathcal{D}_i = k_i^2-\eta$, i.e. single-scale vacuum integrals.
            These are known up to $\ell=5$ and it is in fact possible to derive them iteratively~\cite{Liu:2022mfb}.

        \item
            For $sX^{\ell-1}$ we assume w.l.o.g. that $k_1$ is the soft momentum.
            This means all propagators of the form $\mathcal{D}_i = (k_1+p_i)^2-m_i^2-\eta$ just become $\mathcal{D}_i\to\eta$.
            In propagators that have other loop momenta, we can safely neglect $k_1$.
            This means our $k_1$ integration is trivial and we are left with an $(\ell-1)$-loop integral.
    \end{itemize}
    By recursively applying this algorithm we can solve any $I_\eta$.
\end{proof}

\begin{theorem}[Auxilary mass flow]
    We can use IBP reduction to derive a differential equation for the system of $\vec I_\eta$
    \begin{align}
        \frac{\partial}{\partial\eta}\vec I_\eta = M_\eta \vec I_\eta\,,
    \end{align}
    and use the boundary condition $\eta\to-\I\infty$ to obtain our integrals at $\eta = \im$ through multiple series expansions at
    \begin{itemize}
        \item $\eta\to-\I\infty$:
            We fix our boundary conditions at $\eta\to\I\infty$ and perform a power-log expansion
            \begin{align}
                \vec I_\eta^{\ (\infty)} = \eta^{\vec a+b\epsilon} \sum_{i=0}^\infty \vec c_i(\epsilon) \eta^{-i}\,.
            \end{align}
            The start of the expansion $\vec a$, order of the logarithms $b$, and the first term $\vec c_0$ are wholly determined by the boundary condition while the remaining $\vec c_i(\epsilon)$ are fixed by the differential equation.

        \item $\eta = \eta_0$:
            We match and expand a Taylor series around $\eta_0$ which is chosen such that $|\eta_0|>|\eta_s|$ for all singularities of the differential equation matrix $\eta_s\neq0$.

        \item $\eta = \eta_i$:
            We match and expand Taylor series with overlapping radii of convergence.

        \item $\eta = \eta_N$:
            The last value we expand around is chosen such that $|\eta_N| < |\eta_s|$.
            Here we require a log-power expansion for $\vec I_\eta$ (cf. Lemma~\ref{lem:eta0})
            \begin{align}
                \vec I_\eta^{\ (N)} \sim \sum_{i,j=0}^\infty \vec c_{i,j}(\epsilon)\  \eta^i\log^j\eta\,.
            \end{align}

        \item $\eta = \im$:
            The final integral is obtained by dropping all non-analytic terms in the log-power expansion
            \begin{align}
                \vec I = \vec c_{0,0}(\epsilon)\,,
            \end{align}
            since in dimensional regularisation
            \begin{align}
                \lim_{\eta\to\im} \eta^{a+b\epsilon}=0
                \quad\text{for}\quad
                b\neq 0\,.
            \end{align}
    \end{itemize}
\end{theorem}

\begin{observation}
    Note that we are expanding around $\eta = -\I\infty$ rather than just $\eta=\infty$.
    Singularities of the original integral lie on the real axis in $\eta$ and therefore $\vec I_\eta$ is finite as long as $\Im\eta\neq0$.
    Hence, choosing $\eta=-\I\infty$ may complicate matters as we have to work with complex masses, it greatly simplifies the running $\eta\to\im$.
\end{observation}

\def\mz{{\tilde M_0}}
\begin{lemma}[Vanishing auxiliary mass]
    \label{lem:eta0}
    To formalise the last matching step, we define
    \begin{align}
        \tilde M = \eta M_\eta = \sum_{i=0}^\infty \tilde M_i \eta^i
    \end{align}
    where we assume that $\mz$ is finite.
    Then
    \begin{align}
        \vec I = \Big(\lim_{\eta\to\im} \exp(\log\eta\ \mz)\Big)
            \cdot\Bigg(\sum_{i=0}^\infty P_i \eta_N^i \cdot \exp(\log\eta_N\ \mz)\Bigg)^{-1}
            \cdot \vec I_\eta^{\ (N)}\,.
    \end{align}
    The $P_i$ are matrices that fulfil
    \begin{align}
        i\,P_i + P_i\cdot \mz - \mz\cdot P_i = \sum_{j=0}^{i-1}\tilde M_{i-j}\cdot P_j
        \qquad\text{and}\qquad
        P_0=1\,.
    \end{align}
\end{lemma}
\begin{proof}
    We can write the differential equation using $\tilde M=\eta M_\eta$ as
    \begin{align}
        \eta\frac{\D\vec I_\eta}{\D\eta} = \frac{\D\vec I_\eta}{\D\log\eta} = \tilde M\cdot\vec I_\eta\,.
    \end{align}
    This can be solved by
    \begin{align}
        \vec I_\eta(\eta) = P(\eta)\cdot \exp\Big(\mz\log\eta\Big) \cdot\vec v
        \equiv P(\eta)\cdot\eta^\mz\cdot \vec v
        \label{eq:ansatzP}
    \end{align}
    with some $\vec v$.
    In this expression, the logarithmic structure is completely encoded in the matrix exponential so that $P$ is just a power series.
    We now have
    \begin{align}
        \frac{\D\vec I_\eta}{\D\log\eta}
            = \bigg(
                \frac{\D P}{\D\log\eta} + P\cdot\mz
            \bigg)\cdot\eta^\mz\cdot\vec v
            = \tilde M\cdot P\cdot \eta^\mz \cdot \vec v\,.
    \end{align}
    By power-expanding $\tilde M$ and $P$, we find
    \begin{align}
        \sum_{i=0}^\infty\eta^i
            \Big(i\,P_i + P_i\cdot \mz\Big)
        = \sum_{i=0}^\infty\eta^i
            \sum_{j=0}^i \tilde M_{i-j}\cdot P_j\,.
    \end{align}
    We can now split of the $j=i$ case and compare coefficient-by-coefficient in $\eta^i$
    \begin{align}
        i\,P_i + P_i\cdot \mz - \mz\cdot P_i = \sum_{j=0}^{i-1}\tilde M_{i-j}\cdot P_j\,.
    \end{align}
    For $i=0$, $P_0=1$ is a valid solution for
    \begin{align}
        P_0\cdot \mz - \mz\cdot P_0 = 0\,.
    \end{align}
    By evaluating~\eqref{eq:ansatzP} for $\eta=\eta_N$, we can obtain $\vec v$ since we know $\vec I_\eta(\eta_N)$
    \begin{align}
        \vec v = \Big[P(\eta_N)\cdot\eta_N^\mz\Big]^{-1}\cdot \vec I_\eta(\eta_N)\,,
    \end{align}
    and hence
    \begin{align}
        \vec I_\eta(\eta) =
        P(\eta)\cdot\eta^\mz\cdot\Big[P(\eta_N)\cdot\eta_N^\mz\Big]^{-1}\cdot \vec I_\eta(\eta_N)\,.
    \end{align}


\end{proof}

\begin{lemma}[Step size]
    We already know that the first matching point, $\eta_0$, needs to be bigger than the largest singularity while the last, $\eta_N$ needs to be smaller than the smallest non-zero singularity.
    In practice, we choose a factor $R$ and then define
    \begin{subequations}
        \label{eq:stepssize}
        \begin{align}
            \eta_0 &= -\I R\max\{|\eta_s|\ :\ \text{$\eta_s\neq\infty$ singularities of $M_\eta$}\}\,,\\
            \eta_N &= -\frac\I{R}\min\{|\eta_s|\ :\ \text{$\eta_s\neq0$ singularities of $M_\eta$}\}\,.
        \end{align}
        Next, we choose the intermediary values $\eta_i$ such that
        \begin{align}
            \frac{\eta_{i+1}}{\eta_i} = \frac{R-1}{R}
            \quad\text{for}\quad
            0\le i < N\,.
        \end{align}
    \end{subequations}
    These equations also define the number $N$ of steps we are taking.
\end{lemma}

\begin{example}[Massless bubble]
    Consider the integral family
    \begin{align}
        I_{\alpha\beta}=\int\frac{[\D k]}{
            \big[k^2\big]^\alpha
            \big[(k-p)^2\big]^\beta
        }\,,
    \end{align}
    with $p^2=s\equiv 1$.
    This family has a single master integral $I_{11}$.
    Adding the auxiliary mass modifies this to
    \begin{align}
        I_{\eta,\alpha\beta}=\int\frac{[\D k]}{
            \big[k^2-\eta\big]^\alpha
            \big[(k-p)^2-\eta\big]^\beta
        }\,,
    \end{align}
    which now has two master integrals $I_{\eta,10}$ and $I_{\eta,11}$.
    The differential equation matrix for $\vec I_\eta$ is, as in~\eqref{eq:dmat1}
    \begin{align}
        M_\eta = \begin{pmatrix}
              \frac{1- \epsilon}{\eta         } & 0 \\
             -\frac{2-2\epsilon}{\eta(4\eta-1)} & \frac{2(1-2\epsilon)}{4\eta-1}
        \end{pmatrix}\,.
    \end{align}
    We can hence expect singularities at $\eta=0$, $\eta=1/4$, $\eta=\infty$

    The first boundary condition is just a tadpole
    \begin{align}
        I_{\eta,10} = \int\frac{[\D k]}{k^2-\eta}
            = -\eta^{1-\epsilon}\Gamma(1-\epsilon)\Gamma(-1+\epsilon)\,.
    \end{align}
    For the second, we expand in $\eta\sim\infty$.
    The soft region $s$ vanishes as both propagators just become $1/\eta$ while the hard region $h$ is
    \begin{align}
        I_{\eta,11} \sim \int\frac{[\D k]}{
            \big[k^2-\eta\big]^2
        } = \eta^{-\epsilon}\Gamma(1-\epsilon)\Gamma(\epsilon)
         + \mathcal{O}\Big(\frac1{\eta}\Big)
         \,.
    \end{align}
    Next, we need to choose our expansion points.
    The poles of $M_\eta$ are at $\eta=0$ and $\eta=1/4$.
    Using $R=2$ in~\eqref{eq:stepssize} we have $\eta_0=-\I/2$, $\eta_1=-\I/4$, and $\eta_2=\eta_N=-\I/8$.

    \begin{itemize}
        \item $\eta\to-\I\infty$:
            We have $\vec a=(1,0)$ and $b=-1$.
            We can now solve for $\vec c$ and find
            \begin{subequations}
                \begin{align}
                    \vec c_0 &= \begin{pmatrix}
                        -\Gamma(1-\epsilon)\Gamma(-1+\epsilon)\\
                        \Gamma(1-\epsilon)\Gamma(\epsilon)
                    \end{pmatrix}\,,\\
                    \vec c_1 &=
                        \Big(0,
                            \frac16
                            +\mathcal{O}(\epsilon^2)
                        \Big)^T\,,\\
                    \vec c_2 &=
                        \Big(0,
                            \frac1{60}
                            +\frac1{60}\epsilon
                            +\mathcal{O}(\epsilon^2)
                        \Big)^T\,,\\
                    \vec c_3 &=
                        \Big(0,
                            \frac1{420}
                            +\frac1{280}\epsilon
                            +\mathcal{O}(\epsilon^2)
                        \Big)^T\,.
                \end{align}
            \end{subequations}
            Evaluating this for the next point $\eta=-\I/2$ gives for the second integral
            \begin{align}
                \label{eq:amex:inf}
                I_{\eta,11}^{(\infty)}(\eta=-\I/2) =
                    \frac{1}{\epsilon}
                    +(0.632066\, +1.88709 \I)
                    +(0.0555556\, +1.18759 \I) \epsilon
                    +\mathcal{O}(\epsilon^2)\,.
            \end{align}

        \item $\eta=-\I/2$:
            We perform a normal Taylor expansion with the $(\eta+\I/2)^0$ term given by~\eqref{eq:amex:inf}
            \begin{align}\begin{split}
                I_{\eta,11}^{(0)} &=
                                \Big(\frac{1}{\epsilon }+(0.632066\, +1.88709 \I)+(0.0555556\, +1.18759  \I) \epsilon\Big)\\&\quad
                +(\eta +\I/2)   \Big(                    (0.5714  \, -1.77538 \I)+(3.68432  \, -0.0178779\I) \epsilon\Big)\\&\quad
                +(\eta +\I/2)^2 \Big(                   -(1.39587 \, +0.983636\I)-(0.388233 \, +4.17847  \I) \epsilon\Big)\\&\quad
                +\mathcal{O}\Big((\eta+\I/2)^3, \epsilon^2\Big)
            \end{split}\end{align}
            Evaluating this at $\eta=-\I/4$ we have
            \begin{align}
                \label{eq:amex:0}
                I_{\eta,11}^{(0)}(-\I/4) =
                    \frac1\epsilon
                    + (1.18638  \, + 2.13174 \I) 
                    + (0.0584251\, + 2.50302 \I) \epsilon
                    +\mathcal{O}(\epsilon^2)\,.
            \end{align}

        \item $\eta=-\I/4$:
            We perform a normal Taylor expansion with the $(\eta+\I/4)^0$ term given by~\eqref{eq:amex:0}
            \begin{align}\begin{split}
                I_{\eta,11}^{(1)} &=
                                \Big(\frac{1}{\epsilon }+(1.18638\,+2.13174 \I)+(0.0584251\,+2.50302 \I) \epsilon\Big)\\&\quad
                +(\eta +\I/4)   \Big(                    (1.63896\,-2.76086 \I)+(7.62454  \,+0.251576\I) \epsilon\Big)\\&\quad
                +(\eta +\I/4)^2 \Big(                   -(2.56095\,+4.19991 \I)+(3.18517  \,-14.0005 \I) \epsilon\Big)\\&\quad
                +\mathcal{O}\Big((\eta+\I/4)^3, \epsilon^2\Big)
            \end{split}\end{align}
            Evaluating this at $\eta=-\I/8$ we have
            \begin{align}
                \label{eq:amex:1}
                I_{\eta,11}^{(1)}(-\I/8) =
                    \frac1\epsilon
                    + (1.57145\,   + 2.42639 \I)
                    - (0.0708639\, - 3.73778 \I) \epsilon
                    +\mathcal{O}(\epsilon^2)\,.
            \end{align}

        \item $\eta=\im$:
            We begin by finding $P$.
            Per definition, $P_0=1$.
            The defining equation for $P_1$
            \begin{subequations}
                \begin{align}
                    P_1 +
                    P_1\begin{pmatrix}1-\epsilon&0\\2-2\epsilon&0\end{pmatrix}-
                    \begin{pmatrix}1-\epsilon&0\\2-2\epsilon&0\end{pmatrix}P_1
                    = \begin{pmatrix}0&0\\8 (1 - \epsilon) & -2 (1 - 2 \epsilon)\end{pmatrix}
                \end{align}
                is solved by
                \begin{align}
                    P_1 = \begin{pmatrix}0 & 0\\ 6 - 7\epsilon&-2 + 4\epsilon\end{pmatrix}
                        +\mathcal{O}(\epsilon^2)\,.
                \end{align}
            \end{subequations}
            Next, for $P_2$
            \begin{subequations}
                \begin{align}
                    2P_2 +
                    P_2\begin{pmatrix}1-\epsilon&0\\2-2\epsilon&0\end{pmatrix}-
                    \begin{pmatrix}1-\epsilon&0\\2-2\epsilon&0\end{pmatrix}P_2
                    = \begin{pmatrix}0&0\\20+6\epsilon & -4 \end{pmatrix}
                        +\mathcal{O}(\epsilon^2)\,,\\
                    P_2 = \begin{pmatrix}0&0\\8+\tfrac{10}3\epsilon & -2\end{pmatrix}
                        +\mathcal{O}(\epsilon^2)\,.
                \end{align}
            \end{subequations}
            Summing the first fifty terms
            \begin{align}
                \begin{split}
                    P &=
                    \begin{pmatrix}
                        1 & 0\\
                        -0.114698 -0.718552 i & 1.02909 +0.242934 i
                    \end{pmatrix} \\&\qquad-
                    \begin{pmatrix}
                        0 & 0\\
                         0.0402931 -0.901504 i  & 0.00218107 +0.504238 i
                    \end{pmatrix} \epsilon
                    +\mathcal{O}(\epsilon^2)\,.
                \end{split}
            \end{align}
            We also need the matrix exponential
            \begin{align}
                \lim_{\eta\to\im} \exp(\log\eta\ \mz)
                 = \lim_{\eta\to\im}
                    \begin{pmatrix}\eta^{1-\epsilon}&0\\-2 + 2 \eta^{1 - \epsilon} & 1\end{pmatrix}
                = \begin{pmatrix} 0 & 0 \\ -2 & 1\end{pmatrix}
            \end{align}
            With this we can calculate without too much difficulty
            \begin{align}
                I_{11} = \frac{1}{\epsilon}+(2\,+3.14159\I)-(0.934802\,-6.28319\I)\epsilon + \mathcal{O}(\epsilon^2)\,.
            \end{align}
    \end{itemize}
\end{example}

\begin{observation}
    Note how we started with one master integral $I_{11}$ and had to actually calculate two, $I_{\eta,10}$ and $I_{\eta,11}$.
    In this example, the integrals were simple enough so that this was not a problem but in a real-life example this might not be the case.
    Consider for example the diagram in Figure~\ref{fig:pentabox}~\cite{Liu:2021wks}.
    The original topology contained 108 master integrals.
    Adding an $\eta$ to every propagator increases this to 476 integrals.
    Instead we could add $\eta$ only to some propagator(s).
    If we chose the highlighted line, we only have 176 masters which greatly speeds up the computation.
    Of course, in cases where we have an internal massive line such as an electoweak boson or top quark, we should use its mass as the auxiliary mass.
\end{observation}
\begin{figure}
    \centering
    \begin{tikzpicture}[x=7mm,y=7mm]
        \draw(0,0) -- (1,0);
        \draw(0,-1) -- (0,+1);
        \draw(+2,+1) -- (0,+1) -- (-2,+1) -- (-2,-1) -- (0,-1) -- (+2,-1);
        \draw[red](+2,+1) -- (+2,-1);
        \draw(-2,-1) --+ (225:1);
        \draw(-2,+1) --+ (135:1);
        \draw(+2,+1) --+ (45:1);
        \draw(+2,-1) --+ (315:1);
    \end{tikzpicture}
    \caption{
        The two-loop pentabox of~\cite{Liu:2021wks} with all internal and external masses vanishing.
        The red line is a good candidate for the auxiliary mass.
    }
    \label{fig:pentabox}
\end{figure}
