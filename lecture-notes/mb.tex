%!TEX root=notes

\section{Mellin Barnes integration}
\label{sec:mb}

When calculating loop integrals, we are often faced with having to integrate polynomials to some non-integer power.
Since we can (essentially) only integrate using the definition of the $\Gamma$ function\footnote{
Technically, we can also solve integrals of the form $\int\D x, x^a (1+x)^b (z+x)^c$ using the methods presented in this section.
Hence this integral is just a shortcut to what we show here.}
the most complicated trivially solvable integral is
\begin{align}\tag{\ref{eq:gamma}}
\int_0^\infty \D x\ x^\alpha (a+b x)^\beta
  = \frac{\Gamma(1+\alpha)\Gamma(-1-\alpha-\beta)}{\Gamma(-\beta)}
        \frac{a^{1+\alpha+\beta}}{b^{1+\alpha}}
    \,.
\end{align}
It goes without saying that most real-world integrals are more complicated than this.

\subsection{The Mellin Barnes theorem}

\begin{observation}[Heavy-quark form factor]
Consider once again the following one-loop integral
\begin{align}
    I(a) = \int[\D k]\ \frac1{
        \big[k^2\big]^a
        \big[k^2+2k\cdot p\big]
        \big[k^2-2k\cdot q\big]
    }
\end{align}
with $p^2=q^2=m^2$, $(p+q)^2=s$ and some integer $a$.
After Feynman parametrisation we find (see eg. Observation \ref{obs:UFgraph})
\begin{align}
    I(a) = (-1)^a\frac{\Gamma(1-\epsilon)\Gamma(a+\epsilon)}{\Gamma(a)}
        \int\D x_1\D x_2\D x_3\ \delta(\cdots)
            x_1^{a-1}\frac{\big(x_1+x_2+x_3\big)^{-2+a+2\epsilon}}
                 {\big(m^2\, x_2^2 + (2m^2-s)\, x_2x_3 + m^2\, x_3^2\big)^{a+\epsilon}}\,.
\end{align}
We can trivially solve the $x_1$ integral using \eqref{eq:gamma}
\begin{align}
    I(a) = (-1)^a\frac{\Gamma(1-\epsilon)\Gamma(2-2a-2\epsilon)\Gamma(a+\epsilon)}
              {\Gamma(2-a-2\epsilon)}
        \int\D x_2\D x_3\ \delta(\cdots)
            \frac{\big(x_2+x_3\big)^{-2+2a+2\epsilon}}
                 {\big(m^2\,(x_2+x_3)^2 - s\,x_2x_3\big)^{a+\epsilon}}\,,
\end{align}
but now are stuck.
Obviously we cannot just use \eqref{eq:gamma} to solve this integral.
However, if we could somehow split the $\mathcal{F}$ polynomial into the terms $\propto m^2$ and those $\propto s$, we might stand a chance.
To do this, we note the Mellin-Barnes theorem.
\end{observation}

\begin{theorem}[Mellin-Barnes split]
We can split an arbitrary polynomial into two factors at the cost of an integration. You can think of this akin to an inverse Feynman parametrisation
\begin{align}\label{eq:mbth}
    \frac1{(A+B)^\lambda}
    = \frac1{2\pi\I} \frac1{\Gamma(\lambda)}
        \int_{-\I\infty}^{+\I\infty}\D z
            \ \Gamma(\lambda+z)\Gamma(-z) A^z B^{-z-\lambda}\,.
\end{align}
We ignore the factor $1/(2\pi \I)$ as it will cancel with the one from the residue theorem when we eventually calculate the contour integral.
The contour in question is chosen to separate the poles from the left and right $\Gamma$ functions.
We will soon see how this works in practise.

Crucially, we are not allowed to write eg.
\begin{align}
\frac{\Gamma(1+z)\Gamma(-z)}{\Gamma(z)} = -\Gamma(1-z)
\end{align}
since it would confuse left and right poles.
\end{theorem}
\begin{definition}[Left and right $\Gamma$ functions]
$\Gamma(z)$ functions has infinitely many poles at $z=-n$.
We hence call $\Gamma$ functions of the form $\Gamma(\cdots - z)$ right $\Gamma$ functions as their poles go right towards $+\infty$.
Similarly, $\Gamma(\cdots + z)$ are left $\Gamma$ functions.
\end{definition}

\begin{observation}[Heavy-quark form factor, Pt. 2]
Using \eqref{eq:mbth}, we write $I$ as
\begin{align}
\begin{split}
    I(a) = (-1)^a(m^2)^{-a-\epsilon}
         \frac{\Gamma(1-\epsilon)\Gamma(2-2a-2\epsilon)}
              {\Gamma(2-a-2\epsilon)}
        \int_{-\I\infty}^{\I\infty}&\D z
            \Big(\frac{-s}{m^2}\Big)^z
            \Gamma(-z)\Gamma(a+\epsilon+z)\\&
        \int\D x_2\D x_3\ \delta(\cdots)
            \frac{ x_2^z x_3^z }{(x_2+x_3)^{2+2z}}
\,.
\end{split}
\end{align}
Now we can use \eqref{eq:gamma} to solve this integral, using for example $\delta(\cdots) = \delta(1-x_3)$
\begin{align}
\int\D x_2\D x_3\ \delta(\cdots)
    \frac{ x_2^z x_3^z }{(x_2+x_3)^{2+2z}}
 = \frac{\Gamma(1+z)^2}{\Gamma(2+2z)}\,.
\end{align}
Therefore for $I$
\begin{align}
    I(a) = (-1)^a(m^2)^{-a-\epsilon}
         \frac{\Gamma(1-\epsilon)\Gamma(2-2a-2\epsilon)}
              {\Gamma(2-a-2\epsilon)}
        \int_{-\I\infty}^{\I\infty}&\D z
            \Big(\frac{-s}{m^2}\Big)^z
             \frac{\Gamma(-z)\Gamma(a+\epsilon+z)\Gamma(1+z)^2}{\Gamma(2+2z)}\,.
\label{eq:mbint}
\end{align}
\end{observation}


\subsection{Solving Mellin Barnes integrals using the residue theorem}

\begin{theorem}[Residue theorem for MB]
Assuming the MB integrand falls quickly enough, i.e. the arc $C'$ vanishes (see eg. Figure~\ref{fig:mbcontour}), we can solve a single MB integral using the residue theorem.
Consider
\begin{align}
    I    =\int_{-\I\infty}^{\I\infty}\D z\ \underbrace{
           x^z
           \Bigg(\prod_{i=1}^{n_L} \Gamma(a_i+z)^{c_i}\Bigg)
           \Bigg(\prod_{i=1}^{n_R} \Gamma(b_i-z)^{d_i}\Bigg)
           \big(\cdots\big)^{-1}
        }_{f(z)}
\end{align}
with $c_i,d_i > 0$. Then
\begin{align}
    I = \sum_{i=1}^{n_L} \sum_{n=0}^\infty \res_{-a-n} f(z)
      = \sum_{i=1}^{n_R} \sum_{n=0}^\infty \res_{ b+n} f(z)
\,.
\end{align}
\end{theorem}

\begin{lemma}[Residue formulas]
We remember that if $f(z)$ has a pole of order $n$ at $c$,
\begin{align}
    \res_c f(z) = \frac1{(n-1)!}\lim_{z\to c}
        \frac{\D^{n-1}}{\D z^{n-1}}\Big[(z-c)^nf(z)\Big]\,.
\end{align}
If $f(z) = g(z) h(z)$ and $h$ is free of poles at $c$, this means
\begin{align}
    \res_c f(z) = h(c)\res_c g(z)\,.
\end{align}
Finally,
\begin{align}
&\res_{-n}\Gamma(z)   &=& \frac{(-1)^n}{n!}\,,\\
&\res_{-n}\Gamma(z)^2 &=& \frac{2}{(n!)^2} \psi(n+1)\,,\\
&\res_{-n}\Gamma(z)^3 &=& \frac{(-1)^n}{2(n!)^4}\Big[\pi^2+9\psi(n+1)^2-3\psi'(n+1)\Big]\,,\\
&\res_{-n}\psi(z)     &=& -1\,.
\end{align}
Here we have also included the derivative of the $\Gamma$ function, the digamma function $\psi(x) = \Gamma'(x)/\Gamma(x)$.
Note that these rules can be implemented in Mathematica with an {\tt Assumption} as shown in Listing~\ref{lst:gamma}.
\end{lemma}

\begin{figure}
    \centering
    \renewcommand{\figurename}{Listing}
    \lstinputlisting[language=mathematica]{../code/gammaresidue.m}
    \caption{Mathematica calculation of the residue}
    \label{lst:gamma}
\end{figure}

\begin{observation}[Heavy-quark form factor, Pt. 3]
For simplicity we close the contour to the right as shown in
Figure~\ref{fig:mbcontour} for $a=1$.
We have one residue to calculate since $n_R=1$
\begin{align}\begin{split}
    I(a)&= (-1)^a(m^2)^{-a-\epsilon}
         \frac{\Gamma(1-\epsilon)\Gamma(2-2a-2\epsilon)}
              {\Gamma(2-a-2\epsilon)}
        \sum_{n=0}^\infty \res_n \Bigg[
            \Big(\frac{-s}{m^2}\Big)^z
             \frac{\Gamma(-z)\Gamma(a+\epsilon+z)\Gamma(1+z)^2}{\Gamma(2+2z)}
        \Bigg]
\\&
    = (-1)^{a+1}(m^2)^{-a-\epsilon}
         \frac{\Gamma(1-\epsilon)\Gamma(2-2a-2\epsilon)}
              {\Gamma(2-a-2\epsilon)}
    \sum_{n=0}^\infty
            \Big(\frac{s}{m^2}\Big)^n
             \frac{\Gamma(1+n)\Gamma(a+n+\epsilon)}{\Gamma(2+2n)}
    \,.
\end{split}\end{align}
We can solve the remaining sum and find
\begin{align}
I(a) &
  = (-1)^{a+1} (m^2)^{-a-\epsilon}
     \frac{\Gamma(1-\epsilon)\Gamma(2-2a-2\epsilon)}
          {\Gamma(2-a-2\epsilon)}
     \Gamma(1+\epsilon)
     \pFq21{1,1+\epsilon}{\tfrac32}{\frac s{4m^2}}\,.
\end{align}
\end{observation}
\begin{definition}[Hypergeometric function]
We define the hypergeometric function as
\begin{align}
    \pFq{p}{q}{a_1,\ldots,a_p}{b_1,\ldots,b_q}{z} = \sum_{n=0}^\infty
        \frac{(a_1)_n\cdots(a_p)_n}{(b_1)_n\cdots(b_q)_n}
        \frac{z^n}{n!}
\end{align}
with the Pochhammer symbol $a_n = \Gamma(a+n)/\Gamma(n)$ and the factorial $n!=\Gamma(1+n)$.
For certain values of $a_i$ and $b_i$, these functions can be expanded in $\epsilon$ using {\tt HypExp}~\cite{Huber:2005yg}.
\end{definition}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.9]
    %Contour C
    \draw[->] (-0.85, -4.5) --++ (0, 1.5) arc(-270:-90:-1) --++ (0, 5.5)
        node [below left,xshift=-5] {$C$};

    %Contour C'
    \draw[dashed] (-0.85, -4.9) arc (-90:90:4.9cm);
    \node at (3,3.5) {$C^\prime$};

    %Right poles
    \newcommand\poles[6]{
        \foreach\x in {0,...,#6}
            \draw[fill={#1},draw=none] (#2+\x,#3) circle (2.25pt);

        \node[fill={#1}] at #4 {$\Gamma(#5)$};
    }
    \poles{{rgb:black,4;white,8;green,3}}{- 1}{ 0}{(-2  , 1)}{1+z         }{-3}
    \poles{{rgb:black,4;white,4;blue,3} }{  0}{ 0}{( 2  , 1)}{ -z         }{ 3}
    \poles{{rgb:black,4;white,4;red,3}  }{-.5}{-2}{(-3.5,-3)}{1+z+\epsilon}{-4}

    %Coordinate system
    \draw[-latex] (-4.5,0)--(4.5,0) node [below right] {$\mathrm{Re}(z)$};
    \draw[-latex] (0,-4.5)--(0,4.5) node [below right] {$\mathrm{Im}(z)$};

    \foreach\x in {-4,...,4}
        \draw (\x,-0.1) --+ (0,0.2);
    \foreach\y in {-4,...,4}
        \draw (-0.1,\y) --+ (0.2,0);
\end{tikzpicture}
\caption{The contour for the MB integral \eqref{eq:mbint} for $a=1$}
\label{fig:mbcontour}

\end{figure}


\subsection{Solving Mellin Barnes integrals using Barnes lemma}

\begin{observation}[Resolving singularities]
\label{obs:resolve}
Let us consider the special case of $a=0$ (ignoring the fact that there are easier ways to obtain this result)
\begin{align}
    I(0) = (m^2)^{-\epsilon}
         \frac{\Gamma(1-\epsilon)\Gamma(2-2\epsilon)}
              {\Gamma(2-2\epsilon)}
        \int_{-\I\infty}^{\I\infty}&\D z
            \Big(\frac{-s}{m^2}\Big)^z
             \frac{\Gamma(-z)\Gamma(\epsilon+z)\Gamma(1+z)^2}{\Gamma(2+2z)}\,.
\end{align}
Setting $\epsilon\to0$ is obviously not possible since the left pole from $\Gamma(\epsilon+z)$ would collide with the right pole from $\Gamma(-z)$, meaning the contour would have to cross a pole.
However, if we moved the contour as shown in Figure~\ref{fig:mbcontourRes} then we can set $\epsilon\to0$.
By crossing a pole with the contour we have to explicitly add the residue of the pole
\begin{align}
    I(0) &= (m^2)^{-\epsilon} \Gamma(1-\epsilon)
        \Bigg[
        \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
            \Big(\frac{-s}{m^2}\Big)^z
             \frac{\Gamma(-z)\Gamma(\epsilon+z)\Gamma(1+z)^2}{\Gamma(2+2z)}
        -\underbrace{\res_0 \Big[\cdots\Big]}_{-\Gamma(\epsilon)}
        \Bigg]\\
    &= \frac1\epsilon - \log m^2
        + \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
            \Big(\frac{-s}{m^2}\Big)^z
             \frac{\Gamma(-z)\Gamma(+z)\Gamma(1+z)^2}{\Gamma(2+2z)} + \mathcal{O}(\epsilon)\,.\label{eq:mbresolved}
\end{align}
At this stage we may expand in $\epsilon$ to whatever order we require.

Note that we can also numerically integrate~\eqref{eq:mbresolved}.
\end{observation}



\begin{figure}
\centering
\begin{tikzpicture}[scale=.9]
    %Contour C
    \draw[->] (-0.25, -4.5) --++ (0, 1.5) arc(90:270:-1) node [midway,  below right] {$C$}--++ (0, 5.5)
        ;
    %Contour C'
    \draw[dashed, ->] (-0.5, -4.5) --++ (0, 1.5) --++ (0,2) node [midway, below left] {$C'$}--++ (0, 5.5);

    \centerarc [line width=0.15mm,->](-1,0)(315:290:1.8)

    %Right poles
    \newcommand\poles[6]{
        \foreach\x in {0,...,#6}
            \draw[fill={#1},draw=none] (#2+\x,#3) circle (2.25pt);

        \node[fill={#1}] at #4 {$\Gamma(#5)$};
    }
    \poles{{rgb:black,4;white,8;green,3}}{- 1}{ 0}{(-2  , 1)}{1+z         }{-3}
    \poles{{rgb:black,4;white,4;blue,3} }{  0}{ 0}{( 2  , 1)}{ -z         }{ 3}
    \poles{{rgb:black,4;white,4;red,3}  }{+.25}{-2}{(-3.5,-3)}{z+\epsilon}{-4}

    %Coordinate system
    \draw[-latex] (-4.5,0)--(4.5,0) node [below right] {$\mathrm{Re}(z)$};
    \draw[-latex] (0,-4.5)--(0,4.5) node [below right] {$\mathrm{Im}(z)$};

    \foreach\x in {-4,...,4}
        \draw (\x,-0.1) --+ (0,0.2);
    \foreach\y in {-4,...,4}
        \draw (-0.1,\y) --+ (0.2,0);
\end{tikzpicture}
\caption{The contour for the MB integral \eqref{eq:mbint} for $a=0$}
\label{fig:mbcontourRes}
\end{figure}

\begin{observation}[Slightly contrived box]
Consider this (slightly contrived) example
\begin{align}
  I(a) = \int[\D k]\frac1{\big[k^2\big]
                          \big[(k-p_1)^2\big]
                          \big[(k+p_2)^2\big]
                          \big[(k-p_1+p_4)^2\big]}\,,
\end{align}
with $p_1^2 = p_3^2 = (p_1+p_2)^2 = (p_1-p_3)^2 = s$ and $p_2^2 = p_4^2 = 0$.
This example could for example occur when calculating the boundary condition for some differential equations of Feynman integrals.
There have usually much simpler and often unphysical kinematics and are then evolved to the physics situation.
We have
\begin{align}
    I
    &= (-s)^{-2-\epsilon} \Gamma(1-\epsilon)\Gamma(2+\epsilon)
       \int\D x_1\D x_2\D x_3 \D x_4\delta(\cdots)
       \frac{\big(x_1+x_2+x_3+x_4\big)^{2\epsilon}}
            {\big(x_1\,x_2+x_3\,(x_2+x_4)\big)^{2+\epsilon}}
        \,.
\end{align}
We split the $x_3$ term using Mellin Barnes
\begin{align}\begin{split}
    I
    = (-s)^{-2-\epsilon} \Gamma(1-\epsilon)
       \int_{-\I\infty}^{+\I\infty}\D z
       &\Gamma(-z)\Gamma(2+\epsilon+z)
       \int\D x_1\D x_2\D x_3 \D x_4\delta(\cdots)\\&
       \ x_1^{-2-z-\epsilon}
       \ x_2^{-2-z-\epsilon}
       \ x_3^{z}
       \ (x_3+x_4)^{z}
       \ (x_1+x_2+x_3+x_4)^{2\epsilon}
       \,.
\end{split}\end{align}
We can now obtain
\begin{align}\begin{split}
    I
    = (-s)^{-2-\epsilon}
      \frac{\Gamma(1-\epsilon) \Gamma(-\epsilon)}
           {\Gamma(-2\epsilon)}
       \int_{-\I\infty}^{+\I\infty}\D z
       \frac{\Gamma(-z)\Gamma(1+z)\Gamma(-1-\epsilon-z)^2\Gamma(2+\epsilon+z)}
            {\Gamma(-\epsilon-z)}\,.
\end{split}\end{align}
We could obviously solve this with the residue theorem but we have two left poles or two right poles, one of them involving $\Gamma(\cdots -z)^2$.
Let us try to expand in $\epsilon$ by resolving the singularities
\begin{align}\begin{split}
    I
    &= (-s)^{-2-\epsilon}
      \frac{\Gamma(1-\epsilon) \Gamma(-\epsilon)}
           {\Gamma(-2\epsilon)}
      \Bigg(
           \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
           \frac{\Gamma(-z)\Gamma(1+z)\Gamma(-1-\epsilon-z)^2\Gamma(2+\epsilon+z)}
                {\Gamma(-\epsilon-z)}
           -\res_{-1-\epsilon}\Big(\cdots\Big)
      \Bigg)
    \\
    &= 2(-s)^{-2-\epsilon}\Bigg[
        \frac1{\epsilon^2} - 2\zeta_2
        + \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
            \Gamma(1+z)\Gamma(2+z)\Gamma(-1-z)^2
        \\&\qquad\qquad- \Bigg(2\zeta_3
        + \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
            \Gamma(1+z)\Gamma(2+z)\Gamma(-1-z)^2
            \Big(\psi(-z)-2\psi(-1-z)+\psi(2+z)\Big)
         \Bigg)\epsilon
    \Bigg]\,.
    \label{eq:contrv}
\end{split}\end{align}
\end{observation}

\begin{theorem}[Barnes Lemma]
We have two Barnes Lemma that can be used to calculate integrals
\begin{align}
	\int_{-\I\infty}^{+\I\infty} \D z
    \ \Gamma(\lambda_1+z)\Gamma(\lambda_2+z)
      \Gamma(\lambda_3-z)\Gamma(\lambda_4-z)
    &=\frac{\Gamma(\lambda_{13})\Gamma(\lambda_{14})
            \Gamma(\lambda_{23})\Gamma(\lambda_{24})}
           {\Gamma(\lambda_{1234})}\,,
   \label{eq:barnes1}
    \\
	\int_{-\I\infty}^{+\I\infty} \D z
    \ \frac{\Gamma(\lambda_1+z)\Gamma(\lambda_2+z)
            \Gamma(\lambda_3+z)\Gamma(\lambda_4-z)
            \Gamma(\lambda_5-z)}
           {\Gamma(\lambda_{12345}+z)}
	&=\frac{\Gamma(\lambda_{14})\Gamma(\lambda_{24})
            \Gamma(\lambda_{34})\Gamma(\lambda_{15})
            \Gamma(\lambda_{25})\Gamma(\lambda_{35})}
           {\Gamma(\lambda_{1245})\Gamma(\lambda_{1345})\Gamma(\lambda_{2345})}\,,
   \label{eq:barnes2}
\end{align}
with $\lambda_{12}=\lambda_1+\lambda_2$ etc.
The Barnes Lemma are particularly useful when dealing with multiple MB integrals.
Further, they can be used to generate further identities.
For example, taking the derivative of \eqref{eq:barnes1} w.r.t. $\lambda_1$
\begin{align}\begin{split}
	\int_{-\I\infty}^{+\I\infty} \D z
    \ \Gamma(\lambda_1+z)\Gamma(\lambda_2+z)
      \Gamma(\lambda_3-z)\Gamma(\lambda_4-z)
      \psi(\lambda_1+z)
    &=\frac{\Gamma(\lambda_{13})\Gamma(\lambda_{14})
            \Gamma(\lambda_{23})\Gamma(\lambda_{24})}
           {\Gamma(\lambda_{1234})}\\
    &\qquad\times\Big[\psi(\lambda_{13})+\psi(\lambda_{14})-\psi(\lambda_{1234})\Big]
           \,.
\end{split}\end{align}
\end{theorem}

\begin{observation}[Slightly contrived box, Pt. 2]
We can now calculate the $\mathcal{O}(\epsilon^0)$ term of \eqref{eq:contrv}, and by taking the derivative w.r.t. the $\lambda_i$
\begin{align}
    I
    &= 2(-s)^{-2-\epsilon}\Bigg[
        \frac1{\epsilon^2} - \zeta_2
        - \Bigg(-\zeta_3 + \gamma_E \zeta_2
        + \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
            \Gamma(1+z)\Gamma(2+z)\Gamma(-1-z)^2
            \psi(-z)
         \Bigg)\epsilon
    \Bigg]\,.
\end{align}
It is possible to write $\psi(-z)$ as $\psi(1+z)$ and again use the Barnes Lemmas.
Alternatively, we can note that
\begin{align}
\Gamma(-1-z)\Gamma(2+z)
    = -\frac1{1+z}\Gamma(-z)\Gamma(2+z)
    = -\Gamma(1+z)\Gamma(-z)\,.
\end{align}
We are now allowed do this, since we have specified the contour explicitly and no longer need to distiguish left and right poles.
We write
\begin{align}\begin{split}
\int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
  \Gamma(1+z)\Gamma(2+z)\Gamma(-1-z)^2
  \psi(-z)
&= -
\int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z
  \Gamma(1+z)^2\Gamma(-z)\Gamma(-1-z)
  \psi(-z)
\\
&= -2\zeta_3 - \gamma_E \zeta_2\,.
\end{split}\end{align}
With this
\begin{align}
    I
    &= 2(-s)^{-2-\epsilon}\Bigg[
        \frac1{\epsilon^2} - \zeta_2 - 3\zeta_3\epsilon
    \Bigg]
    \,.
\end{align}
Naturally, doing this manually quickly becomes annoying, the higher we want to go in $\epsilon$.
\end{observation}


\begin{observation}[Strategies]
In the example above, the integral has trivial dependence on external kinematics.
In general, this means that it can be written as $I=(Q^2)^{\ell\epsilon} f(\epsilon)$ where $f$ is some function that only depends on the dimensionality of spacetime.
Using the methods described in Observation~\ref{obs:resolve}, we are able to expand $f$ around $\epsilon=0$ and calculate the remaining integrals numerically.
We will shortly see a particularly clever way to exploit this.

If that were not the case, we have to explicitly sum the residues.
Mathematica is capable of doing this in a limited number of cases and the {\tt FORM} code {\tt XSummer}~\cite{Moch:2005uc} covers many more.
However, all of this works only if there is only a single integration left.
Finding a series expansion for multiple MB integrals is highly non-trivial and subject to active research (eg. \cite{Ananthanarayan:2020fhl}).
\end{observation}

\begin{theorem}[PSLQ algorithm]
Given some real (notionally transcendental) numbers $a_i$, we can find rational numbers $r_i$ such that
\begin{align}
r_0\,a_0 + r_1\,a_1+\cdots+r_n\,a_n = 0
\end{align}
using black magic, also known as number theory.
An implementation of this algorithm can be found with these lecture notes.
\end{theorem}

\begin{observation}[Using PSLQ]
If we can guess a basis of transcendetal numbers such as
\begin{align}
\big\{a_i\big\}_{i=1,\cdots,n} = \{
    1, \zeta_2, \zeta_3, \zeta_4, \cdots,
    \log2, \log2\zeta_2, \cdots \}
    \,,
    \label{eq:pslqbase}
\end{align}
we can numerically evaluate a MB integral to very high precision using {\tt NIntegrate} as $a_0$ and find an analytic solution
\begin{align}
    a_0 = \frac{r_1}{r_0}a_1
        + \frac{r_2}{r_0}a_2
        + \cdots
        + \frac{r_n}{r_0}a_n
        \,.
\end{align}
Depending on which factors are pulled out before $a_0$ is calculated, we might need to add $\gamma_E$ and $\pi$ to the basis.
The larger $n$ is, the more digits we need in $a_0$ which quickly gets expensive.

In our example, expanding \eqref{eq:contrv} to $\epsilon^4$, we have for the integral part
\begin{align}
\begin{split}
    \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z\Big(\cdots\Big)
   &={\tt 3.2898681336964528729448303332920503784378998024136}\\[-1em]
   &-{\tt 2.4041138063191885707994763230228999815299725846810}\ \epsilon^1\\
   &+{\tt 2.1646464674222763830320073930823358055495019038375}\ \epsilon^2\\
   &-{\tt 9.9830729114759243254510727388545706244427677056444}\ \epsilon^3\\
   &-{\tt 2.8676528331332807519186620879948731784305070422237}\ \epsilon^4
\end{split}
\end{align}
calculated to 50 digits accuracy (cf. Listing~\ref{lst:pslq}).
We can now use PSLQ with the following basis
\begin{align}\def\s{\ \ }
    \{
        \underbrace{\zeta_2}_{w=2},\s
        \underbrace{\zeta_3}_{w=3},\s
        \underbrace{\zeta_4}_{w=4},\s
        \underbrace{\zeta_5,\ \zeta_2\zeta_3}_{w=5},\s
        \underbrace{\zeta_6,\ \zeta_3\zeta_3}_{w=6}
    \}
    \label{eq:basisEx}
\end{align}
to find
\begin{align}
    \int_{-\tfrac12-\I\infty}^{-\tfrac12+\I\infty}\D z\Big(\cdots\Big)
    &= 2\zeta_2
      -2\zeta_3\epsilon^1
      +2\zeta_4\epsilon^2
      -\Big(2\zeta_5-4\zeta_2\zeta_3\Big)\epsilon^3
      -\Big(\frac{17}2 \zeta_6+4 \zeta_3^2\Big)\epsilon^4
      +\mathcal{O}(\epsilon^5)\,.
\end{align}
The basis has the added property that is has constant transcendentality.

In~\cite{Laporta:2017okg}, Stefano Laporta calculated 1100 digits of the 4-loop QED correction to $(g-2)_e$ and fitted a basis with more than hundred elements.

\end{observation}

\begin{figure}
    \centering
    \renewcommand{\figurename}{Listing}
    \lstinputlisting[language=mathematica,firstline=159,lastline=184]{../code/mb.m}
    \caption{Numerical evaluation and PSLQ}
    \label{lst:pslq}
\end{figure}

\begin{definition}[Transcendentality]
For each element of the PSLQ basis $a$ we can define a transcendentality $w(a)$.
This property is multiplicative, i.e. $w(a_1\times a_2) = w(a_1) + w(a_2)$.
Some transcendentalities can be found in Table~\ref{tab:transcendentality}.
Note that even though \eqref{eq:basisEx} has what is called constant transcendentality (at each order $w$ is constant), this is not a very useful concept for QCD.
However, certain symmetries such as conformal symmetry result in constant transcendentality at the level of the matrix element.
\end{definition}

\begin{figure}
    \centering
    \renewcommand{\figurename}{Table}
    \begin{tabular}{r|r|p{10cm}}
         $w$ & $\#$ & values\\\hline
        0   &  1   & 1\\
        1   &  1   & $\log2$\\
        2   &  2   & $\zeta_2$,
                     $\log^22$\\
        3   &  3   & $\zeta_3$,
                     $\zeta_2\log2$,
                     $\log^32$\\
        4   &  5   & $\zeta_4$,
                     $\zeta_3\log2$,
                     $\zeta_2\log^22$,
                     $\log^42$,
                     $\Li_4(\tfrac12)$\\
        5   &  8   & $\zeta_5$,
                     $\zeta_2\zeta_3$,
                     $\zeta_4\log2$,
                     $\zeta_3\log^22$,
                     $\zeta_2\log^32$,
                     $\log^52$,
                     $\log2\Li_4(\tfrac12)$,
                     $\Li_5(\tfrac12)$\\
        6   & 13   & $\zeta_6$,
                     $\zeta_3\zeta_3$,
                     $\zeta_5\log2$,
                     $\zeta_2\zeta_3\log2$,
                     $\zeta_4\log^22$,
                     $\zeta_3\log^32$,
                     $\zeta_2\log^42$,
                     $\log^62$,
                     $\zeta_2\Li_4(\tfrac12)$,
                     $\log^22\Li_4(\tfrac12)$,
                     $\log2\Li_5(\tfrac12)$,
                     $\Li_6(\tfrac12)$
                     $H_{0,0,0,0,1,1}(\tfrac12)$\\\hline
        $w$ &      & $\zeta_i$, $\Li_i(\tfrac12)$, $\cdots$
    \end{tabular}
    \caption{Some transcendental numbers up to $w=6$, adapted from~\cite{Laporta:2018eos}}
    \label{tab:transcendentality}
\end{figure}
