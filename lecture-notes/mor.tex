%!TEX root=notes

\section{Method of regions}

In general, the calculation of master integrals with full dependence of any parameter is very difficult and time consuming.
However, in many cases this is not needed, often because the parameters have a strong hierarchy.
This could either mean that the electron mass $m^2$ is much smaller than the momentum transfer $Q^2$ or that the $W$ boson mass $m_W^2$ is much larger.
In these cases, we instead calculate the integrals expanded in the small parameter ($m/Q^2$ or $Q^2/m_W^2$).
The technique used to achieve this is the method of regions~\cite{Beneke:1997zp}.

\subsection{Momentum space}

\begin{example}[Heavy particle]
\label{ex:morheavy}
Let us first consider the case of a heavy particle $m_X^2\gg s$ in
\begin{align}
    I = \int[\D k]\ \frac{1}{
        \big[k^2-m_X^2+\io\big]
        \big[k^2-2k\cdot p+\io\big]
    }
\end{align}
with $p^2 = m^2 \ll m_X^2$.
The naive expansion of the integrand
\begin{align}
    \frac{1}{
        \big[k^2-m_X^2+\io\big]
        \big[k^2-2k\cdot p+\io\big]
    } = -\frac1{m_X^2}\frac1{
        \big[k^2-2k\cdot p+\io\big]
    }
    +\mathcal{O}\Big(\frac1{m_X^4}\Big)
\end{align}
is obviously wrong since integration and expansion does not commute.
The expansion is wrong in the region where $k\sim m_X$.
\end{example}

\begin{theorem}[The method of regions in momentum space]
We can expand an integral already at integrand level by identifying
all relevant regions, expanding in those up to whatever power we desire and adding them up.
In principle infinitely many such regions exist, but most of them
vanish in dimensional regularisation.

\begin{proof}
We only prove the example, though this argument can be formalised.

We split the integration at the scale $\Lambda$ which is
$m\ll\Lambda\ll m_X$
\begin{align}
\begin{split}
    I &
    = \int_0^\Lambda[\D k]\ \frac{1}{
        \big[k^2-m_X^2\big]
        \big[k^2-2k\cdot p\big]
    }
    + \int_\Lambda^\infty[\D k]\ \frac{1}{
        \big[k^2-m_X^2\big]
        \big[k^2-2k\cdot p\big]
    }
    \\&
   = \underbrace{-\frac1{m_X^2}\int_0^\Lambda[\D k]\ \frac{1}{
        \big[k^2-2k\cdot p\big]
    }}_{\text{soft}}
    + \underbrace{\int_\Lambda^\infty[\D k]\ \Bigg(
        \frac{1}{
            \big[k^2-m_X^2\big]
            \big[k^2\big]
        }
        +
        \frac{4(k\cdot p)^2}{
            \big[k^2-m_X^2\big]^3
            \big[k^2\big]
        }\Bigg)}_{\text{hard}} + \mathcal{O}\Big(\frac1{m_x^4}\Big)
    \label{eq:split}
\end{split}
\end{align}
The first term is generally referred to the soft region and the second term as the hard region.
We now need to relate these integrals back to what we can calculate.
It turns out we can just integrate over the whole range since the difference is scaleless, eg.
\begin{align}
    \int_\Lambda^\infty[\D k]\ \frac{1}{
        \big[k^2-2k\cdot p\big]
    }
    \stackrel{m\ll\Lambda}=
    \sum_{i=0}^\infty
    \int_0^\infty[\D k]\ \frac{(m^2)^i}{\big[k^2\big]^{i+1}} = 0\,.
\end{align}
We can now solve the integrals in \eqref{eq:split} over the whole range and obtain
\begin{align}
    I
    = \frac1\epsilon+1+\log\frac{\mu^2}{m_X^2}
        + \frac{m^2}{2m_X^2}\Big(1+2\log\frac{m^2}{m_X^2}\Big)\,.
\end{align}
This agrees with what we would obtain had we expanded the original integral after integration.
\end{proof}
\end{theorem}

\begin{observation}
This is directly connected to the concept of effective field theories (EFT) where we add new operators $\mathcal{Q}$ and Wilson coefficients $\mathcal{C}$ to our Lagrangian
\begin{align}
\mathcal{L} \to \mathcal{L} + \sum_i \mathcal{C}_i\mathcal{Q}_i\,.
\end{align}
The hard region corresponds to the renormalisation of the Wilson coefficients while the soft region is the loop calculation in the EFT.
In this language, the different orders in the expansion correspond directly to the dimensionality of the operators.

This type of expansion is very useful if we want to integrate out a heavy scale.
However, we need more tools if we want to deal with light scales as well.
\end{observation}
\begin{definition}[Light-cone coordinates]
Let $p_i=E_i(1, \vec n_i\beta_i)$ be a high energetic particle ($\beta=1-\mathcal{O}(\lambda^2)$), moving in the $\vec n_i$ direction.
We now decompose its momentum into light-cone basis vectors $n_i =(1,\vec n_i)/\sqrt{2}$ and $\bar{n}_i=(1,-\vec n_i)/\sqrt{2}$.\footnote{Our definition of $n$ and $\bar{n}$ differs from the standard convention by the normalisation factor $1/\sqrt{2}$.}
We now write any momentum $p_j$ as
\begin{align}
    p_j
    = (n_i\cdot p_j) \bar n_i + (\bar n_i\cdot p_j)n_i + p_j^{(\perp,i)}
    = p_j^{(+,i)} + p_j^{(-,i)} + p_j^{(\perp,i)}
    = (n_i\cdot p_j, \bar n_i\cdot p_j, p_{j,\perp})_i
    \, .
\end{align}
For the case $i=j$ our original energetic particle $p_i$ is now
\begin{align}
p_i = \big( E_i (1-\beta_i)/\sqrt2, E_i(1+\beta_i)/\sqrt2, p_{i,\perp}\big)_i
\sim (\lambda^2,1,\lambda)_i\,.
\end{align}
We have especially $p_i^2 = m^2 \sim \lambda^2$ as planned
\begin{align}
p_i^2 = 2p_i^{(+,i)} \cdot p_i^{(-,i)} + \big(p_i^{(\perp,i)}\big)^2
  = m^2 \sim \lambda^2\,.
\end{align}
Here we have used that, since $\bar n^2=n^2=n\cdot p_\perp = 0$,
\begin{align}
p_j^{(\pm,i)} \cdot p_k^{(\pm,i)}
=
p_j^{(\pm,i)} \cdot p_k^{(\perp,i)} = 0
\end{align}
\end{definition}

\begin{definition}[Momentum regions]
Using light-cone coordinates, a region of the loop momentum $k$ is then defined as a specific choice of parameters $a$, $b$, and $c$ where $k \sim (\lambda^a, \lambda^b, \lambda^c)_i$ for a given direction $n_i$.
At this point we expect an infinite number of regions corresponding to the infinite possible choices of $a$, $b$, and $c$.\footnote{One can show that $c=(a+b)/2$ with other choices resulting in scaleless integrals.}
Fortunately, almost all of the infinite number of regions turn out to be zero.

Common momentum regions that contribute are
\begin{subequations}\label{eq:regions}
    \begin{align}
        \mbox{hard:} &%
        \quad k\sim(1,1,1) \label{eq:khard} \\
        \mbox{soft:} &%
        \quad k\sim(\lambda,\lambda,\lambda)\label{eq:ksoft} \\
        \mbox{collinear:} &%
        \quad k\sim(\lambda^2,1,\lambda) \label{eq:kcoll}\\
        \mbox{ultrasoft:} &%
        \quad k\sim(\lambda^2,\lambda^2, \lambda^2)\,, \label{eq:kus}
    \end{align}
\end{subequations}
Note that these scalings are only defined for a given direction.
If we have multiple directions of energetic particles as is often the case, we have to distinguish them, leading to more regions.

We might be able to reduce the number of directions if some particles are back-to-back as is the case in $\gamma^*\to e^+(n_1)e^-(n_2)$.
The $(\lambda^2,1,\lambda)_2$ region is actually the same as the $(1,\lambda^2,\lambda)_1$ region, reducing the amount of bookkeeping required.

In what follows, I will drop the index for the direction when it is unambiguous.
\end{definition}

\begin{example}[Light particles]\label{ex:lightmor}
Consider the following integral
\begin{align}
    I =
    \begin{gathered}
    \begin{tikzpicture}
        \draw (-1.5,0)--(-1,0);
        \centerarc[dashed](0,0)(180:0:1)
        \centerarc[](0,0)(180:0:-1)
        \draw (1.5,0)--(1,0);
        \draw[->] (1.5,-0.1) --+ (0.3,0) node[below] {$p+q$};
        \centerarc[->](0,0)(100:80:1.2) \node at (0,+1.5) {$k-q$};
        \centerarc[->](0,0)(260:280:1.2) \node at (0,-1.5) {$k+p$};
        \fill (0,1) circle (3pt);
    \end{tikzpicture}
    \end{gathered}
    = \int[\D k]\frac1{
        \big[(k+p)^2-M^2\big]
        \big[(k-q)^2-m^2\big]^2
    }\,,\label{eq:exampleMOR}
\end{align}
with $q^2 = m^2 \ll p^2=M^2 \sim (p+q)^2=s$.
A convenient choice is $p\sim(1,1,0)$, $q\sim(0,1,\lambda)$ which results in
\begin{align}
    p^2 = 2p_+\cdot p_- = M^2 \sim 1
    \qquad\text{and}\qquad
    q^2 = q_\perp^2 = m^2 \sim \lambda^2
    \,.
\end{align}
By trying all sensible regions, we find that all but two are scaleless.
Consider eg. $k\sim(\lambda,\lambda,\lambda)$
\begin{align}
    I_s
    = \frac1{\lambda^3}\int[\D k] \frac1{
        \big[2k_+\cdot q_-\big]^2
        \big[2k\cdot p\big]
    }
    = 0\,,
\end{align}
where we have used that $k_+\cdot q_- = k\cdot q_-$.
The only scaleful regions are $k\sim(1,1,1)$ (hard) and $k\sim(\lambda^2,1,\lambda)$ (collinear).

\begin{itemize}
    \item
    In the hard region we have $k\sim(1,1,1)$ and we can write
    \begin{align}
        I_h
        = \int[\D k]\frac1{
            \big[(k+p)^2-M^2\big]
            \big[(k-q_-)^2\big]^2
          }
          +\mathcal{O}(\lambda^2)\,.
    \end{align}
    The leading term is just the original integral with $m=0$ since $q_-^2=0$.
    In real-world applications, we might be able to obtain this from already known QCD results.
    We have
    \begin{align}\begin{split}
        I_h
       &= \Gamma(1-\epsilon)\Gamma(\epsilon)
          \int\D x_1\D x_2\delta(\cdots)
          x_1
          \frac{\big(x_1+x_2\big)^{-2+2\epsilon}}
               {\big(M^2 x_2^2 + (M^2-s)x_1x_2\big)^\epsilon}\\
       &= \Gamma(1-\epsilon)\Gamma(\epsilon)
          \int_0^1\D x_2 (1-x_2) x_2^{-\epsilon}\big(M^2-s+s x_2\big)^{-\epsilon}
          \\
       &= \frac{(1-x)^\epsilon}{M^2}\Big(\frac{M^2}{\mu^2}\Big)^{-\epsilon}
          \Bigg(
              \frac{\Gamma(1-\epsilon)\Gamma(1+\epsilon)}{1-\epsilon}
              \pFq21{1-\epsilon,1+\epsilon}{2-\epsilon}{x}
              \\&\qquad\qquad\qquad+
              \Gamma(1-\epsilon)\Gamma(\epsilon)
              \pFq21{-\epsilon,1+\epsilon}{2-\epsilon}{x}
          \Bigg)
          \\
       &= \frac{1-x}{M^2}\Bigg(
            \frac1\epsilon+\Big(2+\frac1x\Big)\log(1-x)+
            \log\frac{\mu^2}{M^2}
        \Bigg)+\mathcal{O}(m^2)\,.
    \end{split}\end{align}
    where we have set $\delta(1-x_1+x_2)$ and defined $x=s/(s-M^2)$.
    Here we have used a ${}_2F_1$ function that we will encounter again in Section~\ref{sec:mb} and expanded it with {\tt HypExp}~\cite{Huber:2005yg}.

    \item
    In the collinear region, we have account for a factor $\lambda^4$ from $\D^4k$.
    Hence, we have
    \begin{align}\begin{split}
        I_c
        &= \int[\D k] \frac1{
            \big[2k_-\cdot p_+\big]
            \big[k^2 - 2k\cdot q\big]^2
        }\\
        &=-\Gamma(1-\epsilon)\Gamma(1+\epsilon)
            \int\D x_1\D x_2\delta(\cdots)
            \frac{x_1^{-1+\epsilon}}
                 {\big(m^2x_1+(m^2+M^2-s)x_2\big)^{1+\epsilon}}
         \\
        &=-M^{-2}\Big(\frac{m^2}{\mu^2}\Big)^{-2\epsilon}
            \Gamma(1-\epsilon)\Gamma(\epsilon)(1-x)
        \\
        &= \frac{1-x}{M^2}\Bigg(
            -\frac1\epsilon + \log\frac{m^2}{\mu^2}
          \Bigg)
        \,.
    \end{split}\end{align}
    Here, we once again see that we have lost some complexity since the $\U$ polynomial is trivial.

\end{itemize}

Both $I_c$ and $I_h$ are divergent but their sum is finite as was the original integral. However, $I_h\propto (M^2/\mu^2)^{-\epsilon}$ and $I_c\propto (m^2/\mu^2)^{-\epsilon}$ so that $\log(M^2/m^2)$ remains
\begin{align}
    I_c + I_h = -\frac{1-x}{xM^2}\Big(x\log\frac{M^2}{m^2}+(1-2x)\log(1-x)\Big)\,.
\end{align}
This is a common feature in the method of regions:
all regions are separately more divergent than the total result which can introduce new logarithms.
\end{example}


\subsection{Parameter space}
While the method of region is doubtlessly an invaluable tool, especially when we want to connect its regions to effective theories.
However, if all we want to do is calculate integrals without having to bother with a physical intuition, it can be unwieldy, especially since some regions are not visible for a given momentum routing.
It turns out we can formulate the method of regions at the level of the Feynman parameters which is much more general and allows for easier automation~\cite{Pak:2010pt,Jantzen:2012mw}.

Since an important aspect of the method of region is scalelessness, we need to formalise this at the level of the Feynman parameters.

\begin{lemma}[Scaleless integrals]
We call an integral scaleless if rescaling the loop momentum and/or the external momenta results in a global factor.
These integrals vanish in dimensional regularisation.
For example,
\begin{align}
    I
    =  \int[\D k] \frac1{\big[k^2\big]^n}
    \to\int[\D (\alpha k)] \frac1{\big[(\alpha k)^2\big]^n}
    =   \alpha^{d-2n}I = 0\,.
\end{align}
In the language of Feynman parameters, the integral vanishes if we can rescale a strict subset of parameters
\begin{align}\begin{split}
    \U(x_1,\cdots,\alpha x_j,\cdots,x_n) &=
    \alpha^u\U(x_1,\cdots,x_j,\cdots,x_n) \\
    \F(x_1,\cdots,\alpha x_j,\cdots,x_n) &=
    \alpha^f\F(x_1,\cdots,x_j,\cdots,x_n)\,,
\end{split}\end{align}
for some scaling dimensions $u$ and $f$.
For simplicity we only consider $\U\F \to \alpha^{u+f}\U\F$.
\end{lemma}


\begin{theorem}[The method of regions in parameter space]
\label{thm:asy}
To expand a parameter integral, we multiply the small kinematic invariants with appropriate power of $\lambda$ in $\F$.
A region is defined as a $(n+1)$-dimensional vector $\vec v=(1,v_1,\cdots,v_n)$ where $x_i\sim \lambda^{v_i}$.
We can relate $\vec v$ to the complex hull of the point cloud that is spanned by $\U$ and $\F$ (see proof and Example \ref{ex:expara} for details).
This is implemented in public codes such as {\tt asy}~\cite{Pak:2010pt,Jantzen:2012mw} or {\tt pySecDec}~\cite{Heinrich:2021dbf}.
\end{theorem}

\begin{example}[Example in parameter space]
\label{ex:expara}
For the example~\eqref{eq:exampleMOR} we have $\vec v_h = (1, 0, 0)$ and $\vec v_c = (1,0,-1)$.
We have hence
\begin{align}
    I_h&= -\Gamma(1-\epsilon)\Gamma(1+\epsilon)
          \int\D x_1\D x_2\delta(\cdots)
          x_2\frac{(x_1+x_2)^{-1+2\epsilon}}
                  {\big(M^2 x_1^2 + (M^2-s) x_1\, x_2\big)^{1+\epsilon}}\,,\\
    I_c&= -\Gamma(1-\epsilon)\Gamma(1+\epsilon)
          \int\D x_1\D x_2\delta(\cdots)
          \frac{x_2^{2\epsilon}}{\big(m^2 x_2^2 + (M^2 - s) x_1\, x_2\big)^{1+\epsilon}}\,.
\end{align}
The integration of the Feynman parameters is now trivial again as above.
\end{example}


\begin{proofof}{Theorem \ref{thm:asy}}
Consider a term in the Lee-Pomeransky polynomial $\F+\U$ (cf. \eqref{eq:leepomeransky}), it will have the structure
\begin{align}
    \lambda^{r_0} x_1^{r_1} \cdots x_n^{r_n}
    \equiv
    \vec r = (r_0, r_1, \ldots, r_n)\,.
\end{align}
We view the terms as a point cloud $C_{\F+\U}$ in an $(n+1)$ dimensional space that we sometimes split as $C_{\F+\U}=C_\F\cup C_\U$.
$\F$ is homogeneous, i.e. $r_1+\cdots+r_n=\ell+1$, meaning that all terms in $C_\F$ live on an $n$ dimensional hyperplane.
Were the dimensionality lower than $n$, we could factor out a Feynman parameter, making the integral scaleless.

For a given region, this term will scale as
\begin{align}
    \lambda^{r_0} \big(x_1\lambda^{v_1}\big)^{r_1}
           \cdots \big(x_n\lambda^{v_n}\big)^{r_n}
    \sim \lambda^{r_0+v_1r_1+\cdots+v_nr_n}
       = \lambda^{\vec v\cdot \vec r}
    \,.
\end{align}
After expanding $\F$ and $\U$ in $\lambda$, all remaining terms have to have the same scaling since otherwise the expansion would not have been complete.
In other words, the remaining terms belong to a hyperplane orthogonal to $\vec v$.
Points that are above this hyperplane are more suppressed in $\lambda$ since they have larger $r_0$, the power of $\lambda$.

If the dimensionality of this hyperplane is lower than $n$, the integral is scaleless.
Thus we are looking for facets of the envelope of the point cloud $C_{\F+\U}$.
The $\vec v$ are the (inwards facing) normal vectors of these facets with $v_1>0$.
This automatically selects only the bottom facets.

\end{proofof}

\newcommand\rn[1]{\vec r^{\,(#1)}}
\newcommand\vn[1]{\vec v^{\,(#1)}}
\begin{example}[Construction of $\vec v$]
For the example~\eqref{eq:exampleMOR} we have
\begin{align}
\begin{split}
    \F&= M^2 x_1^2 + M^2 x_1 x_2 - s x_1 x_2 + m^2 x_2^2\lambda\,,\\
    \U&= x_1+x_2\,.
\end{split}
\end{align}
This results in the point cloud
\begin{align}
\begin{split}
C_\F&= \Big\{(0, 2, 0), (0, 1, 1), (0, 1, 1), (1, 0, 2)\Big\}\\
C_\U&= \Big\{(0, 1, 0), (0, 0, 1)\Big\}
\end{split}
\end{align}
as shown in Figure~\ref{fig:convex}.
Next, we construct the convex hull of $C_{\F+\U}=C_\F\cup C_\U$.
It has five facets of which we only care about two, those spanned by
\begin{align}
F_1 =\begin{pmatrix}\rn1\\\rn2\\\rn3\end{pmatrix}
    =\begin{pmatrix}0&0&1\\0&1&1\\1&0&2\end{pmatrix}
\quad\text{and}\quad
F_2 =\begin{pmatrix}\rn1\\\rn2\\\rn3\\\rn4\end{pmatrix}
    =\begin{pmatrix}0&0&1\\0&1&0\\0&1&1\\0&2&0\end{pmatrix}
\,.
\end{align}
We can find the normal vectors $\vn1$ and $\vn2$ by finding the kernel of these matrices as
\begin{align}
\vn1 = \big(1,0,-1\big)
\quad\text{and}\quad
\vn2 = \big(1,0,0\big)
\end{align}
Here, $\vn2$ corresponds to the hard region and $\vn1$ to the collinear region.
We do not, for example, care about this facet
\begin{align}
F_3 =\begin{pmatrix}0&1&1\\1&0&2\\0&2&0\end{pmatrix}
\end{align}
which has $\vn3=(0,-1,-1)$.
\end{example}

\newenvironment{tikzddd}[3]{
    \begin{tikzpicture}[
        3d view={#1}{#2},
        scale=#3,
        hull/.style = {draw=black, fill=##1, fill opacity=0.1, draw opacity=1},
        plane/.style = {
            draw=none,
            pattern={Lines[
                distance=2mm, angle=25, line
                width=0.1mm
            ]}, pattern color=black!20
        },
        Fp/.style = {fill=red, circle, inner sep=1.5},
        Up/.style = {fill=black, circle, inner sep=1.5},
        reg/.style = {arrows={-Latex[length=1mm,width=2mm]}}
    ]
}{
    \end{tikzpicture}
}

\newcommand\axes[2]{
    \draw[->] (0,0,0) -- (#1,0,0) node[right] {$r_1$};
    \draw[->] (0,0,0) -- (0,#1,0) node[right] {$r_2$};
    \draw[->] (0,0,0) -- (0,0,#2) node[right] {$r_0$};
}

\begin{figure}
    \centering
    \begin{tikzddd}{100}{25}{3}
        \axes{2.5}{1.5}

        \coordinate (U1) at (1,0,0);
        \coordinate (U2) at (0,1,0);
        \coordinate (F1) at (2,0,0);
        \coordinate (F2) at (1,1,0);
        \coordinate (F3) at (0,2,1);

        \coordinate (A1) at ($(U1)!0.5!(U2)$);
        \coordinate (A2) at ($(F1)!0.5!(F2)$);
        \coordinate (B1) at ($(A1)!0.5!(A2)$);

        \coordinate (A3) at ($(U2)!0.5!(F2)$);
        \coordinate (B2) at ($(A3)!0.2!(F3)$);

        \draw[hull=blue] (U2) -- (F3) -- (U1) -- cycle;
        \draw[hull=blue] (F1) -- (F3) -- (U1) -- cycle;
        \draw[hull=blue] (F2) -- (U2) -- (F3) -- cycle;
        \draw[hull=blue] (F2) -- (F1) -- (F3) -- cycle;
        \draw[hull=blue] (F2) -- (F1) -- (U1) -- (U2) -- cycle;

        \node [Up] at (U1){};
        \node [Up] at (U2){};
        \node [Fp] at (F1){};
        \node [Fp] at (F2){};
        \node [Fp] at (F3){};

        \draw[reg] (B1) --+ (0,0,1) node[left] {$\vn1$};
        \draw[reg] (B2) --+ (0,-0.707,0.707) node[above right] {$\vn2$};

        \draw[plane] (2,0,0) -- (0,2,0) -- (0,2,2) -- (2,0,2) -- cycle;
    \end{tikzddd}

    \caption{
        The point clouds $C_\F$ in red and $C_\U$ in black and their convex hull in blue.
        The grey shaded region corresponds to the requirement that $\F$ is homogeneous, i.e. $r_1+r_2=\ell+1$.
        An interactive version of this plot can be found on the \href{https://yannickulrich.gitlab.io/loop-integration/parameter.html}{course website}.
    }
    \label{fig:convex}
\end{figure}

\begin{lemma}[Lee-Pomeransky vs. Feynman representation]
    The regions obtained from the Lee-Pomeransky representation $C_{\F+\U}$ are identical to those of the Feynman representation $C_{\U\F}$ since their lower facets have a one-to-one correspondence~\cite{Semenova:2018cwy}.
    Using this, we can now consider $n=3$ integrals and still visualise the result.
    We do this by first projecting out the $\lambda$ dimension in $C_{\F+\U}$ and then constructing $C_{\U\F}$.
    Because $C_{\U\F}$ lies in a single hyperplane, we can reintroduce the $\lambda$ direction as orthogonal to that plane.
\end{lemma}
\begin{proof}
    We can construct a surface $C_{\U\F}'$ by finding the intersection between all vertices in $C_\U$ and $C_\F$.
    This surface now corresponds to $C_{\U\F}$ up to a rescaling.
    There is a statement from alebraic geometry called Cayley's trick states that the convex hulls of $C_{\U\F}'$ and $C_{\U+\F}$ are identical.
\end{proof}

\begin{lemma}[Connecting parameter and momentum regions]
    For a given momentum region, we find the (inverse) region vector $-\vec v$ by considering how each propagator scales.
    Consider for example the collinear region of Example~\ref{ex:lightmor}
    \begin{subequations}
        \begin{align}
            \prop1 &= (k+p)^2 - M^2 = \lambda^0 \Big[2k_-\cdot p_+\Big] + \mathcal{O}(\lambda)\,,\\
            \prop2 &= (k-q)^2 - m^2 = \lambda^2 \Big[ k^2-2k\cdot q\Big]\,.
        \end{align}
    \end{subequations}
    This region has $\vn c = (0,-2)$.
\end{lemma}
\begin{proof}
    Consider the denominator after Feynman parametrisation~\eqref{eq:denpara}
    \begin{align}
        D = \prop1 x_1 + ... + \prop t x_t
    \end{align}
    The expansion is only complete if there is no scale seperation between the different terms.
    This means that if $\prop i$ scales as $\lambda^{-v_i}$, $x_i$ needs to scale as $\lambda^{v_i}$.
\end{proof}

\begin{example}[Massless Sudakov form factor~\cite{Gardi:2022khw}]
    \label{ex:sudakov0}
    Consider the following integral
    \begin{align}
        I = \begin{gathered}
                \begin{tikzpicture}
                    \draw[thick] (0,0) -- (180:0.5);
                    \draw[dashed] (0,0) -- (+45:1) -- (-45:1) -- (0,0);
                    \draw (45:1) --+ (0:0.5);
                    \draw (-45:1) --+ (0:0.5);
                \end{tikzpicture}
        \end{gathered}
        = \int\frac{[\D k]}{k^2\ (k-p)^2\ (k-q)^2}\,,
    \end{align}
    with $p^2=q^2=m^2 \ll (p+q)^2 = s$.
    We can easily construct
    \begin{align}
        \U=x_1+x_2+x_3\qquad\text{and}\qquad\F=s x_1 x_2 - m^2\lambda (x_1x_3 + x_2x_3)
    \end{align}
    to find the point clouds $C_\U$ and $C_\F$.
    The projection along the $\lambda$ direction is shown in Figure~\ref{fig:sudakov:nolam} with the point cloud for $C_{\U\F}$ drawn in between.
    The resulting hexagon for $C_{\U\F}$ can now be rotated and again stretched out in the $\lambda$ direction as shown in Figure~\ref{fig:sudakov:lam}.
    This allows us now to identify the upward pointing facets and identify four regions
    \begin{align}
        \begin{pmatrix}
            \vn1\\\vn2\\\vn3\\\vn4
        \end{pmatrix} = \begin{pmatrix}
             0& 0& 0\\
            -2& 0&-2\\
            -2&-2& 0\\
            -4&-2&-2
        \end{pmatrix}
        \equiv
        \begin{pmatrix}
            \text{hard}\\
            \text{collinear to }q\\
            \text{collinear to }p\\
            \text{soft}
        \end{pmatrix}
        \,.
    \end{align}
\end{example}


\begin{figure}
    \centering
    \subfloat[$C_\U$, $C_\F$ and $C_{\U\F}$ in grey]{
        \label{fig:sudakov:nolam}
        \begin{tikzddd}{100}{25}{3}
            \axes{1.5}{1.5}

            \coordinate (U1) at (1,0,0);
            \coordinate (U2) at (0,1,0);
            \coordinate (U3) at (0,0,1);
            \coordinate (F1) at (1,1,0);
            \coordinate (F2) at (1,0,1);
            \coordinate (F3) at (0,1,1);
            \coordinate (C1) at (1,0.5,0);
            \coordinate (C2) at (1,0,0.5);
            \coordinate (C3) at (0.5,0,1);
            \coordinate (C4) at (0,0.5,1);
            \coordinate (C5) at (0,1,0.5);
            \coordinate (C6) at (0.5,1,0);
            \coordinate (C7) at (0.5,0.5,0.5);

            \draw[hull=blue] (U1) -- (U2) -- (U3) -- cycle;
            \draw[hull=blue] (F1) -- (F2) -- (F3) -- cycle;
            \draw[hull=gray] (C1) -- (C2) -- (C3) -- (C4) -- (C5) -- (C6) -- cycle;

            \node [Up] at (U1){};
            \node [Up] at (U2){};
            \node [Up] at (U3){};
            \node [Up] at (F1){};
            \node [Fp] at (F2){};
            \node [Fp] at (F3){};

            \node [Up] at (C1){};
            \node [Fp] at (C2){};
            \node [Fp] at (C3){};
            \node [Fp] at (C4){};
            \node [Fp] at (C5){};
            \node [Up] at (C6){};
            \node [Fp] at (C7){};
            \node [Fp] at (C7){};
            \node [Up] at (C7){};
        \end{tikzddd}
    }\quad
    \subfloat[$C_{\U\F}$ in the rotated frame with the convex hull]{
        \label{fig:sudakov:lam}
        \begin{tikzddd}{-10}{25}{3.2}
            \def\sqrtt{1.73205}
            \def\A{1.2}
            \def\B{0.3}
            \coordinate (C1) at (0,-1,\B);
            \coordinate (C2) at (\sqrtt/2,-1/2,\A);
            \coordinate (C3) at (\sqrtt/2,1/2,\A);
            \coordinate (C4) at (0,1,\A);
            \coordinate (C5) at (-\sqrtt/2,1/2,\A);
            \coordinate (C6) at (-\sqrtt/2,-1/2,\B);
            \coordinate (C7) at (0,0,\A);
            \coordinate (C8) at (0,0,\B);

            \coordinate (Cp1) at (0,-1,0);
            \coordinate (Cp2) at (\sqrtt/2,-1/2,0);
            \coordinate (Cp3) at (\sqrtt/2, 1/2,0);
            \coordinate (Cp4) at (0,1,0);
            \coordinate (Cp5) at (-\sqrtt/2, 1/2,0);
            \coordinate (Cp6) at (-\sqrtt/2,-1/2,0);
            \coordinate (Cp7) at (0,0,0);

            \coordinate (Coll1) at ($(C6)!0.5!(C4)$);
            \coordinate (Coll2) at ($(C8)!0.5!(C2)$);
            \coordinate (B1) at ($(C3)!0.5!(C4)$);
            \coordinate (B2) at ($(C8)!0.5!(C6)$);
            \coordinate (Soft) at ($(B1)!0.33!(C8)$);
            \coordinate (Hard) at ($(B2)!0.33!(C1)$);

            \draw[dashed] (C1) -- (Cp1);
            \draw[dashed] (C2) -- (Cp2);
            \draw[dashed] (C3) -- (Cp3);
            \draw[dashed] (C4) -- (Cp4);
            \draw[dashed] (C5) -- (Cp5);
            \draw[dashed] (C6) -- (Cp6);
            \draw[dashed] (C7) -- (C8) -- (Cp7);


            \node[Up] at (C1){};
            \node[Fp] at (C2){};
            \node[Fp] at (C3){};
            \node[Fp] at (C4){};
            \node[Fp] at (C5){};
            \node[Up] at (C6){};
            \node[Fp] at (C7){};
            \node[Up] at (C8){};

            \draw[hull=gray] (Cp1) -- (Cp2) -- (Cp3) -- (Cp4) -- (Cp5) -- (Cp6) -- cycle;

            \draw [hull=blue] (C8) -- (C6) -- (C1) -- cycle; % lower
            \draw (C2) -- (C3) -- (C4) -- (C5) -- (C7) -- cycle; % upper
            \draw (C2) -- (C1) -- (C6) -- (C5) -- cycle; % stern
            \draw [hull=red] (C8) -- (C3) -- (C4) -- (C8) -- cycle; % bow
            \draw [hull=yellow](C8) -- (C3) -- (C2) -- (C1) -- cycle; % right
            \draw [hull=green] (C6) -- (C5) -- (C4) -- (C8) -- cycle; % left

            \draw[reg,thick] (Hard) --+ (0,0,0.3);
            \draw[reg,thick] (Coll1) --+ (\B/16+\A/16,\sqrtt/16*\B-\sqrtt/16*\A,\sqrtt/16);
            \draw[reg,thick] (Coll2) --+ (-\A/8+\B/8,0,\sqrtt/16);
            \draw[reg,thick] (Soft) --+ (\B/6-\A/6,\B/2/\sqrtt-\A/2/\sqrtt,0.5/\sqrtt);

        \end{tikzddd}
    }\\
    \subfloat[Regions identified on $C_{\U\F}$ with non-affine facets highlighted in red]{
        \label{fig:sudakov:nolamsing}
        \begin{tikzddd}{100}{25}{3}
            \axes{1.5}{1.5}

            \coordinate (U1) at (1,0,0);
            \coordinate (U2) at (0,1,0);
            \coordinate (U3) at (0,0,1);
            \coordinate (F1) at (1,1,0);
            \coordinate (F2) at (1,0,1);
            \coordinate (F3) at (0,1,1);
            \coordinate (C1) at (1,0.5,0);
            \coordinate (C2) at (1,0,0.5);
            \coordinate (C3) at (0.5,0,1);
            \coordinate (C4) at (0,0.5,1);
            \coordinate (C5) at (0,1,0.5);
            \coordinate (C6) at (0.5,1,0);
            \coordinate (C7) at (0.5,0.5,0.5);

            \draw[hull=green] (C1) -- (C2) -- (C3) -- (C7) -- cycle;
            \draw[hull=yellow] (C4) -- (C5) -- (C6) -- (C7) -- cycle;
            \draw[hull=red] (C4) -- (C7) -- (C3) -- cycle;
            \draw[hull=blue] (C1) -- (C7) -- (C6) -- cycle;

            \draw[thick,red] (C2) -- (C3);
            \draw[thick,red] (C4) -- (C5);
        \end{tikzddd}
    }
    \caption{
        The geometric construction of the massless Sudakov form factor.
        For explanation, see Example~\ref{ex:sudakov0}
    }
\end{figure}

\begin{example}[Massive Sudakov form factor]
    \label{ex:sudakovm}
    Consider a slightly modified integral
    \begin{align}
        I' = \begin{gathered}
                \begin{tikzpicture}
                    \draw[thick] (0,0) -- (180:0.5);
                    \draw (0,0) -- (+45:1) -- (-45:1) -- (0,0);
                    \draw (45:1) --+ (0:0.5);
                    \draw (-45:1) --+ (0:0.5);
                \end{tikzpicture}
        \end{gathered}
        = \int\frac{[\D k]}{(k^2-m^2)\ ((k-p)^2-m^2)\ ((k-q)^2-m^2)}\,,
    \end{align}
    with $p^2=q^2=m^2 \ll (p+q)^2 = s$.
    We now get $\U'=\U$ and $\F'=\F + \U(x_1+x_2+x_3)m^2\lambda^2$.
    Constructing again the convex hull for $C_{\U\F}$ we note that it now has one face less.
    This also translates to a missing soft region
    \begin{align}
        \begin{pmatrix}
            \vn1\\\vn2\\\vn3
        \end{pmatrix} = \begin{pmatrix}
             0& 0& 0\\
            -2& 0&-2\\
            -2&-2& 0
        \end{pmatrix}
        \equiv
        \begin{pmatrix}
            \text{hard}\\
            \text{collinear to }q\\
            \text{collinear to }p
        \end{pmatrix}
        \,.
    \end{align}
\end{example}
\begin{figure}
    \centering
    \subfloat[$C_\U$, $C_\F$ and $C_{\U\F}$ in grey]{
        \label{fig:massivesudakov:nolam}
        \begin{tikzddd}{100}{25}{2.2}
            \axes{2.2}{2.2}

            \coordinate (U1) at (1,0,0);
            \coordinate (U2) at (0,1,0);
            \coordinate (U3) at (0,0,1);
            \coordinate (F1) at (1,0,1);
            \coordinate (F2) at (2,0,0);
            \coordinate (F3) at (1,1,0);
            \coordinate (F4) at (0,2,0);
            \coordinate (F5) at (0,1,1);
            \coordinate (F6) at (0,0,2);
            \coordinate (C1) at (1,0,0.5);
            \coordinate (C2) at (1.5,0,0);
            \coordinate (C3) at (1,0.5,0);
            \coordinate (C4) at (0.5,1,0);
            \coordinate (C5) at (0.5,0.5,0.5);
            \coordinate (C6) at (0.5,0,1);
            \coordinate (C7) at (0,1.5,0);
            \coordinate (C8) at (0,1,0.5);
            \coordinate (C9) at (0,0.5,1);
            \coordinate (C10) at (0,0,1.5);

            \draw[hull=blue] (U1) -- (U2) -- (U3) -- cycle;
            \draw[hull=blue] (F1) -- (F2) -- (F3) -- (F4) -- (F5) -- (F6) -- cycle;
            \draw[hull=gray] (C6) -- (C1) -- (C2) -- (C3) -- (C3) -- (C4) -- (C4) -- (C7) -- (C8) -- (C9) -- (C10) -- cycle;

            \node [Up] at (U1){};
            \node [Up] at (U2){};
            \node [Up] at (U3){};
            \node [Fp] at (F1){};
            \node [Fp] at (F2){};
            \node [Fp] at (F3){};
            \node [Up] at (F3){};
            \node [Fp] at (F4){};
            \node [Fp] at (F5){};
            \node [Fp] at (F6){};

            \node [Fp] at (C1){};
            \node [Fp] at (C2){};
            \node [Fp] at (C3){};
            \node [Up] at (C3){};
            \node [Fp] at (C4){};
            \node [Fp] at (C5){};
            \node [Fp] at (C6){};
            \node [Up] at (C4){};
            \node [Fp] at (C7){};
            \node [Fp] at (C8){};
            \node [Fp] at (C9){};
            \node [Up] at (C5){};
            \node [Fp] at (C10){};
        \end{tikzddd}
    }\quad
    \subfloat[$C_{\U\F}$ in the rotated frame with the convex hull]{
        \label{fig:massivesudakov:lam}
        \begin{tikzddd}{-20}{15}{2.2}
            \def\sqrtt{1.73205}
            \def\A{2.2}
            \def\B{0.3}
            \coordinate (C1) at (\sqrtt/2,-0.5,\A);
            \coordinate (C2) at (\sqrtt/2,-1.5,\A);
            \coordinate (C3) at (0,-1,\A);
            \coordinate (C4) at (0,-1,\B);
            \coordinate (C5) at (-\sqrtt/2,-0.5,\A);
            \coordinate (C6) at (0,0,\A);
            \coordinate (C7) at (\sqrtt/2,0.5,\A);
            \coordinate (C8) at (-\sqrtt/2,-0.5,\B);
            \coordinate (C9) at (-\sqrtt,0,\A);
            \coordinate (C10) at (-\sqrtt/2,0.5,\A);
            \coordinate (C11) at (0,1,\A);
            \coordinate (C12) at (0,0,\B);
            \coordinate (C13) at (\sqrtt/2,1.5,\A);

            \coordinate (Cp1) at (\sqrtt/2,-0.5,0);
            \coordinate (Cp2) at (\sqrtt/2,-1.5,0);
            \coordinate (Cp3) at (0,-1,0);
            \coordinate (Cp4) at (-\sqrtt/2,-0.5,0);
            \coordinate (Cp5) at (0,0,0);
            \coordinate (Cp6) at (\sqrtt/2,0.5,0);
            \coordinate (Cp7) at (-\sqrtt,0,0);
            \coordinate (Cp8) at (-\sqrtt/2,0.5,0);
            \coordinate (Cp9) at (0,1,0);
            \coordinate (Cp10) at (\sqrtt/2,1.5,0);

            \coordinate (Bp1) at ($(C13)!0.5!(C12)$);
            \coordinate (Bp2) at ($(C4)!0.5!(C2)$);
            \coordinate (Bp3) at ($(C12)!0.5!(C8)$);
            \coordinate (Bp4) at ($(C9)!0.5!(C13)$);
            \coordinate (Bp5) at ($(C8)!0.5!(C4)$);
            \coordinate (Coll1) at ($(Bp1)!0.5!(Bp2)$);
            \coordinate (Coll2) at ($(Bp3)!0.5!(Bp4)$);
            \coordinate (Hard) at ($(Bp5)!0.33!(C12)$);

            \draw [hull=blue] (C12) -- (C8) -- (C4) -- cycle; % lower
            \draw (C13) -- (C9) -- (C2) -- cycle; % upper
            \draw (C9) -- (C8) -- (C4) -- (C2) -- cycle; % stern
            \draw [hull=yellow] (C13) -- (C12) -- (C4) -- (C2) -- cycle; % right
            \draw [hull=green] (C12) -- (C8) -- (C9) -- (C13) -- cycle; % left

            \draw[hull=gray] (Cp6) -- (Cp1) -- (Cp2) -- (Cp3) -- (Cp4) -- (Cp7) -- (Cp8) -- (Cp9) -- (Cp10) -- cycle;

            \draw[dashed] (Cp10) -- (C13);
            \draw[dashed] (Cp2) -- (C2);
            \draw[dashed] (Cp7) -- (C9);

            \node [Fp] at (C1){};
            \node [Fp] at (C2){};
            \node [Fp] at (C3){};
            \node [Up] at (C4){};
            \node [Fp] at (C5){};
            \node [Fp] at (C6){};
            \node [Fp] at (C7){};
            \node [Up] at (C8){};
            \node [Fp] at (C9){};
            \node [Fp] at (C10){};
            \node [Fp] at (C11){};
            \node [Up] at (C12){};
            \node [Fp] at (C13){};

            \draw[reg,thick] (Coll2) --+ (\A/8 - \B/8, -\sqrtt*\A/8+\sqrtt*\B/8, \sqrtt/8);
            \draw[reg,thick] (Coll1) --+ (-2*\A/8+2*\B/8,0,\sqrtt/8);
            \draw[reg,thick] (Hard) --+ (0,0,0.3);
        \end{tikzddd}
    }\\
    \subfloat[Regions identified on $C_{\U\F}$ with non-affine facets highlighted in red]{
        \label{fig:massivesudakov:nolamsing}
        \begin{tikzddd}{100}{25}{2.2}
            \axes{2.2}{2.2}

            \coordinate (C1) at (1,0,0.5);
            \coordinate (C2) at (1.5,0,0);
            \coordinate (C3) at (1,0.5,0);
            \coordinate (C4) at (0.5,1,0);
            \coordinate (C5) at (0.5,0.5,0.5);
            \coordinate (C6) at (0.5,0,1);
            \coordinate (C7) at (0,1.5,0);
            \coordinate (C8) at (0,1,0.5);
            \coordinate (C9) at (0,0.5,1);
            \coordinate (C10) at (0,0,1.5);

            \draw [hull=green] (C6) -- (C1) -- (C2) -- (C3) -- (C5) -- (C10) -- cycle;
            \draw [hull=yellow] (C5) -- (C4) -- (C7) -- (C8) -- (C9) -- (C10) -- cycle;
            \draw [hull=blue] (C5) -- (C4) -- (C3) -- cycle;

            \draw [thick,red] (C2) -- (C10);
            \draw [thick,red] (C7) -- (C10);
            \draw [thick,red] (C10) -- (C5);
        \end{tikzddd}
    }
    \caption{
        The geometric construction of the massive Sudakov form factor.
        For explanation, see Example~\ref{ex:sudakovm}
    }
\end{figure}

\begin{lemma}[Singularities and the convex hull]
    Singularities of an integral arise from facets of the convex hull that lie on a plane that also contains $\vec 0$~\cite{Heinrich:2021dbf}.
    We can now distinguish two cases
    \begin{itemize}
        \item
            \emph{outer facets} that touch at most one region are regulated dimensionally and exist independent of the method of regions,

        \item
            \emph{inner facets} that touch two regions are not regulated dimensionally and require an analytic regulator, i.e. a shift of the propagator powers.
    \end{itemize}
    These are highlighted in red Figures~\ref{fig:sudakov:nolamsing} and \ref{fig:massivesudakov:nolamsing}.
    It is clear that both have two outer facets but the latter also has in inner facet which means the regions are not regulated dimensionally.
\end{lemma}
\begin{proof}
    In this proof, we follow~\cite{Heinrich:2021dbf,Schlenk:2016epj}.
    In the Lee-Pomeransky representation, the most general case we need to consider is
    \begin{align}
        I = \int
            \Big(\prod_{i=1}^t \D x_i\ x_i^{\alpha_i-1}\Big)
            \Big(\sum_k c_k \vec x^{\ \vec r_k}\Big)^{-d/2}
            \,.
    \end{align}
    One can show that the integration region $\mathbb{R}^t$ can be decomposed into into simplicial cones defined by rays $\vec n_F$
    \begin{align}
        \sigma = \Big\{ \sum_{F\in \sigma} a_F\vec n_F\ :\ a_F > 0\Big\}\,.
    \end{align}
    The rays $\vec n_F$ are the normal vectors of a facet $F$ of the convex hulls discussed above.
    The facets themselves can be described in Hessian normal form by a normal vector $\vec n$ and an offset $a$ as
    \begin{align}
        F = \{\vec y\ :\ \vec n_F\cdot \vec y + a_F=0\}\,.
    \end{align}
    By considering all $t$-dimensional cones $\sigma$ we can reconstruct the full integration region $\mathbb{R}^t$.
    Transforming from the cartesian coordinates $\{\vec e_i\}$ to the local coordinates of the facets we have
    \begin{align}
        x_i = \prod_{F\in\sigma}y^{\vec n_F\cdot e_i}\,,
    \end{align}
    for each cone.
    We now have
    \begin{align}
        I = \sum_\sigma
            |\sigma|
            \prod_{F\in\sigma}
            \int
            \D y_F\ y_F^{\vec n_F\cdot \vec\alpha-1}
            \bigg(\sum_k
                c_k
                \prod_{F\in\sigma}
                y_F^{\vec n_F\cdot \vec r_k}
            \bigg)^{-d/2}
            \,,
    \end{align}
    with the Jacobian $|\sigma|$.
    To guarantee that the complicated monomial has positive exponents, we factor our $y_F^{-a_F}$.
    \begin{align}
        I = \sum_\sigma
            |\sigma|
            \prod_{F\in\sigma}
            \int
            \frac{\D y_F}{y_F}y_F^{\vec n_F\cdot \vec\alpha+\tfrac d2 a_F}
            \underbrace{\bigg(\sum_k
                c_k
                \prod_{F\in\sigma}
                y_F^{\vec n_F\cdot \vec r_k+a_F}
            \bigg)^{-d/2}}_{g(y)}
            \,.
    \end{align}
    Since the $\vec r_k\in F$, the $g$ part is now free of singularities.
    Hence, for $I$ to be finite, we require
    \begin{align}
        \vec n_F\cdot \vec\alpha+\frac d2 a_F > 0\,.
    \end{align}
    If $a_F \neq 0$, the integral is regulated dimensionally.
    The analytic regulator now sets some $\alpha_i\to \alpha_i \pm \eta$ with $\eta>0$ such that the new $\vec\alpha$ is no longer orthogonal to any $\vec n_F$.
\end{proof}
