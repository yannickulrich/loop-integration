\documentclass[a4paper]{article}
\usepackage[english]{babel}
\usepackage[left=2.5cm,top=3cm,bottom=3cm,right=2.5cm]{geometry}
\usepackage{authblk}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{listings}
\usepackage[caption=false]{subfig}
\usepackage[svgnames,x11names]{xcolor}
\usepackage[colorlinks=true
,urlcolor=blue
,anchorcolor=blue
,citecolor=blue
,filecolor=blue
,linkcolor=blue
,menucolor=blue
,linktocpage=true
,pdfproducer=medialab
,pdfa=true
]{hyperref}
\def\shuffle{
    \begin{tikzpicture}[x=0.7ex,y=1ex]
        \draw(-1,1) -- (-1,0) -- (1,0) -- (1,1);
        \draw(0,0) -- (0,1);
    \end{tikzpicture}
}
\usepackage{xstring}
\newcommand{\gitcommit}{
    \newread\headfile%
    \openin\headfile=../.git/refs/heads/root%
    \read\headfile to\headline%
    \closein\headfile%
    \StrGobbleRight{\headline}{34}%
}

\definecolor{commentsColor}{rgb}{0.497495, 0.497587, 0.497464}
\definecolor{keywordsColor}{rgb}{0.000000, 0.000000, 0.635294}
\definecolor{stringColor}{rgb}{0.558215, 0.000000, 0.135316}

\lstset{ %
  basicstyle=\ttfamily,
  keywordstyle=\color{Aquamarine4},
  numberstyle=\color{DarkOrange},
  stringstyle=\color{SkyBlue4},
  commentstyle=\color{Blue2},
  morecomment=[l]{!\ },% Comment only with space after !
  escapeinside={(*@}{@*)},
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}
}

\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta}
\usetikzlibrary{perspective, patterns.meta}
\usetikzlibrary{calc,3d}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }

\tikzset{
    block/.style = {
        draw,
        fill=blue!20,
        text width=5em,
        minimum height=4em,
        text centered,
        rounded corners,
        % minimum width=6em,
    },
    arrow/.style = {
        -{>[length=2mm, width=3mm]},
    },
    logic/.style = {
        draw,
        diamond,
        fill=green!20,
        text width=5em,
        text centered,
    },
    data/.style = {
        draw,
        ellipse,
        fill=red!20,
        minimum height=4em,
        minimum width=5em,
        text centered,
    },
}

\newmuskip\pFqmuskip

\newcommand*\pFq[6][8]{%
  \begingroup % only local assignments
  \pFqmuskip=#1mu\relax
  \mathchardef\normalcomma=\mathcode`,
  % make the comma maths active
  \mathcode`\,=\string"8000
  % and define it to be \pFqcomma
  \begingroup\lccode`\~=`\,
  \lowercase{\endgroup\let~}\pFqcomma
  % typeset the formula
  {}_{#2}F_{#3}{\left[\genfrac..{0pt}{}{#4}{#5};#6\right]}%
  \endgroup
}
\newcommand{\pFqcomma}{{\normalcomma}\mskip\pFqmuskip}

\DeclareMathOperator*{\res}{res}
\newcommand{\Li}{\mathrm{Li}}
\newcommand{\D}{\mathrm{d}}
\newcommand{\I}{\mathrm{i}}
\def\io{{\rm i0}^+}
\def\im{{\rm i0}^-}
\newcommand{\prop}[1]{\mathcal{P}_{#1}}
\newcommand{\mbinom}[2]{\bigg(\!\!\bigg(\genfrac{}{}{0pt}{0}{#1}{#2}\bigg)\!\!\bigg)}

\theoremstyle{definition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{example}[theorem]{Example}

\makeatletter
\newenvironment{proofof}[1]{\par
  \pushQED{\qed}%
  \normalfont \topsep6\p@\@plus6\p@\relax
  \trivlist
  \item[\hskip\labelsep
        \it
         Proof of #1\@addpunct{.}]\ignorespaces
}{%
  \popQED\endtrivlist\@endpefalse
}
\makeatother

\title{Practical guide to analytic loop integration}
\author{Yannick Ulrich}
\affil{
Albert Einstein Center for Fundamental Physics, Institute for Theoretical Physics, University of Bern\\
Bern, Sidlerstrasse 5, CH-3012 Bern, Switzerland
}
\renewcommand\Affilfont{\itshape\small}

\date{version \gitcommit}

\begin{document}
\maketitle

\begin{abstract}
Loop integration is a vital part of any higher order calculation.
However, practical tools to actually compute these integrals are rarely covered in lectures.
In this course, I will cover some advanced tools that have been used in a number of multi-scale multi-loop calculations.

After introducing the problem, I will discuss integration-by-parts reduction to reduce the number of integrals.
Next, I will discuss the method of regions to reduce the number of scales of the integrals.
Finally, I will discuss the method of differential equations and in particular Auxilary Mass Flow to actually calculate the integrals.

The course will be composed of lectures introducing the techniques and practical, hands-on example at the two-loop level.

For further reading, see~\cite{Weinzierl:2022eaz,Smirnov:2012gma,Smirnov:2006ry,Urwyler:2019bsc,Engel:2018msc,Borowka:2014aaa}.

\end{abstract}


\tableofcontents
\vfill
\begin{minipage}{22em}
This work is licensed under a Creative Commons Attribution 4.0 International License.
\end{minipage}
\hspace{1em}
\begin{minipage}{6.5em}
\includegraphics{cc-by-4.0}
\end{minipage}
\clearpage

\input{basics}
\input{ibp}
\input{deq}
\input{mor}
\input{mb}
\input{deq-series}


\bibliographystyle{JHEP}
\bibliography{notes}{}

\end{document}
