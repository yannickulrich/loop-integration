%!TEX root=notes
%PDFLATEX

\section{Method of differential equations}

We have now a reduced our problem to a set of master integrals that we need to compute.
While it is at least in principle possible to calculate one integral at a time using the representation in \eqref{eq:masterform}, this is rarely a good idea since we often have many hundreds of integrals and the required integrals cannot be solved analytically.
Instead, we would prefer a method that can solve all master integrals in one stroke.
The method of differential equations is the way to do this.

\subsection{Deriving differential equations}

\begin{observation}
    Feynman integrals are functions of the dimensional regulator $\epsilon$ and masses and invariants collectively called $s_i$.
    Since derivatives w.r.t. the $s_i$ can be related to derivates w.r.t. the momenta, it is easy to see the derivative of a Feynman integral is another Feynman integral in the same family.
    By IBP-reducing these back to the same set of master integrals, we can write a closed-form linear system of differential equations for the master integrals.
\end{observation}

\begin{theorem}[Derivatives of Feynman integrals]

    The derivate of a set of Feynman integrals $\vec I$ w.r.t. some kinematic parameter $s$ can be written as
    \begin{align}
        \frac{\partial \vec I}{\partial s_i} = M_i(\{s\}, \epsilon) \vec I
    \end{align}
    where $M_i$ is a matrix that depends on the kinematics and $\epsilon$.
    The derivate $\partial_{s_i}$ can be written in terms of the momenta
    \begin{align}
        \frac{\partial}{\partial s_i} = \sum_{jk} a_{i,jk}\ p_k\cdot\frac{\partial}{\partial p_j}\,,
    \end{align}
    with coefficients $a_{i,jk}$ that are determined by applying this operator to the invariants themselves.

\end{theorem}

\begin{example}[Heavy quark form factor]

    Consider the family in \ref{obs:UFgraph}
    \begin{align}
        I_{\alpha\beta\gamma}
            = \int[\D k_1]
                \frac1{\big[k_1^2\big]^\alpha}             % x_1
                \frac1{\big[(k_1+p)^2-m^2\big]^\beta }     % x_2
                \frac1{\big[(k_1-q)^2-m^2\big]^\gamma}     % x_3
    \end{align}
    with $p^2=q^2=m^2$ and $(p+q)^2=s$.
    The derivative w.r.t. to $s$ can be written as
    \begin{align}
        \frac{\partial}{\partial s} = \Big(a_{s,11}\ p + a_{s,21}\ q\Big)\cdot\frac{\partial}{\partial p} + \Big(a_{s,12}\ p+a_{s,22}\ q\Big)\cdot\frac{\partial}{\partial q}\,.
    \end{align}
    By having this act on $p^2$, $q^2$, and $p\cdot q$, we find
    \begin{align}\begin{split}
        \frac{\partial(p^2     )}{\partial s} &= \Big(a_{s,11}\ p + a_{s,21}\ q\Big)\cdot\underbrace{\frac{\partial(p^2     )}{\partial p}}_{2p} + \Big(a_{s,12}\ p+a_{s,22}\ q\Big)\cdot\underbrace{\frac{\partial(p^2     )}{\partial q}}_{ 0}\,,\\
        \frac{\partial(q^2     )}{\partial s} &= \Big(a_{s,11}\ p + a_{s,21}\ q\Big)\cdot\underbrace{\frac{\partial(q^2     )}{\partial p}}_{ 0} + \Big(a_{s,12}\ p+a_{s,22}\ q\Big)\cdot\underbrace{\frac{\partial(q^2     )}{\partial q}}_{2q}\,,\\
        \frac{\partial(p\cdot q)}{\partial s} &= \Big(a_{s,11}\ p + a_{s,21}\ q\Big)\cdot\underbrace{\frac{\partial(p\cdot q)}{\partial p}}_{ q} + \Big(a_{s,12}\ p+a_{s,22}\ q\Big)\cdot\underbrace{\frac{\partial(p\cdot q)}{\partial q}}_{ p}\,.
    \end{split}\end{align}
    This can be simplified to
    \begin{align}\begin{split}
        0        &= a_{s,11}\ 2m^2 + a_{s,21}\ \big(s-2m^2\big)\,,\\
        0        &= a_{s,22}\ 2m^2 + a_{s,12}\ \big(s-2m^2\big)\,,\\
        \frac12  &= \big(a_{s,11}+a_{s,22}\big)\ \frac{s-2m^2}{2} + \big(a_{s,21}+a_{s,12}\big)\ m^2\,.
    \end{split}\end{align}
    When solving this under-determined linear system we have some choice.
    Here, we choose $a_{s,11}=0$ and arrive at
    \begin{align}
        % \frac{\partial}{\partial s} = a_{s,11}\ p\cdot\frac{\partial}{\partial p} + a_{s,21}\ q\cdot\frac{\partial}{\partial p} + a_{s,12}\ p\cdot\frac{\partial}{\partial q}+a_{s,22}\ q\cdot\frac{\partial}{\partial q}\,.
        \frac{\partial}{\partial s} = \frac{2m^2}{(4m^2-s)s}\ p\cdot\frac{\partial}{\partial q}+\frac{2m^2-s}{(4m^2-s)s}\ q\cdot\frac{\partial}{\partial q}\,.
    \end{align}
    A similar calculation with $a_{m^2,12}=a_{m^2,21}$, leads us to
    \begin{align}
        \frac{\partial}{\partial m^2} = \frac{p^\mu-q^\mu}{4m^2-s} \Big( \frac{\partial}{\partial p^\mu} - \frac{\partial}{\partial q^\mu} \Big)\,.
    \end{align}
    We can now trivially calculate the effect of our two operators on an integral $I_{\alpha\beta\gamma}$.
    In the notation of Definition \ref{def:seed}
    \begin{align}\begin{split}\label{eq:diffop}
        \partial_s &= \frac{\gamma}{4m^2-s}\bigg( \frac{2m^2}s {\bf 2}^- {\bf 3}^+ - {\bf 1}^- {\bf 3}^+ + \frac{s-2m^2}{s}\bigg)\,,\\
        \partial_{m^2} &= \frac{1}{4m^2-s}\bigg( 2\beta{\bf 1}^-{\bf 2}^+ - \beta {\bf 3}^-{\bf 2}^+ + 2\gamma {\bf 1}^-{\bf 3}^+ - \gamma{\bf 2}^-{\bf 3}^+ - \beta-\gamma\bigg)\,.
    \end{split}\end{align}
\end{example}

\begin{example}[Master integrals in the heavy quark form factor]
    We can now pick two integrals, eg. $I_{001}$ and $I_{011}$, apply~\eqref{eq:diffop}, perform the IBP reduction and arrive at our differential equations.
    For this it is useful to collect our two master integrals in a vector $\vec I=(I_{001}, I_{011})^T$
    \begin{align}\begin{split}
        \frac{\partial \vec I}{\partial s}   &= \frac1{(4m^2-s)s}\begin{pmatrix}0&0\\2-2\epsilon & -2m^2 + s\epsilon\end{pmatrix} \vec I\,, \\
        \frac{\partial \vec I}{\partial m^2} &= \frac1{(4m^2-s)m^2}\begin{pmatrix}(4m^2-s)(1-\epsilon)&0\\-2+2\epsilon & 2m^2(1-2\epsilon)\end{pmatrix} \vec I\,.
        \label{eq:dmat1}
    \end{split}\end{align}
\end{example}

\begin{theorem}[Integral scaling]
    When our integral depends on multiple scales $s_i$ (such as $s$ and $m^2$ in the above example), we have multiple matrices $M_i$.
    Note that
    \begin{align}
        \sum_i s_i M_i = {\rm diag}(\lambda_i)\,,
    \end{align}
    where the $\lambda_i = dl/2 - r_i + s_i$ are the scaling dimension of the $i$-th integral (cf. notation of Definition~\ref{def:reduze}).
    This is an extremely useful cross check that the matrices $M_i$ were correctly derived.

\end{theorem}

\begin{theorem}[Some useful identities]\label{th:idents}
    We often want to change variables and/or basis.
    \begin{itemize}
        \item
            We can change our basis of master integrals from $\vec I$ to $\vec I' = T^{-1} \vec I$ with some invertible matrix $T$.
            This means our $M_i$ change to
            \begin{align}
                M_i' = T^{-1}\cdot M_i\cdot T - T^{-1}\cdot\frac{\partial T}{\partial s_i}
            \end{align}

        \item
            To change from the variable $s_i \to s_i'$, we write
            \begin{align}
                M_{i'} = M_i \times \frac{\partial s_i}{\partial s_i'}\Bigg|_{s_i\to s_i'}
                \label{eq:subs}
            \end{align}

        \item
            It is often advisable to choose scaleless variables and rescale the integrals so that all $\lambda_i=1$.
    \end{itemize}
\end{theorem}

\subsection{Canonical basis and \texorpdfstring{$d\log$}{dlog} form}

\begin{example}[Change of variables]
    Let us choose $T={\rm diag}(m^{-2\epsilon}, m^{-2-2\epsilon})/\epsilon$.
    This way our master integrals become
    \begin{align}
        \vec J = \Big( m^{2\epsilon}\epsilon I_{001}, m^{2+2\epsilon}\epsilon I_{011} \Big)^T
    \end{align}
    and our matrices becomes
    \begin{align}\begin{split}
        M_s' &= \frac{1}{4m^2-s} \begin{pmatrix}0&0\\\frac{2m^2}s(1-\epsilon) & -\frac{2m^2}{s}+\epsilon \end{pmatrix}\,,\\
        M_{m^2}' &= \frac{1}{4m^2-s} \begin{pmatrix}\frac{4m^2-s}{m^2}&0\\ -2+2\epsilon & 6-\frac{s}{m^2}(1+\epsilon) \end{pmatrix}\,.
    \end{split}\end{align}
    One can easily see that $sM_s' + m^2 M_{m^2}' = 1$.
    Let us now introduce $y=s/(4m^2)$.
    The new $M_y$ is
    \begin{align}
        M_y = \frac1{1-y}\frac1{2y} \begin{pmatrix}0&0\\1&-1\end{pmatrix}
            +\frac1{1-y}\epsilon   \begin{pmatrix}0&0\\-\frac1{2y}&1\end{pmatrix}\,.
        \label{eq:hqff:my}
    \end{align}
\end{example}

\begin{definition}[Canonical form]
    If all matrices $M_i$ can be brought into the form $M_i = M_i^{(0)} + \epsilon M_i^{(1)}$ we call the basis \emph{precanonical}.
    A \emph{canonical basis} has further $M_i^{(0)} = 0$.
    In other words, the differential equation can be written as
    \begin{align}
        \frac{\partial \vec J}{\partial s_i} = \epsilon M_i \vec J\,.
    \end{align}
    Once the differential equations are in canonical form, solving them is fairly straightforward, at least in principle.
\end{definition}

\begin{definition}[$d\log$ form]
    In some cases it is further possible to simplify the equations in terms of differential forms
    \begin{align}\label{eq:dlog}
        \D\vec J = \epsilon \D A \vec J\,,
    \end{align}
    where $A$ can be written as
    \begin{align}
        A = \sum_i A_i \log\eta_i
    \end{align}
    with $A_i\in\mathbb{Q}$ matrices of rational numbers.
    This is referred to as the $d\log$ form and it usually (but not always~\cite{Duhr:2020gdd})\footnote{
        This can eg. happen if the $d\log$ basis has multiple non-simultaneously rationalisable square roots.
    } possible to write the answer in term of polylogarithms.
    The $\eta_i$ are called letters and their set $\{\eta_i\}$ the alphabet.
    In practise this means that the original $M$ can be written as
    \begin{align}
        M = \sum_i \frac{A_i}{\eta_i}\,.
    \end{align}

\end{definition}

\begin{example}
    There are algorithms that can assist in arriving at a canonical or precanonical form.
    However, the process often is a mixture of guesswork, experience, and computer codes.
    In our case, we can have
    \begin{align}
        \vec J = \Big( \epsilon\, m^{2\epsilon+2}\, I_{002}, \epsilon\, \sqrt s\sqrt{s-4m^2}\, m^{2\epsilon+2}\, I_{012} \Big)^T\,,
    \end{align}
    with the matrix
    \begin{align}
        M_s = \epsilon \begin{pmatrix}0&0\\ \frac1{\sqrt s\sqrt{s-4m^2}} & -\frac1{s-4m^2}\end{pmatrix}\,.
    \end{align}
    The presence of squared propagators and square roots in $\vec J$ is a fairly common feature.
    To remove these, we perform one more change of variables $s/(4m^2) = y = -x^2/(1-x^2)$ and arrive at
    \begin{align}
        M_x = \epsilon\begin{pmatrix} 0&0 \\ \frac1{1+x}+\frac1{1-x} & \frac1{1+x} - \frac1{1-x} \end{pmatrix}\,.
    \end{align}
    Now our matrix is in $d\log$ form
    \begin{align}
        \D\vec J = \epsilon\ \D\Big(A_1 \log(1+x) + A_2 \log(1-x)\Big) \vec J\\
        A_1 = \begin{pmatrix}0&0\\-1&1\end{pmatrix}\qquad
        A_2 = \begin{pmatrix}0&0\\ 1&1\end{pmatrix}
    \end{align}

\end{example}


\subsection{Chen-interated integrals}

Once our integral is in $d\log$ form, we are often done since our integral can now be solved in terms of Chen iterated integrals~\cite{Chen:1977oja}.
\begin{definition}[Chen iterated integral]
    The formal solution to~\eqref{eq:dlog} is a Chen iterated integral
    \begin{align}\label{eq:chen}
        \vec J(\vec s, \epsilon) = \mathbb{P}\exp\Bigg[\epsilon\int_\gamma\D A\Bigg] \vec J_0(\epsilon)\,.
    \end{align}
    The $\mathbb{P}$ indicates path-ordering along the integration contour $\gamma$.
    The $\vec J_0(\epsilon)$ is the boundary condition of our differential equation.
\end{definition}

\begin{example}[Boundary bonditions of the heavy quark form factor at $x=0$]
    Before we can solve the integral for arbitrary $x$, we need to solve it for a specific value of $x$ to act as our boundary condition.
    For this we pick $x=0$ which corresponds to $s=0$.
    Since the original integral $I_{012}$ is regular as $s\to 0$, our master integral is zero $I_{012}(s=0)=0$.
    The other integral $I_{002}$ is a single-scale integral that could not be computed using differential equations anyway.
    However, it is trivial to see from \eqref{eq:base} $I_{002}=m^{-2\epsilon}\Gamma(1-\epsilon)\Gamma(\epsilon)$.
    Hence,
    \begin{align}
        \vec J_0(\epsilon) = \Big(m^2\Gamma(1-\epsilon)\Gamma(1+\epsilon), 0\Big)^T\,.
    \end{align}
\end{example}

\begin{definition}[Generalised polylogarithms]
    The class of function that is obtained from iterated integrals of rational functions are called generalised or Goncharov polylogarithms (GPL)~\cite{Goncharov:1998kja}
    \begin{align}
        G(z_1, \cdots, z_m; y) =
            \int_0^y \frac{\D t_1}{t_1-z_1}
            \int_0^{t_1} \frac{\D t_2}{t_2-z_2}
            \cdots
            \int_0^{t_{m-1}} \frac{\D t_m}{t_m-z_m}\,.
    \end{align}
    These functions are extremely well studied and many tools exist to work with them~\cite{Vollinga:2004sn,Frellesvig:2016ske,Frellesvig:2018lmm,Naterop:2019xaf,Duhr:2019tlz}
\end{definition}
\begin{lemma}
    Some useful identities for GPLs are
    \begin{subequations}
        \begin{align}
            G(\underbrace{0,\cdots,0}_{n}; y) &= \frac1{n!}\log^n y\\
            G(\underbrace{a,\cdots,a}_{n}; y) &= \frac1{n!}\log^n\Big(1-\frac ya\Big)\\
            G(\underbrace{0,\cdots,0}_{n-1},a; y) &= -{\rm Li}_n\frac ya = -{\tt PolyLog[n, y/a]}\\
            G(\underbrace{0,\cdots,0}_{n-k},\underbrace{a,\cdots,a}_k; y) &= (-1)^kS_{n-k,k}\frac ya = (-1)^k {\tt PolyLog[n-k,k, y/a]}
        \end{align}
        We can relate GPLs to the simpler harmonic polylogarithms~\cite{Remiddi:1999ew,Maitre:2005uu} if $a_i=0,\pm1$
        \begin{align}
            H(a_1,\cdots,a_n;y) = (-1)^p G(a_1,\cdots,a_n;y)
        \end{align}
        where $p$ counts the number of $a_i=+1$.
        The literature often considers $y=1$ since
        \begin{align}
            G(z_1, \cdots, z_n; y) = G(\kappa z_1, \cdots, \kappa z_n; \kappa y)
        \end{align}
        for any $\kappa\neq0$.
        It is further possible to write the product of GPLs as a single GPL
        \begin{align}
            G(\vec a\ ;y) \cdot G(\vec b\ ;y)= \sum_{\vec c = \vec a\,\shuffle\,\vec b}G(\vec c\ ;y)\,,
        \end{align}
        with the shuffle product $\vec a\,\shuffle\,\vec b$.
        For example
        \begin{align*}
            \{a,b\}\shuffle\{c,d\} = \Big\{
                \{a,b,c,d\},
                \{a,c,b,d\},
                \{a,c,d,b\},
                \{c,a,b,d\},
                \{c,a,d,b\},
                \{c,d,a,b\}
            \Big\}\,.
        \end{align*}
        This last property is the reason we care about GPLs as it allows us to simplify matrix elements and eg. cancel the contribution from the two-loop with the one-loop-squared.
    \end{subequations}
\end{lemma}

\begin{theorem}
    To turn the formal solution~\eqref{eq:chen} into a practical solution, we expand it order-by-order
    \begin{align}
        \vec J(\vec s, \epsilon) &= \sum_{n=0}^\infty \epsilon^n \vec J^{(n)}\,,\\
        \begin{split}
            \vec J^{(0)}(\vec s, \epsilon) &= \vec J_0^{(0)}\,,\\
            \vec J^{(n)}(\vec s, \epsilon) &= \sum_i \int_0^x\frac{\D x'}{\eta_i} \frac{\partial\log(\eta_i)}{\partial x'} A_i\cdot \vec J^{(n-1)} + \vec J_0^{(n)}\,.
        \end{split}
    \end{align}
\end{theorem}

\begin{example}
    \label{ex:hqbub}
    We have with $m=1$
    \begin{align}
        \vec J^{(0)} &= \begin{pmatrix}1\\0\end{pmatrix}\,,\\
        \vec J^{(1)} &= \int_0^x\frac{\D x'}{x'-1}\begin{pmatrix}0\\-1\end{pmatrix}
                      + \int_0^x\frac{\D x'}{x'+1}\begin{pmatrix}0\\+1\end{pmatrix}
                      = \begin{pmatrix}0\\G(-1;x)-G(1;x)\end{pmatrix} \,,\\
        \vec J^{(2)} &= \int_0^x\frac{\D x'}{x'-1}\begin{pmatrix}0\\G(-1;x)-G(1;x)\end{pmatrix}
                      + \int_0^x\frac{\D x'}{x'+1}\begin{pmatrix}0\\G(-1;x)-G(1;x)\end{pmatrix}
                      + \begin{pmatrix}+\zeta_2\\0\end{pmatrix} \notag\\&
                      = \begin{pmatrix}+\zeta_2\\G(-1,-1;x)-G(-1,1;x)+G(1,-1;x)-G(1,1;x)\end{pmatrix}\,.
    \end{align}
    We could continue with this expansion as long as we want to and hence arrive at an expression for our integral.
\end{example}
