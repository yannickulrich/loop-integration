%!TEX root=notes
%PDFLATEX

\section{Series expansions}
The previous example of differential equations was fairly simple, we could guess a $d\log$ form without too much difficulty and our alphabet was $\{1+x, 1-x\}$.
While GPLs are fairly well studied objects, many phonologically relevant integrals cannot be expressed in terms of GPLs.
Instead they require elliptic integrals (or worse).
Even though it is sometimes still possible to evaluate these functions numerically, there is an aspect of diminishing return.
What do we gain from an analytic solution if it requires minutes or hours to be evaluated for each phase space point?
Instead, we can be pragmatic and prefer a semi-numerical solution in terms of one or more series expansions.
We will mostly follow~\cite{Hidding:2020ytt} but include further background information and a simple example.

Before we do this, we need to extend our discussion to processes with more than one kinematic variable.

\begin{lemma}[Integration paths]

    Consider a family of integrals that depend on more than one kinematic variable but rather a set $\{s\}$.
    To transport the boundary condition at some $\{s_0\}$ we define a line segment $\gamma(x)$ that connects our boundary condition to our target.
    For example,
    \begin{align}
        \gamma(x=0) = (\gamma_{s_1}(x), \gamma_{s_2})(x), \cdots) = \{s_0\}
        \quad\text{and}\quad
        \gamma(x=1) = \{s\}\,.
    \end{align}
    We can use \eqref{eq:subs} to write
    \begin{align}
        \frac{\partial\vec I}{\partial x} = A_x(x,\epsilon) \vec I
        \quad\text{with}\quad
        A_x = \sum_{s_i\in\{s\}} A_{s_i}(\gamma(x))\frac{\partial\gamma_{s_i}(x)}{\partial x}
    \end{align}
    to reduce our problem to the one-dimensional case.

\end{lemma}

\subsection{Reminder: ordinary differential equations}
\begin{theorem}[Frobenius method]
    Consider a $p$-th order differential equation of the form
    \begin{align}
        {\tt DE} = \sum_{j=0}^p c_j(x) \frac{\partial^j}{\partial x^j} f(x) = 0\,,
    \end{align}
    with $c_p = 1$ w.l.o.g.
    It is valid for the $c_j(x)$ to have poles as long as they permit a power expansion around $x=0$.
    We can obtain a solution of this equation by choosing an ansatz
    \begin{align}
        f(x) = x^r \sum_{i=0}^\infty a_i x^i
    \end{align}
    and comparing coefficients order-by-order in $x$.
    This gives us a linear system of equations for as many $a_i$ as we need which we can solve iteratively.
\end{theorem}
\begin{proof}
    We assume that the $c_j$ can be power expanded around $x=0$ as
    \begin{align}
        c_j(x) = x^{r_c}\sum_{k=0}^\infty c_{j,k}x^k\,,
    \end{align}
    where $r_c$ is chosen to equal for all $c_j$.
    With the derivatives
    \begin{align}
        \frac{\partial^j}{\partial x^j} f(x) = x^r \sum_{i=0}^\infty a_i x^{i-j}
            \frac{\Gamma[1 + i + r]}{\Gamma[1 + i - j + r]}
    \end{align}
    we now have
    \begin{align}
        {\tt DE} = \sum_{j=0}^p \sum_{k=0}^\infty \sum_{i=0}^\infty
            c_{j,k} a_i
            \frac{\Gamma[1 + i + r]}{\Gamma[1 + i - j + r]}
            x^{i-j+k+r_j+r}
        = 0\,.
    \end{align}
    We can collect terms by their power in $x$ and get a system of equations for the $a_i$
    \begin{align}
        {\tt DE} = \sum_{i=0}^\infty x^{i-p+r+r_c}
            \underbrace{
                \sum_{k=0}^p\sum_{j=0}^{i+k-p}
                a_j c_{k, i - j + k - p} \frac{\Gamma[1 + j + r]}{\Gamma[1 + j - k + r]}
            }_0\,.
    \end{align}
    The first non-zero term in this expansion is called the incidental equation and it will fix $r$ while keeping $a_0$ free.
    If $c_{p,0}$ is not zero, this term is at $i=0$
    \begin{align}
        i=0:\qquad a_0 c_{p,0} \frac{\Gamma[1 + r]}{\Gamma[1 - p + r]} = a_0 c_{p,0} \prod_{n=0}^{p-1}(r-n) = 0\,.
    \end{align}
    It is advisable to choose the largest solution for $r$ to perform recursion and obtain the remaining $p-1$ solutions of the differential equation.
    Once we have a value for $r$ we can solve the remaining system and obtain as many $a_j$ as we desire.
\end{proof}


\begin{theorem}[Reduction of order]
    After obtaining one solution $f_0$ for the differential equation, we can obtain another solution $f_1$ by applying the Frobenius method to
    \begin{align}
        {\tt DE}' = \sum_{j=0}^{p-1} \underbrace{
            \sum_{n=0}^{p-1-j}
                \binom{p-n}{1+j} c_{-n+p}(x)
                \frac{\partial^{p-n-j-1}f_0(x)}{\partial x^{p-n-j-1}}
            }
            \frac{\partial^j}{\partial x^j} f_1'(x) = 0\,,
            \label{eq:frob2}
    \end{align}
    and integrating $f_1 = f_0 \cdot \int \D x f_1'$ (which is trivial since $f_1'$ is a power series).
    Recursively applying this algorithm will eventually produce all $p$ solutions since ${\tt DE}'$ is of lower order than ${\tt DE}$.
\end{theorem}
\begin{proof}
    Applying the original differential equation to $f_1$ yields
    \begin{subequations}
    \begin{align}
        0 &=
            \sum_{j=0}^p c_j(x)
            \frac{\partial^j}{\partial x^j} \bigg(f_0(x) \cdot \int \D x f_1'(x)\bigg)
        \\ &=
            \sum_{j=0}^p c_j(x)
            \binom{j}{n}
            \frac{\partial^{j-n}f_0}{\partial x^{j-n}}
            \frac{\partial^n}{\partial x^n} \bigg(\int \D x f_1'(x)\bigg)
        \\ &=
            \sum_{j=0}^p c_j(x)
            \binom{j}{n}
            \frac{\partial^{j-n}f_0}{\partial x^{j-n}}
            \frac{\partial^{n-1}f_1'}{\partial x^{n-1}}\,.
    \end{align}
    \end{subequations}
    Now we only need to re-arrange the summation to arrive at \eqref{eq:frob2}

\end{proof}

\begin{lemma}
    We can translate a $p\times p$ system of first-order differential equations
    \begin{align}
        \frac{\partial\vec f}{\partial x} = M\vec f
        \label{eq:redhom}
    \end{align}
    into a $p$-th order differential equation for the first function $f_1$.
    Solving this for $f_1$ will then allow us to obtain the rest of $\vec f$.
    We can gather these into a $n\times p$ matrix $F$ s.t.
    \begin{align}
        \frac{\partial F}{\partial x} = M F\,.
    \end{align}
\end{lemma}
\begin{proof}
    By taking derivatives of \eqref{eq:redhom} it is obvious that even higher derivatives can be related back to $\vec f$ with different matrices $M^{(j)}$
    \begin{align}
        \begin{split}
            \frac{\partial^j\vec f}{\partial x^j}
                &= M^{(j)}\vec f\\
                &= \frac{\partial}{\partial x}\frac{\partial^{j-1}\vec f}{\partial x^{j-1}}
                = \frac{\partial}{\partial x}\Big( M^{(j-1)}\vec f\Big)
                = \frac{\partial M^{(j-1)}}{\partial x}\vec f
                    + M^{(j-1)}\frac{\partial \vec f}{\partial x}
                = \underbrace{\Bigg(
                    \frac{\partial M^{(j-1)}}{\partial x} + M^{(j-1)}M
                \Bigg)}_{M^{(j)}}\vec f\,.
        \end{split}
    \end{align}
    The top rows $M_{1i}^{(j)}$ of the new matrices define yet another matrix.
    Depending on how many derivatives we include in this we can obtain two different version:
    $\tilde M$, a $(p\times p)$-matrix, and $\bar M$, a $((p+1)\times p)$-matrix.

    Assuming it is invertible, $\tilde M$ can be used to reconstruct all integrals from the first one as
    \begin{align}
        \vec f = \tilde M^{-1} \Bigg(
            f_1,
            \frac{\partial f_1}{\partial x},
            \frac{\partial^2 f_1}{\partial^2 x},
            \cdots,
            \frac{\partial^{p-1} f_1}{\partial^{p-1} x}
        \Bigg)\,.
        \label{eq:vecfsol}
    \end{align}
    To actually obtain $f_1$, we want a $p$-th order differential equation for $f_1$.
    Note that since $\tilde M$ is assumed to be invertible, $\bar M$ has a single element $\vec c$ in its kernel, i.e. $\vec c \bar M=\vec 0$.
    Right-multiplying with $\vec f$ allows us to write
    \begin{align}
        0 = \sum_{j=0}^p c_j \tilde M_{j,i} \vec f
            = \sum_{j=0}^p c_j \frac{\partial^j f_1}{\partial x^j}
    \end{align}
    We can write~\eqref{eq:vecfsol} using the Wronskian matrix $W$
    \begin{align}
        W = \begin{pmatrix}
                             f_1 & \cdots &                  f_p\\
            \partial_x       f_1 & \cdots & \partial_x       f_p\\
            \vdots               & \ddots & \vdots \\
            \partial_x^{p-1} f_1 & \cdots & \partial_x^{p-1} f_p
        \end{pmatrix}\,,
    \end{align}
    as $F = \tilde M^{-1}W$.
\end{proof}

\newcommand{\inh}{\vec{\mathcal{I}}}
\begin{lemma}[Inhomogenous differential equations]
    To obtain the general solution of an inhomogeneous differential equation $\partial_x \vec f - M\vec f = \inh$, we need to add the general solution $F$ of the homogeneous equation $\partial_xF-MF=0$ to a particular solution of the inhomogeneous one.
    This is done by
    \begin{align}
        f_i = \sum_j \Bigg[
            F\cdot\Bigg(
                \int\D x F^{-1}\cdot\frac1p\Big(\inh,\cdots\inh\Big)
                + {\rm diag}\Big(c_1,\cdots,c_p\Big)
            \Bigg)
        \Bigg]_{ij}\,.
    \end{align}
    The $c_i$ are the integration constants that need to be fixed by the $p$ boundary conditions.
    Since we are only considering series expansions this integrals is very straightforward.
\end{lemma}
\begin{proof}
    Let us define the $p\times p$ matrices $B = (\inh,\cdots,\inh)/p$ and $E={\rm diag}(c_1,\cdots,c_p)$.
    Consider now the derivative of the bracket $G_{ij}=[\cdots]_{ij}$
    \begin{align}
        \partial_x G =
            \partial_x F\cdot\Big( \int\D x F^{-1}\cdot B + E \Big) +
            B\,.
    \end{align}
    Since $F$ is a solution of the homogeneous differential equation, i.e. $\partial_x F = MF$, we can see that $G$ is a solution of
    \begin{align}
        \partial_x G =
            M\cdot F\cdot\Big( \int\D x F^{-1}\cdot B + E \Big) +
            B
            = M\cdot G + B\,.
    \end{align}
    The factor of $1/p$ in $B$ arises from the sum to ensure that $\sum_j B_{ij} = \mathcal{I}_i$.
\end{proof}


\begin{observation}[Getting back to Feynman integrals]
    Since we can rescale integrals by an arbitrary power in $\epsilon$, we can write w.l.o.g. $M=M_0+M_1 \epsilon+M_2\epsilon^2+\cdots$.
    By expanding the differential equation order-by-order in $\epsilon$, we have
    \begin{align}
        \frac{\partial \vec I_k}{\partial x} - M_0 \vec I_k = \underbrace{\sum_{j=0}^{k-1} M_{k-j} \vec I_j}_{\vec{\mathcal{I}}_k}\,.
    \end{align}
    When working on the $k$-th order, we will assume that all previous orders are known so that the inhomogeneity of our differential equation $\vec{\mathcal{I}}_k$ is fully known.

    By choosing a clever ordering of the integral, i.e. one that makes $M$ as block-triangular as possible we can simplify the differential equations we need to solve considerably.
    It is fairly obvious that we should solve all subsectors of any given integral firsts since derivatives never evaluate to integrals in higher sectors.
\end{observation}

\begin{definition}[Coupled integrals]
    The matrix $M_0$ can be interpreted as describing a graph $G$ whose nodes are master integrals and whose edges corresponds to dependencies between integrals.
    If $(M_0)_{ij}\neq 0$, there is an edge from $i\to j$.
    We consider those integrals coupled that belong to the same \emph{strongly connected component} of the graph, i.e. those sub-graphs where a path from each node to each other node exists.
\end{definition}

\begin{lemma}[Integration sequence]
    An optimal integration sequence is obtained by considering the \emph{condensation} $\tilde G$ of the graph $G$, i.e. replacing each strongly connected sub-graph with a single node.
    We then sort the nodes topologically to ensure that those integrals with the fewest dependencies are solved first.
\end{lemma}

\begin{example}[Three-loop sunset]
    Consider the three-loop massive sunset
    \begin{align}
        I_{\alpha\beta\gamma\delta} = \begin{gathered}
            \begin{tikzpicture}[baseline=-2pt]
                \draw[dashed] (-1.5, 0) node[left] {$p^2$} -- (-1,0);
                \draw (0,0) circle(1);
                \draw (-1,0) arc (120:60:2);
                \draw (-1,0) arc (60:120:-2);
                \draw[dashed] (1.5, 0) -- (+1,0);
            \end{tikzpicture}
            = \int\frac{
                [\D k_1][\D k_2][\D k_3]
              }{
                \big[k_1^2-m^2\big]^\alpha
                \big[k_2^2-m^2\big]^\beta 
                \big[k_3^2-m^2\big]^\gamma
                \big[(k_1+k_2+k_3+p)^2-m^2\big]^\delta
            }\,,
        \end{gathered}
    \end{align}
    with $p^2=s\neq 0$ and $m=1$.
    It is fairly easy to find the differential equation in $s$
    \begin{subequations}
        \begin{align}
            \frac{\partial}{\partial s}
            \begin{pmatrix}
                I_{2211}\\ I_{2111}\\I_{1111}\\I_{1110}
            \end{pmatrix}
            = \big(M_0 + \epsilon M_1 \big)
            \begin{pmatrix}
                I_{2211}\\ I_{2111}\\I_{1111}\\I_{1110}
            \end{pmatrix}\,,
        \end{align}
        with matrices
        \begin{align}
            M_0 &= \begin{pmatrix}
                 \frac{18}{(s-16)(s-4)}&-\frac{4(s+20)}{(s-4)(s-16)s}& \frac{36}{(s-4)(s-16)s}&-\frac{2}{(s-16)s}\\
                -\frac{ 3}{       s-4 }& \frac{  s+12 }{(s-4)      s}&-\frac{ 6}{(s-4)      s}& 0 \\
                 0                     &-\frac{4      }{           s}& \frac{ 2}{           s}& 0 \\
                 0                     &  0                          &                      0 & 0
            \end{pmatrix}\,,\label{eq:graph3l:m0}\\
            M_1 &= \begin{pmatrix}
                 \frac{-s^2-16 s-64}{(s-16)(s-4)s}&\frac{14(s+20)}{(s-4)(s-16)s}&-\frac{174}{(s-4)(s-16)s}&-\frac{6}{(16-s)s} \\
                 0                                & \frac{-2 s-16}{(s-4)      s}& \frac{ 17}{(s-4)      s}& 0 \\
                 0                                & 0                           &-\frac{ 3}{            s}& 0 \\
                 0                                & 0                           & 0 & 0 \\
            \end{pmatrix}\,.
        \end{align}
    \end{subequations}
    The graph corresponding to $M_0$ is shown in Figure~\ref{fig:graph3l}


\end{example}

\begin{figure}
    \centering
    \begin{tikzpicture}[x=3em,y=3em]

        \draw [red]  (-4  ,-1) rectangle (-2.1,1);
        \draw [blue] (-1.9,-2) rectangle (1.9,2);

        \node (2111) at (-60:1) {$I_{2111}$};
        \node (1111) at ( 60:1) {$I_{1111}$};
        \node (2211) at (180:1) {$I_{2211}$};
        \node (1110) at (180:3) {$I_{1110}$};

        \draw [->] (2211) -- (1110);
        \draw [->] (2211) -- (1111);
        \draw [->] (2211) edge [bend right] (2111);
        \draw [->] (2211.60) arc (-30:220:0.4);

        \draw [->] (2111) edge [bend right=10] (2211);
        \draw [->] (2111) edge [bend right=20] (1111);
        \draw [->] (2111.-60) arc (30:-220:0.4);

        \draw [->] (1111) edge [bend right] (2111);
        \draw [->] (1111.60) arc (-30:220:0.4);
    \end{tikzpicture}
    \caption{
        The connection graph corresponding to \eqref{eq:graph3l:m0}.
        When solving this system, we begin with the red integral $I_{1110}$ before simultaneously solving the blue integrals.
    }
    \label{fig:graph3l}
\end{figure}


\subsection{A full worked example}
We can now consider an actual example, start to finish.
We have already derived the matrix for the heavy quark bubble $M\equiv M_y$ in~\eqref{eq:hqff:my}.
While we did guess a canonical form in Example~\ref{ex:hqbub} we will pretend to not know this and instead consider the pre-canonical basis.

\begin{example}[Boundary condition at $y=0$]
    We take our boundary condition at $y=0$, i.e. $s=0$.
    Note that this can be dangerous as the differential equation matrices have a singularity at $y=0$.
    However, it turns out that the integrals are well-behaved around this region.
    Otherwise, we would have to do an asymptotic expansion around $y=0$.

    The first integral, $J_1\sim I_{001}$, is trivial
    \begin{subequations}
    \begin{align}
        J_1 &= m^{-2+2\epsilon}\epsilon
                \int[\D k_1] \frac1{\big[(k_1-q)^2-m^2\big]}
            = -\epsilon\Gamma(1-\epsilon)\Gamma(-1+\epsilon)\\
            &=
              -1
              -\epsilon
              -(1+\zeta_2)\epsilon^2
              -(1+\zeta_2)\epsilon^3
              -\Big(1+\zeta_2+\frac74\zeta_4\Big)\epsilon^4
              +\mathcal{O}(\epsilon^5)\,.
    \end{align}
    \end{subequations}
    The second one, $J_2\sim I_{011}$, is slightly more complicated.
    We begin by deriving the $\U$ and $\F$ polynomials as we have learned in the previous section
    \begin{align}
    \begin{split}
        \U &= x_1+x_2\,,\\
        \F &= s\ x_1x_2 + m^2\U x_1 + m^2\U x_2
            = s\ x_1x_2 + m^2(x_1+x_2)^2
            \to m^2(x_1+x_2)^2\,.
    \end{split}
    \end{align}
    And hence,
    \begin{subequations}
        \begin{align}
            J_2(y=0) &= m^{2\epsilon}\epsilon I_{011}
                = -m^{2\epsilon}\epsilon
                    \Gamma(1-\epsilon) \Gamma(2-d/2)
                    \int_0^\infty\D x_1\D x_2\delta(\cdots)
                    \frac{\U^{2-d}}{(\F-\io)^{2-d/2}}
                \notag\\
                &= -\Gamma(1+\epsilon)\Gamma(1-\epsilon)
                    \int_0^\infty\D x_1\D x_2 \frac{\delta(1-x_1)}{(x_1+x_2)^2}
                = -\Gamma(1+\epsilon)\Gamma(1-\epsilon)\\
                &= 
                  -1
                  -\zeta_2\epsilon^2
                  -\frac74\zeta_4\epsilon^4
                  +\mathcal{O}(\epsilon^5)\,.
        \end{align}
    \end{subequations}
\end{example}
\begin{example}[Expansion around $y=0$]
    The differential equation we need to solve is only for $J_2=J_{2,0} + J_{2,1}\epsilon+J_{2,2}\epsilon^2+\cdots$
    \begin{subequations}
        \begin{align}
            \partial_y J_{2,0} &= \underbrace{M_{0,21} J_{1,0}}_{\mathcal{I}_0} + M_{0,22} J_{2,0}\,,\\
            \partial_y J_{2,k} &= \underbrace{M_{1,21} J_{1,k-1} + M_{1,22} J_{2,k-1}}_{\mathcal{I}_k} + M_{0,22} J_{2,k}\,.
        \end{align}
    \end{subequations}
    The homogeneous solution $J_h$ of $\partial_y J_h = M_{0,22} J_h$ does not depend on the order in $\epsilon$.
    Following the Frobenius method, we have
    \begin{subequations}
        \begin{align}
            J_h &= \sum_{i=0}^\infty y^{i+r} c_i\\
            \partial_x J_h &= \sum_{i=-1}^\infty (i+r+1)y^{i+r} c_{i+1}\\
            &= \frac1{2y(y-1)} \sum_{i=0}^\infty y^{i+r} c_i
             = -\frac12\sum_{j=-1}^\infty y^j\sum_{i=0}^\infty y^{i+r} c_i\,.
        \end{align}
    \end{subequations}
    The first term of this equation at $y^{r-1}$,
    \begin{align}
        y^{r-1}\Big( \frac{c_0}2 + rc_0\Big) = 0\,,
    \end{align}
    requires $r=-1/2$.
    In general, this would be numerically solved order-by-order.
    Here, we can write down a closed form solution as
    \begin{align}
        c_n = -\frac{(2n)!}{2^{1+2n}(1+n)(n!)^2}c_0\,.
    \end{align}
    Hence, our homogenous solution is
    \begin{align}
        J_h = 
                           c_0 y^{-1/2}
            -\frac{1}{  2} c_0 y^{1/2}
            -\frac{1}{  8} c_0 y^{3/2}
            -\frac{1}{ 16} c_0 y^{5/2}
            -\frac{5}{128} c_0 y^{7/2}
            -\frac{7}{256} c_0 y^{9/2}
            +\mathcal{O}(y^{11/2})\,.
    \end{align}
    Next, we need to derive particular solutions order-by-order.
    \begin{enumerate}
        \item $k=0$:
            \begin{subequations}
                The inhomogeneity is given by $J_{1,0}=-1$
                \begin{align}
                    \mathcal{I}_0 = \frac1{2y(y-1)}
                \end{align}
                We can now find
                \begin{align}
                    J_{p,0} = J_h \int\D y \frac{\mathcal{I}_0}{J_h}
                     = \frac{J_h}{c_0} \int\D y\Bigg(
                        -\frac{y^{-1/2}}{2}
                        -\frac{3 y^{1/2}}{4}
                        -\frac{15 y^{3/2}}{16}
                        -\frac{35 y^{5/2}}{32}
                        -\frac{315 y^{7/2}}{256}
                        +\mathcal{O}(y^{9/2})
                    \Bigg)\,.
                \end{align}
                Since $\int y^n = y^{1+n}/(1+n)$,
                \begin{align}
                    J_{p,0} = -1\,.
                \end{align}
            \end{subequations}
            Our boundary condition requires $J_{2,0}(y=0)=-1$ which means that $c_0=0$ and $J_{2,0} = J_{p,0} = -1$.

        \item $k=1$:
            We now have
            \begin{subequations}
                \begin{align}
                    \mathcal{I}_1 &= -1-y-y^2-y^3-y^4-y^5+\mathcal{O}(y^6)\\
                    J_{p,1} &=
                        -\frac{2 y}{3}
                        -\frac{4 y^2}{15}
                        -\frac{16y^3}{105}
                        -\frac{32y^4}{315}
                        -\frac{256y^5}{3465}+\mathcal{O}(y^6)\,.
                \end{align}
                Our boundary condition requires $J_{2,1}(y=0)=0$ which means that $c_0=0$ and $J_{2,1} = J_{p,1}$.
            \end{subequations}

        \item $k=2$:
            \begin{subequations}
                \begin{align}
                    \mathcal{I}_2 &= 
                        -\frac{\zeta_2}{2 y}
                        -\frac{\zeta_2}{2}
                        +y \Big(-\frac{\zeta_2}{2}-\frac{2}{3}\Big)
                        +y^2 \Big(-\frac{\zeta_2}{2}-\frac{14}{15}\Big)
                        +y^3 \Big(-\frac{\zeta_2}{2}-\frac{38}{35}\Big)
                        +\mathcal{O}(y^4)\,,\\
                    J_{p,2} &= -\zeta_2 
                        -\frac{4 y^2}{15}
                        -\frac{8 y^3}{35}
                        -\frac{176 y^4}{945}
                        -\frac{320y^5}{2079}
                        +\mathcal{O}(y^6)\,.
                \end{align}
            \end{subequations}
    \end{enumerate}
    We can continue this process until we our preferred order in $\epsilon$.

    We can plot the resulting function (expanded up to $\mathcal{O}(y^{15})$ for better convergence) with the exact result we have obtained in Example~\ref{ex:hqbub}.
    The resulting ratios are shown in Figure~\ref{fig:hqbub:pl1}.
    Note that the expansion becomes very bad around $y=\pm1$.
    This is due its finite radius of convergence $R=1$, as we will see next.
\end{example}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{diffexp1}
    \caption{
        The power-law expansion compared with the correct result for $\epsilon^1$ and $\epsilon^2$.
        The radius of convergence $R=1$ is indicated with the arrows.
    }
    \label{fig:hqbub:pl1}
\end{figure}

\begin{theorem}[Fuchs's theorem (applied to Feynman integrals)]
    When expanded around $x=0$, the general solution of the homogenous equation $\partial_x\vec f-M\vec f=0$ has a radius of convergence
    \begin{align}
        R = \min\Big\{|x_i|\ :\ \text{where $x_i$ are the poles of $M$}\Big\}
    \end{align}
    as these specify the radii of convergence of a series expansion of $M$.
\end{theorem}

\begin{lemma}[Matching of power series]
    Consider a function $f(x)$ with a power series with known coefficients $a_i$ around $x_0$ and a radius of convergence $R_0$
    \begin{align}
        f(x) = f_0(x) = (x-x_0)^{r_0} \sum_{i=0}^\infty a_i (x-x_0)^i\,.
    \end{align}
    Consider further an expansion around $x_1$ with yet unknown coefficients $b_i$ and a radius $R_1$
    \begin{align}
        f(x) = f_1(x) = (x-x_1)^{r_1} \sum_{i=0}^\infty b_i (x-x_1)^i\,.
    \end{align}
    If there is a point $x$ such that $|x-x_0| < R_0$ and $|x-x_1|<R_1$, we can evaluate $f_0(x)$ and use this as the boundary condition for $f_1$ to determine the $b_i$.
    We now have an expression for $f$ that covers a larger area
    \begin{align}\begin{split}
        f_{1,2} : \{x\ :\ |x-x_0|<R_0\} \cup\{x\ :\ |x-x_1|<R_1\} &\to \mathbb{C}\\
        x &= \begin{cases}f_0(x) & |x-x_0| < |x-x_1|\\f_1(x) & \text{otherwise}\end{cases}\,.
    \end{split}\end{align}
\end{lemma}

\begin{example}[Expanding around $y=-1/2$]
    By setting $y=-1/2-y'/2$, we can expand around $y=-1/2$ ($y'=0$) to extend our region of good convergence.
    This changes the differential equation
    \begin{align}
        M' = -\frac{1}{3 + 4 y' + y'^{2}}
            \begin{pmatrix}0&0\\-1+\epsilon&1+\epsilon(1+y')\end{pmatrix}\,,
    \end{align}
    the homogeneous solution
    \begin{align}
        J_{h'} = c'_0
            -\frac{c'_0}{3}y'
            +\frac{5 c'_0}{18} y'^2
            -\frac{13 c'_0}{54} y'^3
            +\frac{139 c'_0}{648} y'^4
            -\frac{379 c'_0}{1944} y'^5
            +\mathcal{O}(y'^6)\,,
    \end{align}
    and the particular solutions
    \begin{align}
        J_{p',0} &= 1\,,\\
        J_{p',1} &=
            \frac{y'}{3}
            -\frac{y'^2}{9}
            +\frac{2 y'^3}{27}
            -\frac{5 y'^4}{81}
            +\frac{67 y'^5}{1215}
            +\mathcal{O}(y'^6)\,.
    \end{align}
    The general solution $J_{h'}+J_{p',1}$ has one free parameter, $c'_0$ that can be fixed through the matching at eg. $y=-1/6$ ($y'=-2/3$)
    \begin{align}
        J_{2,1}\Big(y=-\frac16\Big) &= \frac{255388946}{2447679465} = 0.104339\,,\\
        \Rightarrow c'_0 &= \frac{185606208569944}{660431033911495} = 0.281038\,.
    \end{align}
    The new result is shown in Figure~\ref{fig:hqbub:pl2}.
    Note that $M'$ has poles at $y'=\{-1,-3\}$ ($y=\{1,0\}$) indicating a radius of convergence of $R'=1$.
\end{example}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{diffexp2}
    \caption{
        The stiched result expanded around $y=0$ and $y=-1/2$ (indicated by the dots) and the area in which each result is to be used (indicated by the arrows).
    }
    \label{fig:hqbub:pl2}
\end{figure}

\begin{example}[Expanding around $y=-1$]
    We can repeat this one last time and expand around $y = -1-y'' = -1$ to cover the whole range $-2<y<1$.
    We perform the matching $y=-5/6$ with the result is shown in Figure~\ref{fig:hqbub:pl3}.
\end{example}

\begin{observation}
    Note that we could have jumped from $y=0$ to $y=-1$ directly without expanding around $y=-1/2$ first.
    However, as shown in Figure~\ref{fig:hqbub:pl3}, the resulting precision is significantly reduced as the $y=0$ expansion is less precise at the matching point $y=-1/2$.
    It is therefore advisible to use smaller steps or techniques that improve convergence such as M\"o{}bius transforms (ensure that the nearest pair of singularities are an equal distance from the origin) or Pad\'e{} approximants (rational functions of fixed degree that can be derived from the Taylor expansion and that usually perform better)~\cite{Hidding:2020ytt}.
\end{observation}

\begin{figure}
    \centering
    \subfloat[ $y=-1$, $y=-1/2$, $y=0$ ]{
        \includegraphics[width=0.48\textwidth]{diffexp3}
    }%
    \subfloat[ $y=-1$, $y=0$ ]{
        \includegraphics[width=0.48\textwidth]{diffexp3v2}
    }
    \caption{
        The stiched result to cover the full domain $y=[-2,0]$, expanded with three nodes (left panel) or two nodes (right panel).
    }
    \label{fig:hqbub:pl3}
\end{figure}

\input{amflow}
