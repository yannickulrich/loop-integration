(* ::Package:: *)

(* ::Subsection:: *)
(*Normal family without auxiliary mass*)


(* ::Text:: *)
(*We define the following master integrals*)


masters = {base[1, 0, 0, 2], base[0, 1, 2, 0], base[1, 1, 1, 1]};


(* ::Text:: *)
(*Let us load the differential equations and undo the crossing*)


deqBase = Get["deq.base.m"] /. {
    INT["base", __, {a__}] :> base[a],
    INT["basex123", 2, 6, 3, 0, {0, a_, b_, 0}] -> base[a, 0, 0, b],
    INT["basex1234", 2, 6, 3, 0, {0, 1, 2, 0}] -> base[0, 1, 2, 0],
    INT["basex1243", 2, 6, 3, 0, {0, 1, 2, 0}] -> base[1, 0, 0, 2]
};


(* ::Text:: *)
(*We can define the matrices for s and t*)


As = Simplify@Normal[CoefficientArrays[
    der[#, s] & /@ masters /. deqBase,
    masters
][[2]]];


At = Simplify@Normal[CoefficientArrays[
    der[#, t] & /@ masters /. deqBase,
    masters
][[2]]];


(* ::Text:: *)
(*Our canonical basis requires a simple rescaling by s, t and s t \[Epsilon]*)


T = Inverse@CoefficientArrays[
    basis = (-s)^ep ep {
        t base[0, 1, 2, 0],
        s base[1, 0, 0, 2],
        s t ep base[1, 1, 1, 1]
    },
    masters
][[2]];


{Bs, Bt} = MapThread[
    Inverse[T] . #1 . T - Inverse[T] . D[T, #2] /. d -> 4 - 2ep &,
    {{As, At}, {s, t}}
]//Simplify;


(* ::Text:: *)
(*In the good variable x=t/s we have*)


Print[MatrixForm[
    Bx = d[t, x] Bt /. t -> s x /. d -> D // Simplify
]];


Quiet[CreateDirectory["mat"]];
Put[Coefficient[Bx, ep, 0], "mat/dx_0.m"];
Put[Coefficient[Bx, ep, 1], "mat/dx_1.m"];


(* ::Subsection:: *)
(*Auxiliary mass*)


(* ::Text:: *)
(*We now have three more master integrals*)


mastersAM = Join[
    masters /. base -> auxiliary,
    {auxiliary[1, 0, 0, 0], auxiliary[1, 1, 0, 1], auxiliary[1, 1, 1, 0]}
];


(* ::Text:: *)
(*Loading the files from reduze*)


deqAM = Get["deq.aux.m"] /. {
    INT["auxiliary", __, {a__}] :> auxiliary[a],
    INT["auxiliaryx123", 2, 6, 3, 0, {0, a_, b_, 0}] -> auxiliary[a, 0, 0, b],
    INT["auxiliaryx1234", 2, 6, 3, 0, {0, 1, 2, 0}] -> auxiliary[0, 1, 2, 0],
    INT["auxiliaryx1243", 2, 6, 3, 0, {0, 1, 2, 0}] -> auxiliary[1, 0, 0, 2],
    INT["auxiliaryx123", 3, 7, 3, 0, {1, 1, 1, 0}] -> auxiliary[1, 1, 0, 1]
};


(* ::Text:: *)
(*And defining the matrix Subscript[A, \[Eta]]*)


Aeta = CoefficientArrays[
    der[#, eta] & /@ mastersAM /. deqAM,
    mastersAM
][[2]] // Normal // Simplify;


(* ::Text:: *)
(*Our basis is now not rescaled except to make it finite in \[Epsilon] and \[Eta]*)


T = Inverse@CoefficientArrays[
    basisAM = Join[
        ep {
            auxiliary[0, 1, 2, 0],
            auxiliary[1, 0, 0, 2],
            ep auxiliary[1, 1, 1, 1]
        },
        ep {
            eta^-1 ep auxiliary[1, 0, 0, 0],
            auxiliary[1, 1, 0, 1],
            auxiliary[1, 1, 1, 0]
        }
    ],
    mastersAM
][[2]];


(* ::Text:: *)
(*We can now write our matrix Subscript[B, \[Eta]]*)


BEta = Inverse[T] . Aeta . T - Inverse[T] . D[T, eta] /. d -> 4 - 2 ep // Simplify;


Put[BEta, "AMMat.m"];
