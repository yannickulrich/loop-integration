(* ::Package:: *)

(* ::Subsection:: *)
(*Setup*)


(* ::Text:: *)
(*We need to define a few setup variables, namely the number of terms in the expansion (30), the phase space point used, the order in the ep expansion, and the R ratio*)


nmax=30;
num={s->-3,t->-1};
oep=3;
Rratio=3;


(* ::Subsection:: *)
(*Utilities*)


(* ::Text:: *)
(*This is a fast, iterative, linear solver*)


ClearAll[MySolve]
MySolve[args___][eq_,n_]:=Module[{A,B},
    {A,B} = CoefficientArrays[
        eq,
        c[args,n,#]&/@Range[6]
    ]//Normal;
    Thread[c[args,n,#]&/@Range[6]->Simplify[LinearSolve[B,-A]]]
]
MySolve[args___][mat_]:=Module[{rels},
    rels={};
    Table[
        rels=Simplify@Join[rels,MySolve[args][mat[[;;,i]]/.rels,i]],
        {i,Length[mat[[1]]]}
    ];
    rels
]


EvaluateSeries[expr_,val_]:=expr/.{
    HoldPattern@SeriesData[eta,a__]:>ReplaceAll[
        Normal@SeriesData[eta,a],eta->val
    ]
}


(* ::Subsection:: *)
(*Step size*)


matrix=Get["AMMat.m"];


(* ::Text:: *)
(*Our matrix has poles for certain values of eta != 0, inf *)


poles=DeleteDuplicates[Flatten[
    eta/.Solve[#==0]&/@Expand@Select[
        DeleteDuplicates@Flatten[
            List@@@Select[
                Flatten[one Denominator[matrix/.num]],
                #=!=one&
            ]
        ],
        #=!=one&
    ]
]];


validpoles=Select[poles,#=!=0&];


eta0=-I Rratio Max[Abs@validpoles];
etaN=-I/Rratio Min[Abs@validpoles];


(* ::Text:: *)
(*The step size is determined by solving ((R-1)/R)^n eta0 = etaN*)


Nsteps = Floor[n] /. First@Solve[
    ((Rratio-1)/Rratio)^n eta0 == etaN,
    n,
    Assumptions->n>0
];


(* ::Text:: *)
(*We can now actually write down the steps*)


steps = Append[
    ((Rratio-1)/Rratio)^Range[0, Nsteps] eta0,
    etaN
];


(* ::Subsection:: *)
(*Boundary conditions*)


(* ::Text:: *)
(*The boundary conditions at eta = inf are just vacuum integrals with s=t=0*)


boundary=Gamma[1-ep]Gamma[1+ep]eta^(-1-ep) {
    -1/2*ep,
    -1/2*ep,
    ep^2*(1 + ep)/6/eta,
    ep*eta/(1 - ep),
    -1/2*ep,
    -1/2*ep
};


(* ::Subsection:: *)
(*Matching at \[Eta]=-I \[Infinity]*)


(* ::Text:: *)
(*Our ansatz here is I[j] = eta^(a+b ep) \sum_{i = 0} eta^-i c[i,j]*)


ansatz1 = eta^Exponent[boundary, eta] Table[
    Sum[eta^-i c[i,j], {i, 0, nmax}]
        + SeriesData[eta, Infinity, {}, nmax, nmax+1,1],
    {j, 6}
];


(* ::Text:: *)
(*The c[0,j] are found using the boundary conditions*)


c01sol =Thread[
    c[0, #] &/@ Range[6] -> boundary /. eta -> 1
];


(* ::Text:: *)
(*Using the differential equations, we can now find the other c[i,j]. Note that we divide by eta^-ep to help Mathematica*)


ansatz1Sol = Join[
    c01sol,
    MySolve[][
        Expand[
            (D[ansatz1, eta] - matrix . ansatz1)/eta^-ep
            /. c01sol
            /. num
        ][[;;,3,;;]]
    ]
];


sol0=Series[Normal[ansatz1]/.ansatz1Sol,{ep,0,oep},{eta,Infinity,nmax}];


(* ::Subsection:: *)
(*Matching at \[Eta]=Subscript[\[Eta], i]*)


(* ::Text:: *)
(*The next ansatz is an expansion around eta[i]. We write it as a series in eta and then in eta - eta[i]*)


ansatzi[etai_] := Table[
    SeriesData[ep, 0, Table[
        SeriesData[eta,etai,Table[
            c[k,i,j], {i, 0, nmax}
        ],0,nmax+1,1],
        {k, 0, oep}
    ], 0, oep+1, 1],
    {j, 6}
]


(* ::Text:: *)
(*The first term of this expansion is again given by the boundary at eta = eta[i]*)


GetBoundary[j_Integer, prevsol_SeriesData] := Table[
    c[k, 0, j] -> SeriesCoefficient[prevsol, k],
    {k, 0, oep}
]
GetBoundary[etai_Complex,prevsol_List] := Flatten[MapThread[
    GetBoundary, {
        Range[6],
        EvaluateSeries[prevsol, etai]
    }
]]


(* ::Text:: *)
(*We can now build the differential equation condition for the eta[i] matching*)


BuildDEQi[etai_,boundary_]:=Thread[(
    D[ansatzi[etai], eta] - N@ReplaceAll[matrix,num] . ansatzi[etai] /. boundary
)[[;;,3,;;,3]]];


(* ::Text:: *)
(*and iteratively solve it*)


SolveDEQi[list_List]:=Module[{rels},
    rels={};
    Do[
        rels = Join[rels,MySolve[k][list[[k+1]]/.rels]],
        {k,0,oep}
    ];
    rels
]


(* ::Text:: *)
(*Combining everything gives us our i-th solution*)


Solvei[prevsol_,etai_]:=Module[{b},
    b=GetBoundary[etai,prevsol];
    ansatzi[etai]/.b/.SolveDEQi[BuildDEQi[etai,b]]
]


(* ::Text:: *)
(*Now we can just apply Fold*)


AbsoluteTiming[
    soln = Fold[Solvei,Chop@N@sol0,steps];
]//First


(* ::Subsection:: *)
(*Matching at \[Eta]=SuperPlus[0]*)


(* ::Text:: *)
(*The matrix we now need is \tilde A = eta B[eta]*)


Atild = Simplify[eta matrix];
Atild0 = Atild /. eta -> 0;


(* ::Text:: *)
(*We need to make sure the difference between its eigenvalues are non-integer*)


(* ::Input:: *)
(*Eigenvalues[Atild0]*)


(* ::Text:: *)
(*We now need to solve for the P_n*)


SolveP[plist_, n_] := Module[{Pmat, eq, A, B},
    Pmat=Outer[P,Range[6],Range[6]];
    eq=Flatten[
        n Pmat + (Pmat . Atild0-Atild0 . Pmat/.num) - Sum[
            SeriesCoefficient[Atild /. num,{eta,0,n-k}] . plist[[k+1]],
            {k, 0, n-1}
        ]
    ];
    {A,B}=CoefficientArrays[eq,Flatten[Pmat]];
    Append[plist,Partition[LinearSolve[B,-A],6]]
]


(* ::Text:: *)
(*Again, using Fold to construct all P_n*)


AbsoluteTiming[
    plist = Fold[SolveP,{IdentityMatrix[6]},Range[nmax]];
] // First


(* ::Text:: *)
(*The full expression for P is therefore*)


P = Total[plist etaN^Range[0, nmax]];


(* ::Text:: *)
(*and the inverse matrix P .  etaN ^ \tilde A(0)*)


invmat = Inverse@Chop@Series[
    N[P . MatrixExp[Atild0 Log[etaN]] /. num],
    {ep, 0, oep}
];


(* ::Text:: *)
(*The last bit is the \lim_{eta -> 0^+} eta^\tilde A(0)*)


lim=Expand[MatrixExp[Atild0 Log[eta]]] /. eta^-ep -> 0 /. num;


ans = Chop[lim . invmat . EvaluateSeries[soln,etaN]]
