#!/bin/sh

openscad -o 3dmodel-inner.stl -D "part=\"I\"" 3dmodel.scad
openscad -o 3dmodel-upper.stl -D "part=\"U\"" 3dmodel.scad
openscad -o 3dmodel-lower.stl -D "part=\"L\"" 3dmodel.scad
openscad -o 3dmodel-axes.stl -D "part=\"A\"" 3dmodel.scad
