(* vim: set foldmethod=marker *)
(* Init{{{*)
(* Let us define a scalar product and a Lorentz tensor object *)
SetAttributes[SP,Orderless];
SP[x_] := SP[x, x]
SP[x_Plus,y_]:=SP[#,y]&/@x
SP[l_?NumberQ x_,y_]:= l SP[x,y]

LTensor /: LTensor[x_, mu_] LTensor[y_, mu_] := SP[x, y]
LTensor /: LTensor[x_, mu_]^2 := SP[x, x]

Derivative[0, 1][SP][x_, y_] := LTensor[x, mu]
Derivative[1, 0][SP][x_, y_] := LTensor[y, mu]

Derivative[1, 0][LTensor][k_, mu_] = d
(*###############################################################}}}*)
(* Families{{{*)
onshell = {SP[p] -> m^2};
family = {SP[k1], SP[k2], SP[k1-k2-p] - m^2, SP[k1, p], SP[k2, p]} /. onshell;

sp2den = First@Solve[
    den /@ Range[Length[family]] == family,
    DeleteDuplicates@Cases[family, _SP, -1]
];
den2int = Times @@ (
    den[#1]^ToExpression["a"<>ToString[#1]<>"_"] &/@
    Range[Length[family]]
) -> int[-ToExpression["a"<>ToString[#1]]  &/@ Range[Length[family]]];

FromFamily[expr_]:=expr/.int[a_] :> Times@@(family^-a)
ToFamily[expr_]:=Expand[expr/.sp2den]/.den2int
(*###############################################################}}}*)
(* Get IBP{{{*)
moms = {LTensor[k1, mu], LTensor[k2, mu], LTensor[p, mu]};

ibps = Collect[ToFamily@Expand[Join[
    D[moms FromFamily[int[{a1,a2,a3,a4,a5}]],k1],
    D[moms FromFamily[int[{a1,a2,a3,a4,a5}]],k2]
]]/.onshell, _int, Factor];


s2n[a1] = 1;
s2n[a2] = 2;
s2n[a3] = 3;
s2n[a4] = 4;
s2n[a5] = 5;

ibps//.{
    int[{a___,b_+1,c___}]:>int[{a,b,c}] SuperPlus@s2n[b],
    int[{a___,b_-1,c___}]:>int[{a,b,c}] SuperMinus@s2n[b],
    int[{a1,a2,a3,a4,a5}] -> 1
}
(*###############################################################}}}*)
(* Lexicographic ordering{{{*)
(* We define the sector as usual *)
Sector[int[i_]] := FromDigits[
    # > 0 & /@ i /. True -> 1 /. False -> 0 // Reverse,
    2
]
zerosectors = {0, 1, 2, 3, 4, 5, 6};

(* We define t as the number of propagators,
             r as the sum of pos. powers,
             s as the sum of neg. powers *)
TRS[a_List] := {
    Length@Select[a, # > 0 &],
    Total[ Select[a, # > 0 &]],
    Total[-Select[a, # < 0 &]]
}
TRS[int[{a__Integer}]] := TRS[{a}]
(* We pick the following random order *)
LexiOrdered[a_int, b_int] := Module[{t1, r1, s1, t2, r2, s2},
    {t1, r1, s1} = TRS[a]; {t2, r2, s2} = TRS[b];
    If[t1 > t2, Return[-1]];
    If[t1 < t2, Return[1]];
    If[s1 > s2, Return[-1]];
    If[s1 < s2, Return[1]];
    If[r1 > r2, Return[-1]];
    If[r1 < r2, Return[1]];

    Return[0]
]
(*###############################################################}}}*)
(* Laporta's algorithm{{{*)
(* Given an expression, we need to find the most complicated
   integral in it *)

MostComplicated[expr_] := Last@Sort[
    DeleteDuplicates[Cases[expr, _int, -1]],
    LexiOrdered
]
MostComplicated[i_int] := i

(* Let us define a cut-off point in r and s and list everything below *)
rmax = 3;
smax = 1;
ClearAll[IsOkay]
IsOkay[int[{a1_,a2_,a3_,a4_,a5_}]] /; a4 > 0 := False
IsOkay[int[{a1_,a2_,a3_,a4_,a5_}]] /; a5 > 0 := False
IsOkay[int[a_]] /; TRS[a][[1]] > 3 := False (* t *)
IsOkay[int[a_]] /; TRS[a][[2]] > rmax := False
IsOkay[int[a_]] /; TRS[a][[3]] > smax := False
IsOkay[__] = True;

intlist = Flatten[Outer[
    int[{##}]&,
    Sequence @@ ConstantArray[Range[-smax, rmax], Length[family]]
]];

seedPre = Sort[
    Select[intlist, IsOkay],
    LexiOrdered
];
seedRep = seedPre /. int[a_] :> ReplaceAll[Thread[(ToExpression["a"<>ToString[#1]]  &/@ Range[Length[family]]) -> a]];

(* We now run Laporta's algorithm *)
rels = {i_int /; MemberQ[zerosectors, Sector[i]] -> 0};
relss = {};
Table[
    rel = Collect[seed[id] //. rels, _int, Factor];
    AppendTo[relss, {seed, id, seed[id] /. rels[[1]]}];
    If[rel =!= 0,
        rels = Flatten[Join[rels, Solve[rel == 0, MostComplicated[rel]]]];
    ],
    {seed, seedRep[[ ;; ]]}, {id, ibps}
];

(* At the borders of the seed range we might get some useless
   identities that relate two complicated integrals we don't know. *)
IsUseless[a_ -> b_] := Quiet[
    LexiOrdered[MostComplicated[a], MostComplicated[b]] === 0
]
RemoveUseless[rels_] := Select[rels, Not@*IsUseless]

(* We also need to do the re-substitution and simplification *)
rels2 = RemoveUseless@Thread[
    rels[[2 ;;, 1]] -> Collect[
        rels[[2 ;;, 1]] //. rels, _int, Factor
    ]
];


Inv[x_]:=1/x
SolveFor[(a_ : 0) + (b_ : 1) x_, x_] := {x -> -a Inv[b]}

Laporta[seedRep_, ibps_]:=Module[{rels, rel},
    rels = {i_int /; MemberQ[zerosectors, Sector[i]] -> 0};
    Table[
        rel = Collect[seed[id] //. rels, _int, Factor];
        If[rel =!= 0,
            rels = Flatten[Join[
                rels,
                SolveFor[rel, MostComplicated[rel]]
            ]];
        ],
        {seed, seedRep[[ ;; ]]}, {id, ibps}
    ];
    RemoveUseless@Thread[
        rels[[2 ;;, 1]] -> Collect[
            rels[[2 ;;, 1]] //. rels, _int, Factor
        ]
    ]
]

(*###############################################################}}}*)
