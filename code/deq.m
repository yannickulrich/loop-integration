(* vim: set foldmethod=marker *)
(* Init{{{*)
(* Let us first load the IBP reduction from laporta.m *)
Get["laporta.m"];
(*###############################################################}}}*)
(* Construct DEQ {{{*)
(* Tools {{{*)
ClearAll[deq1]
(* This constructs the a differential operator `d/dx` and applies it to `p1.p2`. Note that `p1` and `p2` needs to be of the form `p[i_Integer]`. *)
deq1[n_][{p1_, p2_}, x_] := {
  DD[expr, x] -> Sum[
    a[i, j] LTensor[p[i], mu] dd[expr, p[j]],
    {i,n}, {j,n}
  ],
  d[SP[p1, p2], x] == Sum[
    a[i, j] LTensor[p[i], mu] D[SP[p1, p2], p[j]],
    {i,n}, {j,n}
  ]
}
(* This builds the differential operators for all invariants sij and applies them to n momenta *)
deq1[n_][sij_List] := Outer[
  deq1[n],
  Tuples[p /@ Range[n], 2],
  sij,
  1
]
(* Call this with a list of invariants and momenta to construct differential operators *)
deq1[sij_List, mom_List] := deq1[Length[mom]][sij] /. Thread[
  p /@ Range[Length[mom]] -> mom
]

(* Solve for all coefficients a[i,j] *)
SolveAll[expr_] := Solve[expr, Cases[expr, _a, -1]]
(*####################################################}}}*)
(* Actually construct DEQ {{{*)
(* Actually build the differential operators d/ds and d/dm2 *)
diffop = deq1[{s, m2}, {p, q}] /. onshell /. m -> Sqrt[m2] /. d -> D;
diffops = diffop[[1, 1, 1]] /. {
  a[1, 1] -> 0
} /. SolveAll[
  diffop[[;; , 1, 2]] /. a[1, 1] -> 0
];
diffopm = diffop[[1, 2, 1]] /. {
  a[1, 2] -> a[2, 1]
} /. SolveAll[
  diffop[[;; , 2, 2]] /. a[1, 2] -> a[2, 1]
];
diffop = Join[diffops, diffopm];
(* We can now build a differential equation for a master integral I_{a,b,c} *)
deq[{a_, b_, c_}] = Collect[ToFamily[Expand[
  diffop /. {
    expr -> FromFamily[int[{a, b, c}]]
  } /. {
    dd -> D
  } /. {
    m -> Sqrt[m2]
  }
]], _int, Simplify]
(* Our masters are given by what's left over after reduction *)
masters = DeleteDuplicates@Cases[
  rels2[[;; , 2]] /. int[{0, 1, 0}] -> int[{0, 0, 1}],
  _int,
  -1
]
(* and the differential equations are *)
deqs = Thread@Collect[
  deq @@@ masters /. rels2 /. {
    m -> Sqrt[m2],
    int[{0, 1, 0}] -> int[{0, 0, 1}]
  }, _int, Simplify
];
(*####################################################}}}*)
(* Matrices A and checks {{{*)
(* We can now build our matrices As and Am2 *)
MatrixForm[As = Simplify@Normal@CoefficientArrays[deqs[[1, ;; , 2]], masters][[2]]]
MatrixForm[Am2 = Simplify@Normal@CoefficientArrays[deqs[[2, ;; , 2]], masters][[2]]]
(* We can check scaling *)
Simplify[s As + m2 Am2] == DiagonalMatrix[{d-2,d-4}/2]
(*####################################################}}}*)
(*###############################################################}}}*)
(* Change of variables {{{*)
(* Let us rescale the integrals to make them finite & free of overall scaling *)
T = Inverse[
  CoefficientArrays[mastersNew = {
    m2^ep ep int[{0, 0, 1}],
    m2^(1 + ep) ep int[{0, 1, 1}]
  } /. rels2,
  masters
][[2]]];
(* The new matrices Bs and Bm *)
Map[MatrixForm, {Bs, Bm} = MapThread[
  Collect[
    Inverse[T] . #1 . T - Inverse[T] . D[T, #2] /. {
      d -> 4 - 2 ep
    }, ep, Simplify
  ] &,
  {{As, Am2}, {s, m2}}
]]
(* Scaling is now trivial *)
Simplify[s Bs + m2 Bm] == IdentityMatrix[2]
(* Let's define `s=4m2 y` *)
MatrixForm[
	By = Collect[
		Bs d[s, y] /. s -> 4 m2 y /. d -> D,
		ep,
		Simplify
  ]
]
(* Canonical basis {{{*)
(* Our canonical basis is *)
basis2 = {
  ep m2^(ep + 1) int[{0, 0, 2}],
  ep Sqrt[s] Sqrt[s - 4 m^2] m2^(ep + 1) int[{0, 1, 2}]
};

T2 = Simplify@Inverse[CoefficientArrays[
  basis2 /. rels2 /. {
    m -> Sqrt[m2],
    int[{0, 1, 0}] -> int[{0, 0, 1}]
  },
  masters
][[2]]] /. d -> 4-2ep // Simplify

Map[MatrixForm, {B2s, B2m} = MapThread[
  Collect[
    Inverse[T2] . #1 . T2 - Inverse[T2] . D[T2, #2] /. {
      d -> 4 - 2 ep
    }, ep, Simplify
  ] &,
  {{As, Am2}, {s, m2}}
]]

(* and let us also define `s = -4x^2/(1-x^2)` *)
MatrixForm[
  B2x = Simplify[
    B2s d[s, x] /. {
      s -> -((4 x^2)/(1 - x^2)) m2
    } /. d -> D,
    m2 > 0 && 1 > x > 0
  ]
]

(* Defining two letters *)
eta1 = x-1; eta2 = x+1;
M1 = {{0,0}, {-1,1}};
M2 = {{0,0}, {1,1}};
(* we can write *)
B2x == Dt[
  ep (M1 Log[eta1] + M2 Log[eta2])
] /. {Dt[ep] -> 0, Dt[x] -> 1} // Simplify
(*####################################################}}}*)
(*###############################################################}}}*)
(* Solving the integral {{{*)
(* Boundary conditions{{{*)
(* We can calculate the boundary conditions simply as *)
boundaryExact = {-m2 Gamma[1 - ep] Gamma[1 + ep], 0};
boundary[0] = SeriesCoefficient[boundaryExact, {ep, 0, 0}];
boundary[1] = SeriesCoefficient[boundaryExact, {ep, 0, 1}];
boundary[2] = SeriesCoefficient[boundaryExact, {ep, 0, 2}];
boundary[3] = SeriesCoefficient[boundaryExact, {ep, 0, 3}];
boundary[4] = SeriesCoefficient[boundaryExact, {ep, 0, 4}];
(*####################################################}}}*)
(* Integration of GPLs{{{*)
GIntegrate[expr_] := Expand[expr] /. {
  G[a___, x]/(b_ + x) :> G[-b, a, x]
}
(*####################################################}}}*)
(* Solution{{{*)
(* Adding `G[x]=1` helps GIntegrate *)
mysol[0] := boundary[0] G[x]
mysol[n_] := GIntegrate[
  1/eta1 M1.mysol[n-1] + 1/eta2 M2.mysol[n-1]
] + boundary[n]G[x]
answer = SeriesData[ep, 0, #, 0, 5, 1] & /@ Thread[
  mysol /@ Range[0, 4]
]
(*####################################################}}}*)
(*###############################################################}}}*)
(* Series expansions{{{*)
(* Particular solutions{{{*)
(* The tadpole integral is just *)
asol = Thread[
  a/@Range[0,5] -> Simplify[
    Series[ep Gamma[1-ep] Gamma[-1+ep], {ep,0,5}][[3]]
  ]
];

ParticularSolution[inh_, y_, solHom_, nn_ : 10] := Simplify@Series[
  solHom (
    Expand[
      y^zero Normal@Series[inh/solHom, {y,0,nn}] /. asol
    ] /. y^n_ :> y^(1+n)/(1+n) /. zero -> 0
  ), {y,0,nn}
]
(*####################################################}}}*)
(* Expansion around y=0{{{*)
(* we only care about the second integral *)
By00 = Coefficient[By[[2]], ep, 0];
By01 = Coefficient[By[[2]], ep, 1];

bhom0 = y^r Sum[y^i c[i], {i, 0, 15}];
deqSub0 = D[bhom0, y] - By00[[2]] bhom0;
Series[deqSub0, {y,0,0}]
(* Hence r=-1/2 *)
solHom0 = Series[
  bhom0 /. r->-1/2 /. First@Solve[
    Series[deqSub0 /. r-> -1/2, {y,0,10}][[3]]==0
  ],
  {y, 0, 10}
]

(* ep^0: inhomogeneity is just B_{1,0}a_0 *)
bsol0[0] = ParticularSolution[By00[[1]] a[0], y, solHom0] + solHom0;
bsol0[0] = bsol0[0] /. c[0] -> 0;

(* For the next orders, the inhomogeneities are B_{1,0} a_{k-1} + B_{2,0} b_{k-1} + B_{1,1} a_k *)
bsol0[1] = ParticularSolution[By01.{a[k-1], bsol0[k-1]} + By00[[1]] a[k] /. k -> 1 /. asol, y, solHom0] + solHom0 /. c[0] -> 0;
bsol0[2] = ParticularSolution[By01.{a[k-1], bsol0[k-1]} + By00[[1]] a[k] /. k -> 2 /. asol, y, solHom0] + solHom0 /. c[0] -> 0;
bsol0[3] = ParticularSolution[By01.{a[k-1], bsol0[k-1]} + By00[[1]] a[k] /. k -> 3 /. asol, y, solHom0] + solHom0 /. c[0] -> 0;
bsol0[4] = ParticularSolution[By01.{a[k-1], bsol0[k-1]} + By00[[1]] a[k] /. k -> 4 /. asol, y, solHom0] + solHom0 /. c[0] -> 0;

mysol0 = {
  SeriesData[ep, 0, a /@ Range[0, 4] /. asol, 0, 5, 1],
  SeriesData[ep, 0, bsol0 /@ Range[0, 4], 0, 5, 1]
};
(*####################################################}}}*)
(* Expansion around y=-1/2{{{*)
(* we only care about the second integral *)
By1 = By d[y, yy] /. y -> -1/2 - yy/2 /. d -> D;
By10 = Coefficient[By1[[2]], ep, 0];
By11 = Coefficient[By1[[2]], ep, 1];

bhom1 = yy^r Sum[yy^i c[i], {i, 0, 15}];
deqSub1 = D[bhom1, yy] - By10[[2]] bhom1;
Series[deqSub1, {yy,0,0}]
(* Hence r=0 *)
solHom1 = Series[
  bhom1 /. r->0 /. First@Solve[
    Series[deqSub1 /. r-> 0, {yy,0,10}][[3]]==0
  ],
  {yy, 0, 10}
]

bsol1[0] = ParticularSolution[By10[[1]] a[0], yy, solHom1] + solHom1;
bsol1[0] = bsol1[0] /. First@Solve[Normal[bsol1[0]] == Normal[bsol0[0]] /. yy -> -1 - 2 y /. y -> -1/6];

(* For the next orders, the inhomogeneities are B_{1,0} a_{k-1} + B_{2,0} b_{k-1} + B_{1,1} a_k *)
bsol1[1] = ParticularSolution[By11.{a[k-1], bsol1[k-1]} + By10[[1]] a[k] /. k -> 1 /. asol, yy, solHom1] + solHom1;
bsol1[1] = bsol1[1] /. First@Solve[Normal[bsol1[1]] == Normal[bsol0[1]] /. yy -> -1 - 2 y /. y -> -1/6];
bsol1[2] = ParticularSolution[By11.{a[k-1], bsol1[k-1]} + By10[[1]] a[k] /. k -> 2 /. asol, yy, solHom1] + solHom1;
bsol1[2] = bsol1[2] /. First@Solve[Normal[bsol1[2]] == Normal[bsol0[2]] /. yy -> -1 - 2 y /. y -> -1/6];
bsol1[3] = ParticularSolution[By11.{a[k-1], bsol1[k-1]} + By10[[1]] a[k] /. k -> 3 /. asol, yy, solHom1] + solHom1;
bsol1[3] = bsol1[3] /. First@Solve[Normal[bsol1[3]] == Normal[bsol0[3]] /. yy -> -1 - 2 y /. y -> -1/6];
bsol1[4] = ParticularSolution[By11.{a[k-1], bsol1[k-1]} + By10[[1]] a[k] /. k -> 4 /. asol, yy, solHom1] + solHom1;
bsol1[4] = bsol1[4] /. First@Solve[Normal[bsol1[4]] == Normal[bsol0[4]] /. yy -> -1 - 2 y /. y -> -1/6];

mysol1 = {
  SeriesData[ep, 0, a /@ Range[0, 4] /. asol, 0, 5, 1],
  SeriesData[ep, 0, bsol1 /@ Range[0, 4], 0, 5, 1]
};
(*####################################################}}}*)
(* Expansion around y=-1{{{*)
By2 = By d[y, yyy] /. y -> -1 - yyy /. d -> D;
By20 = Coefficient[By2[[2]], ep, 0];
By21 = Coefficient[By2[[2]], ep, 1];

bhom2 = yyy^r Sum[yyy^i c[i], {i, 0, 15}];
deqSub2 = D[bhom2, yyy] - By20[[2]] bhom2;
Series[deqSub2, {yyy,0,0}]
(* Hence r=0 *)
solHom2 = Series[
  bhom2 /. r->0 /. First@Solve[
    Series[deqSub2 /. r-> 0, {yyy,0,10}][[3]]==0
  ],
  {yyy, 0, 10}
]

bsol2[0] = ParticularSolution[By20[[1]] a[0], yyy, solHom2] + solHom2;
bsol2[0] = bsol2[0] /. First@Solve[Normal[bsol2[0]] == Normal[bsol0[0]] /. yyy -> -1 - 2 y /. y -> -1/6];

(* For the next orders, the inhomogeneities are B_{1,0} a_{k-1} + B_{2,0} b_{k-1} + B_{1,1} a_k *)
bsol2[1] = ParticularSolution[By21.{a[k-1], bsol2[k-1]} + By20[[1]] a[k] /. k -> 1 /. asol, yyy, solHom2] + solHom2;
bsol2[1] = bsol2[1] /. First@Solve[Normal[bsol2[1]] == Normal[bsol1[1]] /. yyy -> -1-y /. yy -> -1-2y /. y -> -5/6];
bsol2[2] = ParticularSolution[By21.{a[k-1], bsol2[k-1]} + By20[[1]] a[k] /. k -> 2 /. asol, yyy, solHom2] + solHom2;
bsol2[2] = bsol2[2] /. First@Solve[Normal[bsol2[2]] == Normal[bsol1[2]] /. yyy -> -1-y /. yy -> -1-2y /. y -> -5/6];
bsol2[3] = ParticularSolution[By21.{a[k-1], bsol2[k-1]} + By20[[1]] a[k] /. k -> 3 /. asol, yyy, solHom2] + solHom2;
bsol2[3] = bsol2[3] /. First@Solve[Normal[bsol2[3]] == Normal[bsol1[3]] /. yyy -> -1-y /. yy -> -1-2y /. y -> -5/6];
bsol2[4] = ParticularSolution[By21.{a[k-1], bsol2[k-1]} + By20[[1]] a[k] /. k -> 4 /. asol, yyy, solHom2] + solHom2;
bsol2[4] = bsol2[4] /. First@Solve[Normal[bsol2[4]] == Normal[bsol1[4]] /. yyy -> -1-y /. yy -> -1-2y /. y -> -5/6];

mysol2 = {
  SeriesData[ep, 0, a /@ Range[0, 4] /. asol, 0, 5, 1],
  SeriesData[ep, 0, bsol2 /@ Range[0, 4], 0, 5, 1]
};
(*####################################################}}}*)
(*###############################################################}}}*)
