GammaResidue[expr_,{z_,pole_}]:=Assuming[
  Element[n,Integers] && n>=0,
  Residue[expr,{z,pole}]
]/.n!-> Gamma[n+1]
