(* ::Package:: *)

BeginPackage["MBPackage`PSLQ`"];


MyPSLQ::usage="MyPSLQ[query,basis,prec:100] is a wrapper for PSLQ. Given a real number \
(or rule, list or SeriesData) it fits the elements of basis using the PSLQ algorithm";


PSLQ::usage="Implementation of the PSLQ algorithm.
PSLQ[list, prec] finds integers \!\(\*SubscriptBox[\(a\), \(i\)]\) \
s.t. \!\(
  \*SubscriptBox[a, 1]\*SubscriptBox[\(list\), 1] +
  \*SubscriptBox[a, 2]\*SubscriptBox[\(list\), 2] +
  \[CenterEllipsis]
  \*SubscriptBox[a, n]\*SubscriptBox[\(list\), n]=0
\) up to precision prec.";


Begin["`Private`"]


(* This builds a matrix D such that D.H is diagonal with diag(D.H) = diag(H) *)
ClearAll[DiagonalN,HermiteStep,HermiteD];
DiagonalN[n_,k_,D_,H_]:=Table[-Sum[D[[i,i-j]]Round[H[[i-j,i-k]]/H[[i-k,i-k]]],{j,0,k-1}],{i,1+k,n}]
HermiteStep[H_,D_,k_]:=D+DiagonalMatrix[DiagonalN[Length[H],k,D,H],-k]
HermiteStep[H_]:=HermiteStep[H,#1,#2]&
HermiteD[H_,n_Integer]:=Fold[HermiteStep[H],IdentityMatrix[n],Range[n-1]]
HermiteD[H_]:=HermiteD[H,Length[H]]


H[x_]:=Module[{Hgen,s,n},
  n=Length[x];
  s=Reverse[Sqrt@Accumulate[Reverse[x]^2]];
  Hgen[i_,i_]:=s[[i+1]]/s[[i]];
  Hgen[i_,j_]/;i>j:=-x[[j]]x[[i]] / s[[j]] / s[[j+1]];
  Hgen[i_,j_]/;i<j:=0;
  Array[Hgen,{n,n-1}]
]


SwapRow[{A___,ar_,ar1_,B___},r_]/;Length[{A}]==r-1:={A,ar1,ar,B}


r[H_]:=First@Ordering[Abs[(2/Sqrt[3])^Range[Length[H]-1] Diagonal[H]],-1]
r[<|___,H->Hi_,___|>]:=r[Hi]


P[n_,r_,a_,b_]:=Module[{Pmat},
  Pmat=IdentityMatrix[n-1];
  Pmat[[r;;r+1,r;;r+1]]={
    {a,-b},
    {b,a}
  }//Map[Normalize];
  Pmat
]


(* This uses the triangular inversion algorithm             *
 * Given a lower triangular matrix                          *
 *    T = 1 + N                                             *
 * One can show that N^n = 0. Hence,                        *
 *    1 = 1-(-N)^n = (1-(-N))(1+(-N)+(-N)^2+...+(-N)^(n-1)) *
 * In other words,                                          *
 *    T^-1 = 1+(-N)+(-N)^2+...+(-N)^(n-1)                   *
 *          = Sum[ (-1)^k N^k, {k,0,n-1} ]                  *)
DInverse[N_,n_]:=Total[NestList[-Dot[N,#]&,IdentityMatrix[n],n]]
DInverse[D_]:=DInverse[D-IdentityMatrix[Length[D]],Length[D]]


InitState[x0_]:=Module[{H0,D,Dinv},
  H0=H[x0];
  D=HermiteD[H0];
  Dinv=DInverse[D];
  <|
    H->Dot[D,H0],
    x->Dot[x0,Dinv],
    A->D,
    B->Dinv
  |>
]


Step1[<|H->H0_,x->x0_,A->A0_,B->B0_|>,r_]:=<|
  H->SwapRow[H0,r],
  x->SwapRow[x0,r],
  A->SwapRow[A0,r],
  B->Transpose@SwapRow[Transpose[B0],r]
|>
Step1[state_]:=Step1[state,r[state]]


Step2[<|A___,H->H1_,B___|>,<|___,H->H0_,___|>,r_]/;r<Length[H1]-1:=<|
  A,
  H->Dot[H1,P[Length[H0],r,H0[[r+1,r]],H0[[r+1,r+1]]]],
  B
|>
Step2[<|A___,H->H1_,B___|>,_,r_]/;r==Length[H1]-1:=<|A,H->H1,B|>
Step2[state1_,state0_]:=Step2[state1,state0,r[state0]]
(* Currying *)
Step2[state0_]:=Step2[#,state0]&


Step3[<|A___,H->H0_,B___|>]/;Min@Abs@Diagonal[H0]==0:=$Failed
Step3[<|H->H0_,x->x0_,A->A0_,B->B0_|>]:=Module[{D,Dinv},
  D=HermiteD[H0];
  Dinv=DInverse[D];
  <|H->Dot[D,H0],x->Dot[x0,Dinv],A->Dot[D,A0],B->Dot[B0,Dinv]|>
]


Goodness[H0_,x0_]:=Min[Abs[Union[x0,Diagonal[H0]]]]
Goodness[<|A___,H->H0_,B___,x->x0_,C___|>]:=Goodness[H0,x0]
Goodness[$Failed]:=0;


Finish[<|___,x->x0_,___,B->B0_,___|>]:=B0[[;;,First[Ordering[x0,1]]]]
Finish[$Failed]:=$Failed


Step[state_]:=Step3@Step2[state]@Step1@state


PSLQ[x_, prec_,n___]:=Finish@NestWhile[Step,InitState@Normalize[x],Goodness[#]>10^(-prec+5)&,1,n]


MyPSLQ[query_Real,basis_List,prec_:100] := Module[{out},
  out=PSLQ[Prepend[N[basis,prec],query],prec];
  If[FailureQ[out],Return[$Failed]];
  out=out[[2;;]]/-out[[1]];
  Print["Matching ",query->N[Plus@@(out basis),prec]," \[CapitalDelta]=",(query-(Plus@@(out basis)))/query];
  Plus@@(out basis)
]

MyPSLQ[x_->q_Real,basis_List,prec_:100]:=x->MyPSLQ[q,basis,prec]
MyPSLQ[list_List,basis_List,prec_:100]:=MyPSLQ[#,basis,prec]&/@list
MyPSLQ[HoldPattern@SeriesData[x_,x0_,list_List,nmin_,nmax_,den_],basis_List,prec_:100]:=(
  SeriesData[x,x0,MyPSLQ[list,basis,prec],nmin,nmax,den]
)


(* ::Code:: *)
(*inx = Prepend[Zeta/@{2,3,4}, 449Zeta[2]/347 + 311 Zeta[3] / 307 + 479 * Zeta[4] / 467];*)
(**)
(*Print[PSLQ[N[inx,55],55]=={-49749043, 64372681, 50397239, 51027391}];*)


End[];


EndPackage[];
