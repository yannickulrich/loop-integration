function cat(L1, L2) = [for(L=[L1, L2], a=L) a];
function eulermatZ(A) = [
    [ cos(A), -sin(A), 0, 0],
    [ sin(A),  cos(A), 0, 0],
    [   0   ,   0    , 1, 0],
    [   0   ,   0    , 0, 1]
];
function eulermatY(B) = [
    [ cos(B),  0, sin(B), 0],
    [     0 ,  1, 0     , 0],
    [-sin(B),  0, cos(B), 0],
    [   0   ,   0    , 0, 1]
];
// Let us consider the massless Sudakov form factor
//   1/(k-p)^2 1/(k-q)^2 1/k^2
// with p^2=q^2=m^2 << (p+q)^2. We have
//    U = x1+x2+x3
//    F = s x1 x2 - m^2 x1 x3 - m^2 x2 x3
// The point clouds are
pointsU = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]];
pointsF = [[1, 1, 0, 0], [1, 0, 1, 2], [0, 1, 1, 2]];
// The Cayley cloud constructs U*F
cayley = [ for (u = pointsU, f = pointsF) (u+f)/2 ];
// This matrix rotates the Cayley surface to align with the axis
mat = eulermatZ(atan(sqrt(2)))*eulermatY(45);
cayley2 = [ for (c = cayley) (mat*c - [0,sqrt(3)/2,0,0]) ];

scaleLam = .1;
// Let's project out the dead dimension
cayleyX = cat(
    [ for (c = cayley2) [c[0], scaleLam*c[3],c[2], ] ],
    [ for (c = cayley2) [c[0], scaleLam     ,c[2], ] ]
);

facesBow = [
    [3,6, 0],
    [7,1,0,6],
    [8,7, 6],
    [3,5,8,6],
];
facesStern = [
    [5,3,0,1],
    [7,8,5,1],
];

facesSternSolid = [
    [9,0,1],
    [0,9,12,3],
    [5,3,12],
    [9,10,7,8,5,12]
];

facesBoat = cat(facesBow, facesStern);
facesSolid = cat(facesBow, facesSternSolid);

cloud = [ for (d = [ for (c = cat(pointsU, pointsF)) mat*c - [0,sqrt(3)/2,0,0] ]) [d[0], d[1], d[2]]];

module boat () {
        scale(0.85)
            polyhedron(
                points=cayleyX,
                faces=facesBoat
            );
}

module UFPrism () {
    walls = [
        [0,1,2], /* U */
        [3,4,5], /* F */
        [0,3,1],
        [0,4,3],
        [0,2,4],
        [2,5,4],
        [1,5,2],
        [1,3,5],
    ];
    polyhedron(cloud, faces=walls);
}

module pins(s) {
    pinR = 0.015*s;
    pinH = 0.04*s;
    for(theta=[0,120,240]) {
        translate([0.56*cos(theta),pinH,0.56*sin(theta)])
            rotate([90,0,0])
                cylinder(pinH, pinR, pinR, $fn=100);
    }
}

module upper () {
    difference() {
        UFPrism();

        translate([0,-1,0])
        cube(2, center=true);
        translate([0,-0.001,0])
        pins(1.1);
    }
}

module lower () {
    union() {
        difference() {
            UFPrism();
            translate([0,1,0])
                cube(2, center=true);
            scale(0.85)
                translate([0,-scaleLam+0.001,0])
                    polyhedron(
                        points=cayleyX,
                        faces=facesSolid
                    );
        }
        pins(1);
    }
}

module axes() {
    difference() {
        translate([-0.025,-0.025,-0.025]) union () {
            cube([1.5,0.05,0.05]);
            cube([0.05,1.5,0.05]);
            cube([0.05,0.05,1.5]);
        };
        translate([0.5,0.5,0.5]) multmatrix(eulermatY(-45)*eulermatZ(-atan(sqrt(2)))) {
            UFPrism();
        }
        translate([0,0,0.999])cube([0.05,0.05,1]);
    }
}

echo(part);
translate([0.5,0.5,0.5]) multmatrix(eulermatY(-45)*eulermatZ(-atan(sqrt(2)))) {
    if(part == undef || part == "U") {
        translate([0,3*$t,0]) upper();
    }
    if(part == undef || part == "I") {
        translate([0,2*$t,0]) boat();
    }
    if(part == undef || part == "L") {
        translate([0,$t,0]) lower();
    }
}

if(part == undef || part == "A") {
    axes();
}
