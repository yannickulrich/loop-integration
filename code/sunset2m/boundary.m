{
    SeriesData[ep, 0, {
        1,
        0,
        2*Zeta[2],
        0,
        6*Zeta[4]
    }, 0, 5, 1],
    SeriesData[ep, 0, {
        + 3*Zeta[2]/8
        + 1/4
        ,
        + 21*Zeta[3]/16
        - 9*Log[2]*Zeta[2]/4
        + 9*Zeta[2]/8
        - 1/2
        ,
        - 63*Zeta[4]/16
        + 9*PolyLog[4, 1/2]
        + 9*Log[2]^2*Zeta[2]/2
        + 3*Log[2]^4/8
        + 63*Zeta[3]/16
        - 27*Log[2]*Zeta[2]/4
        + Zeta[2]/2
        + 1
        ,
        - 1395*Zeta[5]/32
        + 54*PolyLog[5, 1/2]
        - 9*Zeta[2]*Zeta[3]/8
        + 189*Log[2]*Zeta[4]/8
        - 9*Log[2]^3*Zeta[2]
        - 9*Log[2]^5/20
        - 189*Zeta[4]/16
        + 27*PolyLog[4, 1/2]
        + 27*Log[2]^2*Zeta[2]/2
        + 9*Log[2]^4/8
        - Zeta[2]
        - 2
    }, 0, 4, 1]
}
