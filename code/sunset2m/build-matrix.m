deq = Get["deq.m"] /. INT[__, {a__}] :> sunset[a];

masters = {
    sunset[2, 2, 0, 0, 0],
    sunset[2, 0, 2, 0, 0],
    sunset[2, 1, 1, 0, 0],
    sunset[1, 1, 2, 0, 0]
};

Am1 = Normal@Last@CoefficientArrays[
    der[#, m1]& /@ masters /. deq,
    masters
];
Am2 = Normal@Last@CoefficientArrays[
    der[#, m2]& /@ masters /. deq,
    masters
];

(* Check scaling *)
Print@Simplify[Am1 m1 + Am2 m2];
T = m2^(d-4) ep^-2 DiagonalMatrix[(m2/m1)^{2, 1, 2, 1/2}];

{Bm1, Bm2} = Simplify[MapThread[
    Inverse[T] . #1 . T - Inverse[T] . D[T, #2] /. d -> 2 - 2ep &,
    {{Am1, Am2}, {m1, m2}}
]];
Print@Simplify[Bm1 m1 + Bm2 m2];

Bz = Simplify[Bm2 d[m2, z] /. m2 -> m1 z^2 /. d -> D, z>0];

Quiet[CreateDirectory["mat"]];
Put[Coefficient[Bz, ep, 0], "mat/dz_0.m"];
Put[Coefficient[Bz, ep, 1], "mat/dz_1.m"];
