nmax = 20;

M0 = D[1-x,x] Get["mat/dz_0.m"][[3;;,3;;]] /. z->1-x;

Mmats = Simplify[NestList[
    D[#, x] + # . M0 &,
    IdentityMatrix[2],
    2
]];
Mbar = Mmats[[;; 2, 1]];
Mtild = Mmats[[;; 3, 1]];

CP = First@NullSpace[Transpose[Mtild]]; CP /= CP[[-1]];

ansatz[r_] := x^r (Sum[x^i a[i], {i,0,nmax}] + O[x]^(nmax+1))

(* This solves the i-th order of the DE described by CP *)
SolveOrder[CP_List][rels_, i_] := Module[{eq, p},
    p = Length[CP] - 1;
    eq = Sum[
        FullSimplify[
            (a[j] c[k, i - j + k - p] Gamma[1 + j + r])/Gamma[1 + j - k + r]
        ], {k, 0, p}, {j, 0, i + k - p + 2}
    ];
    eq = FullSimplify[
        eq /. c[a_, j_] :> SeriesCoefficient[CP[[a + 1]], {x, 0, j}] /. rels
    ];
    Print[eq];
    Flatten[{Solve[eq == 0], rels}]
]

(* The first order allows for r=0 and r=-1 *)
Print[SolveOrder[CP][{a[0] -> 1}, 0]];

(* We now build the solutions for r=0 *)
solh1 = Fold[SolveOrder[CP], {r -> 0, a[1] -> 1}, Range[nmax]];

(* The next step is the reduction of order. This functions builds us a new CP *)
ReductionOfOrder[CP_List, f0 : (_Times | _SeriesData | _Plus), p_Integer] := Table[
    Sum[
        Binomial[p - n, 1 + j] CP[[-n + p + 1]] D[f0, {x, p - n - j - 1}],
        {n, 0, p - 1 - j}
    ],
    {j, 0, p - 1}
]
ReductionOfOrder[CP_List, {f0__Rule}, p_Integer] := ReductionOfOrder[CP, ansatz[r] /. {f0}, p]

(* solh1 can be written as 1/(1-x) *)

CP2 = Simplify[ReductionOfOrder[CP, a[0] / (1-x), 2]]; CP2 /= CP2[[-1]];

(* We now have r = -2 *)
solh2prime = Fold[SolveOrder[CP2], {a[0] -> 1}, Range[0, nmax]];

(* The full 2nd solution is *)
solh2 = (ansatz[0] /. solh1) Integrate[ansatz[-2] /. solh2prime, x];

(* To build the full answer, construct the Wronskian *)
WronskianMat = {
    {ansatz[0] /. solh1, solh2},
    D[{ansatz[0] /. solh1, solh2}, x]
};

solF = Simplify[Inverse[Mbar] . WronskianMat];
solF >> solf.m
