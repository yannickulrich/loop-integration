# Calculation of the two-mass sunset

In [Sheet 6](https://yannickulrich.gitlab.io/loop-integration/sheet6.pdf) we consider the two-mass sunset integral

![A sunset with two masses](https://gitlab.com/yannickulrich/loop-integration/-/raw/root/code/sunset2m/diag.svg){height=100px}

We begin by constructing the differential equations using [reduze](https://reduze.hepforge.org/)
```shell
$ reduze jobs.yaml
```
This will produce `deq.m`
```mathematica
{
der[INT["sunset",2,3,4,0,{2,2,0,0,0}],m1] -> 
  INT["sunset",2,3,4,0,{2,2,0,0,0}] *
      (m1^(-1)*(-4+d)),

der[INT["sunset",2,3,4,0,{2,2,0,0,0}],m2] -> 0,

scaling[INT["sunset",2,3,4,0,{2,2,0,0,0}]] -> 
  INT["sunset",2,3,4,0,{2,2,0,0,0}] *
      (-8+2*d),

der[INT["sunset",2,5,4,0,{2,0,2,0,0}],m1] -> 
  INT["sunset",2,5,4,0,{2,0,2,0,0}] *
      (1/2*m1^(-1)*(-4+d)),

der[INT["sunset",2,5,4,0,{2,0,2,0,0}],m2] -> 
  INT["sunset",2,5,4,0,{2,0,2,0,0}] *
      (1/2*m2^(-1)*(-4+d)),

scaling[INT["sunset",2,5,4,0,{2,0,2,0,0}]] -> 
  INT["sunset",2,5,4,0,{2,0,2,0,0}] *
      (-8+2*d),
...
}
```
We then build the differential equation matrix *A<sub>z</sub>*
```shell
$ math < build-matrix.m
```
This will create two files `mat/dz_0.m` and `mat/dz_1.m` that correspond to the *&epsilon;<sup>0</sup>* and *&epsilon;<sup>1</sup>* part respectively.
```mathematica
(* dz_0.m *)
{{0, 0, 0, 0}, {0, 0, 0, 0}, {(z - z^3)^(-1), -(z - z^3)^(-1), z^2/(z - z^3), 
  1/(z^2*(-1 + z^2))}, {z^2/(-1 + z^2), (1 - z^2)^(-1), z^2/(-1 + z^2), 
  z^2/(z - z^3)}}
(* dz_1.m *)
{{4/z, 0, 0, 0}, {0, 2/z, 0, 0}, {0, 0, (6 - 4*z^2)/(z - z^3), 
  2/(z^2*(-1 + z^2))}, {0, 0, (2*z^2)/(-1 + z^2), 2/(z - z^3)}}
```
We can now construct the homogeneous solution around *z=1*
```shell
$ math < homogenous-solution.m
```
which creates `solf.m`
```mathematica
{{SeriesData[x, 0, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
    1, 1}, 0, 21, 1], SeriesData[x, 0, {-1, -1 + Log[x], -1/4 + Log[x], 
    Log[x], 5/48 + Log[x], 29/192 + Log[x], 83/480 + Log[x], 11/60 + Log[x], 
    5063/26880 + Log[x], 41029/215040 + Log[x], 123857/645120 + Log[x], 
    24847/129024 + Log[x], 547453/2838528 + Log[x], 
    2191429/11354112 + Log[x], 7124743/36900864 + Log[x], 
    3563015/18450432 + Log[x], 570133451/2952069120 + Log[x], 
    9122540621/47233105920 + Log[x], 155086613977/802962800640 + Log[x], 
    155088315677/802962800640 + Log[x], 184168379933/953518325760 + Log[x]}, 
   -1, 20, 1]}, {SeriesData[x, 0, {1}, 0, 21, 1], 
  SeriesData[x, 0, {1, -1 + Log[x], 1/4, 0, -1/48, -1/64, -3/320, -1/192, 
    -5/1792, -3/2048, -7/9216, -1/2560, -9/45056, -5/49152, -11/212992, 
    -3/114688, -13/983040, -7/1048576, -15/4456448, -1/589824, -17/19922944}, 
   -1, 20, 1]}}
```
And finally calculate the full solution
```shell
$ math < full-solution.m
                            2             3        4
{1 - 2.77259 ep + 7.13349 ep  - 12.6737 ep  + O[ep] , 
 
                               2             3        4
>   1 - 1.38629 ep + 4.25077 ep  - 5.00476 ep  + O[ep] , 
 
                                     2             3        4
>   1.35818 - 2.29744 ep + 6.54748 ep  - 8.88948 ep  + O[ep] , 
 
                                      2             3        4
>   0.46699 - 0.553964 ep + 1.81783 ep  - 1.85911 ep  + O[ep] }
```
This already printed the solution at *z=0.5*.
