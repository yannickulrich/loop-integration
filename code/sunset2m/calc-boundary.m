<<../UF.m
{U,F}=Expand[UF[{k1,k2},{k1^2 - m1, k2^2 - m1, (k1 + k2 + p)^2 - m2}]]/.p^2->m2//Factor;
int = PowerExpand[
    (-1)^(2 ep) Gamma[1 - ep]^2 Gamma[2 + 2 ep] x[1]  U^(1 + 3ep)/F^(2 + 2 ep)
] /. m1|m2 -> 1;

A = x[3]^2;
B = U;

intMB = int /. (A+B)^lam_ -> 1/(2Pi) A^sig B^(lam-sig) Gamma[-sig]Gamma[sig-lam]/Gamma[-lam];
intMBdone = Integrate[
        Integrate[intMB, {x[3], 0, Infinity}, GenerateConditions->False]
        /. a_^n_ :>Factor[a]^n // PowerExpand,
    {x[2], 0, Infinity}, GenerateConditions->False] /.x[1]->1;

s0 = -1/3;
prec = 70;
integrands = Re[Series[intMBdone, {ep, 0, 2}][[3]]] /. sig -> s0 + x I;
intN = SetPrecision[NIntegrate[
    integrands,
    {x, -Infinity, Infinity},
    WorkingPrecision -> 2 prec, PrecisionGoal -> prec, AccuracyGoal -> prec
], prec];

basis = {
    1,
    Zeta[2],
    Zeta[2] Log[2], Zeta[3],
    Log[2]^4, Zeta[4], PolyLog[4, 1/2], Zeta[2] Log[2]^2
};

Reconstruct[{r0_Integer, rels__Integer}] := Total[{rels}/r0 basis]
Reconstruct[r_Real] := Reconstruct@FindIntegerNullVector[Prepend[basis, r]]

boundary = SeriesData[ep,0,Reconstruct/@intN, 0,3,1]
