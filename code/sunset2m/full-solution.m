solF = Get["solf.m"];
invF = Simplify[Inverse[solF]];

(* The first set of integrals is just *)
Do[
    sub1sol[k] = SeriesCoefficient[{
        z^(4 ep)Gamma[1 - ep]^2 Gamma[1 + ep]^2,
        z^(2 ep)Gamma[1 - ep]^2 Gamma[1 + ep]^2
    }, {ep, 0, k}],
    {k, -1, 3}
];

M1 = -Get["mat/dz_1.m"][[3;;,3;;]] /. z->1-x;
Minh = -Get["mat/dz_0.m"][[3;;,;;2]] /. z->1-x;

Inhomogeneity[k_] := 1/2 (Minh.sub1sol[k] + M1.sub2sol[k - 1]) /. z->1-x;

boundary = Get["boundary.m"][[2]];

(* Let's write a function to quickly match the boundary condition *)
MatchBoundary[
    HoldPattern@{
        SeriesData[x, 0, {p1_, f1_, __}, -1, _, 1],
        SeriesData[x, 0, {p2_, f2_, __}, -1, _, 1]
    }, {b1_, b2_}
] := First@Solve[p1 == p2 == 0 && f1 == b1 && f2 == b2]
MatchBoundary[
    HoldPattern@{
        SeriesData[x, 0, {f1_, __}, 0, _, 1],
        SeriesData[x, 0, {f2_, __}, 0, _, 1]
    }, {b1_, b2_}
] := First@Solve[f1 == b1 && f2 == b2]

ApplyBoundary[expr_, k_] := expr /. MatchBoundary[expr, {SeriesCoefficient[boundary,k], SeriesCoefficient[boundary, k]}];


sub2sol[-1] = {0,0};
Table[
  sub2sol[k] = Simplify@ApplyBoundary[
      Total /@ Simplify[solF.(
          Integrate[invF.Inhomogeneity[k], x] + DiagonalMatrix[{C[1], C[2]}]
      )],
      k
  ],
  {k, 0, 3}
];

SeriesData[ep,0,#,0,4,1]&/@Thread[sub2sol/@Range[0,3]] >> sol.m
Print[Join[
  SeriesData[ep,0,Normal[#]/.z->1-x/.x->0.5,0,4,1]&/@Thread[sub1sol/@Range[0,3]],
  SeriesData[ep,0,Normal[#]/.z->1-x/.x->0.5,0,4,1]&/@Thread[sub2sol/@Range[0,3]]
]];
