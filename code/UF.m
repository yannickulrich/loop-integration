(* This function takes D_i and U_i == \lambda_1 * ... * \lambda_i and
   returns D_{i+1} and U_{i+1} *)
UFstep[{di_, ui_}, k_] := Module[{t0, t1, t2},
    t0 = Coefficient[di, k, 0];
    t1 = Coefficient[di, k, 1];
    t2 = Coefficient[di, k, 2];

    {t0 - t1*t1 / (4*t2), ui * t2}
]

(* This functions takes a list of loop momenta and propagators and
   folds UFstep over it *)
UF[ks_, props_] := Module[{d0, dl, ul},
    d0 = Total[props x /@ Range[Length[props]]];
    {dl, ul} = Fold[UFstep, {d0, 1}, ks];
    {ul, dl ul}
]
