(* ::Package:: *)

(* ::Subsection:: *)
(*Integral definition*)


(* ::Text:: *)
(*To calculate the one-loop massless bubble with auxiliary mass flow, we consider the family*)


family = {k^2-eta, (k-p)^2-eta};


(* ::Text:: *)
(*Our two master integrals are*)


masters = {int[{1,0}], int[{1,1}]};


(* ::Subsection:: *)
(*Auxiliary mass flow*)


(* ::Text:: *)
(*The differential equation matrix is*)


matrix = {{(1-ep)/eta, 0}, {-(2-2ep)/eta/(4eta-1), 2(1-2ep)/(4eta-1)}};


(* ::Text:: *)
(*Our poles are at*)


poles = {0, 1/4};


(* ::Text:: *)
(*To find the steps, we need eta0 and etaN*)


Rratio = 2;
validpoles=Select[poles,#=!=0&];
eta0=-I Rratio Max[Abs@validpoles];
etaN=-I/Rratio Min[Abs@validpoles];


(* ::Text:: *)
(*The step size is determined by solving ((R-1)/R)^n eta0 = etaN*)


Nsteps = Floor[n] /. First@Solve[
    ((Rratio-1)/Rratio)^n eta0 == etaN,
    n,
    Assumptions->n>0
]


(* ::Text:: *)
(*We can now actually write down the steps*)


steps = DeleteDuplicates@Append[
    ((Rratio-1)/Rratio)^Range[0, Nsteps] eta0,
    etaN
]


Rratio


nmax=50;


ClearAll[c1,c2,d1,d2]


(* ::Subsubsection:: *)
(*Matching at infinity*)


(* ::Text:: *)
(*The boundary condition is*)


a={1,0}; b=-1;
{c1[0],c2[0]} = {-Gamma[1-ep]Gamma[ep]/(ep-1), Gamma[1-ep]Gamma[ep]};


(* ::Text:: *)
(*Our ansatz is therefore*)


ansatzInf=eta^(a+ep b) {
  SeriesData[eta,Infinity,c1[#]&/@Range[0,nmax],0,nmax+1,1],
  SeriesData[eta,Infinity,c2[#]&/@Range[0,nmax],0,nmax+1,1]
};


(* ::Text:: *)
(*We can now solve the c[i]*)


deqInf=Expand[eta^ep(D[ansatzInf,eta]-matrix . ansatzInf)];
Set@@@Simplify[First@Solve[deqInf[[;;,3]]==0,Join[c1/@Range[1,nmax],c2/@Range[1,nmax]]]];


(* ::Text:: *)
(*For the notes, we need to pretty-print the first three values*)


{c1[0],c2[0]}=={-Gamma[1-ep]Gamma[-1+ep], Gamma[1-ep]Gamma[ep]}//FullSimplify
Series[{c1[#],c2[#]}&/@{1,2,3},{ep,0,1}]
%=={{0,1/6},{0,1/60+1/60 ep},{0,1/420+1/280 ep}}+O[ep]^2//Simplify


(* ::Text:: *)
(*Evaluating at the first value eta=-I/2*)


sol0=Chop@N[Series[Normal[ansatzInf]/.eta->steps[[1]],{ep,0,1}]]
Chop[%[[2]]-(
  1/ep+(0.632066 + 1.88709I)+(0.0555556 + 1.18759I)ep
),10^-5]


(* ::Subsubsection:: *)
(*Matching at -I/2*)


ClearAll[d1,d2]


(* ::Text:: *)
(*We can initialise the boundary conditions from the previous step*)


d1[0,-1]=SeriesCoefficient[sol0[[1]],-1];
d1[0,0]=SeriesCoefficient[sol0[[1]],0];
d1[0,1]=SeriesCoefficient[sol0[[1]],1];
d2[0,-1]=SeriesCoefficient[sol0[[2]],-1];
d2[0,0]=SeriesCoefficient[sol0[[2]],0];
d2[0,1]=SeriesCoefficient[sol0[[2]],1];


(* ::Text:: *)
(*and write down an ansatz to solve*)


ansatz1={
  SeriesData[eta,steps[[1]],d1[#,-1]/ep+d1[#,0]+d1[#,1]ep+O[ep]^2&/@Range[0,nmax],0,nmax+1,1],
  SeriesData[eta,steps[[1]],d2[#,-1]/ep+d2[#,0]+d2[#,1]ep+O[ep]^2&/@Range[0,nmax],0,nmax+1,1]
};


Set@@@First@Solve[(D[ansatz1,eta]-matrix . ansatz1)[[;;,3,;;,3]]==0];


(* ::Text:: *)
(*Comparing with the notes,*)


Chop[{
  d2[1,-1]/ep+d2[1,0]+d2[1,1]ep-((0.5714 - 1.77538I) + (3.68432 - 0.0178779I)ep),
  d2[2,-1]/ep+d2[2,0]+d2[2,1]ep-(-(1.39587 + 0.983636I) - (0.388233 + 4.17847I)ep)
}, 10^-5]


(* ::Text:: *)
(*We evaluate at the next step*)


sol1=Normal[ansatz1]/.eta->steps[[2]]//Simplify;
Chop[sol1[[2]]-(
  1/ep + (1.18638 + 2.13174I) + (0.0584251 + 2.50302I)ep
),10^-5]


(* ::Subsubsection:: *)
(*Matching at -I/4*)


ClearAll[e1,e2]


{
  {e1[0,-1],e2[0,-1]},
  {e1[0,0],e2[0,0]},
  {e1[0,1],e2[0,1]}
}= Coefficient[sol1,ep,#]&/@{-1,0,1};


ansatz2={
  SeriesData[eta,steps[[2]],e1[#,-1]/ep+e1[#,0]+e1[#,1]ep+O[ep]^2&/@Range[0,nmax],0,nmax+1,1],
  SeriesData[eta,steps[[2]],e2[#,-1]/ep+e2[#,0]+e2[#,1]ep+O[ep]^2&/@Range[0,nmax],0,nmax+1,1]
};


Set@@@First@Solve[(D[ansatz2,eta]-matrix . ansatz2)[[;;,3,;;,3]]==0];


Chop[{
  e2[1,-1]/ep+e2[1,0]+e2[1,1]ep-((1.63896 - 2.76086I) + (7.62454 + 0.251576I)ep),
  e2[2,-1]/ep+e2[2,0]+e2[2,1]ep-(- (2.56095 + 4.19991I) + (3.18517 - 14.0005I)ep)
}, 10^-4]


sol2=Normal[ansatz2]+O[ep]^2/.eta->steps[[3]]//Simplify;
Chop[sol2[[2]]-(
  1/ep + (1.57145 + 2.42639I) - (0.0708639 - 3.73778I)ep
),10^-5]


(* ::Subsubsection:: *)
(*Matching at 0*)


Mtild[i_]:=SeriesCoefficient[eta matrix,{eta,0,i}];


lim=Expand[MatrixExp[Mtild[0] Log[eta]]]/.eta^a_->0;


pansatz={{P11,P12},{P21,P22}};


eqp[n_]:=n pansatz+pansatz . Mtild[0] - Mtild[0] . pansatz - Sum[Mtild[n-k] . P[k],{k,0,n-1}]


ClearAll[P]
P[0]=IdentityMatrix[2];
Do[
  P[i]=pansatz/.First@Solve[eqp[i]==0,Flatten[pansatz]],
  {i,1,nmax}
];


Chop[lim . Inverse[Sum[P[i]etaN^i,{i,0,nmax}] . MatrixExp[Log[etaN] Mtild[0]]] . sol2]
