(* vim: set foldmethod=marker *)
(* Init{{{*)
Get["~/Cs/MBPackage/MBPackage.wl"]
ILimit = ReplaceAll;
(*###############################################################}}}*)
(* Problem 1{{{*)
(* a{{{*)
para1a = x1^alpha x2^beta x3^gamma (x2 x3+x1 x2+x1 x3)^delta;
para1a1 = GammaIntegrate[para1a, x1]//.Powrep;
para1a12 = GammaIntegrate[para1a1, x2] //. Powrep;
(*#####################################################}}}*)
(* b{{{*)
para1b = (
    x1^alpha
    (x2 x3 + x2 x4 + x3 x4)^beta
    (x1 x3 + x2 x3 + x1 x4 + x2 x4 + x3 x4)^gamma
);

para1bdone = GammaIntegrate[
    GammaIntegrate[
        GammaIntegrate[para1b, x1],
        x2
    ]//.Powrep, x3
]/.x4->1;

(*#####################################################}}}*)
(* c{{{*)
c = Series[
    para1bdone/.{
        alpha->-2ep, beta->-1-2ep, gamma->-1+3ep
    },
    {ep,0,0}
]
(*#####################################################}}}*)
(*###############################################################}}}*)
(* Problem 2{{{*)
GetPara[props_,onshell_]:=GetParamRep[
    Thread[{props,1}],
    onshell
] /. Subscript[x, n_] :> ToExpression["x" <> ToString[n]]
GetPara[props_, 1] := GetPara[props, {p^2->-ss}];
GetPara[props_, 2] := GetPara[props, {p^2->0, q^2->0, p q->-ss/2}];
(* a{{{*)
para2a = GetPara[{k1^2, (k1+p)^2, (k1-q)^2}, 2];
para2a2 = Simplify[para2a//.Powrep];
res2a = GammaIntegrate[GammaIntegrate[para2a2,x1],x2]/.x3->1;
(*#####################################################}}}*)
(* b{{{*)
para2b = GetPara[{k1^2, k2^2, (k1-k2-p)^2}, 1];
para2b2 = Simplify[para2b//.Powrep];
res2b = Simplify[
    GammaIntegrate[GammaIntegrate[para2b2/.x2->1,x1],x3]
]
(*#####################################################}}}*)
(* c{{{*)
para2c = GetPara[{k2^2, (k1+k2)^2, (k1+p)^2, (k1-q)^2}, 2];
para2c2 = Simplify[Simplify[para2c]//.Powrep] /. {
  a_^n_ :> Expand[a]^Expand[n]
};
res2c = GammaIntegrate[GammaIntegrate[GammaIntegrate[para2c2, x3],x4]/.x1->1,x2];
(*#####################################################}}}*)
(* d{{{*)
para2d = GetPara[{k1^2, (k1+p)^2, (k1+k2+p)^2, k2^2}, 1];
para2d2 = Simplify[Simplify[para2d]//.Powrep] /. {
  a_^n_ :> Expand[a]^Expand[n]
};
res2d = GammaIntegrate[GammaIntegrate[GammaIntegrate[para2d2/.x4->1, x1],x2],x3];
(*#####################################################}}}*)
(*###############################################################}}}*)
(* Problem 3{{{*)
(* a{{{*)
para3a = GetPara[{k1^2, k2^2, k3^2, (k1-k2-k3-p)^2}, 1];
para3a2 = Simplify[Simplify[para3a]//.Powrep] /. {
  a_^n_ :> Expand[a]^Expand[n]
};
res3a = Simplify[
    GammaIntegrate[GammaIntegrate[GammaIntegrate[para3a2,x1]//.Powrep,x2]//.Powrep,x3]/.x4->1
]
(*#####################################################}}}*)
(* b{{{*)
LoopMom[l_] := ToExpression["k"<>ToString[#]]&/@Range[l]
Param[l_] := GetPara[Append[LoopMom[l], p+Total[LoopMom[l]]]^2,1]//.Powrep
para2b / Param[2]//.Powrep
para3a / Param[3]//.Powrep

ProdPow = {
    Product[a_, b_]^c_ :> Product[a^c, b],
    HoldPattern[Product[a_, b_] Product[c_, b_]] :> Product[a c, b]
};
Prod1 = {
    HoldPattern@Product[a_, {i, 1, 1 + l}] :>
        Product[a, {i, 1, l}] ReplaceAll[a, i -> l + 1]
};

rules = {
    U[1] :> x[1]+x[2],
    U[l_] :> Product[x[i],{i,1,l}] + U[l-1] x[l+1],
    F[l_] :> Product[x[i],{i,1,l+1}]
};

para3b = (
    (-1)^r Gamma[1 - \[Epsilon]]^l Gamma[r - l d/2]
    U[l]^( r - (l + 1) d/2)/F[l]^(r - l d/2)
)/.{
    r -> l+1, d->4-2\[Epsilon]
}//Simplify;

para3b/.rules[[-1]]/.ProdPow

para3b2 = para3b/.rules/.ProdPow/.Prod1/.x[1+l]->y;
para3b3 = Simplify[
    GammaIntegrate[para3b2,y]//.ProdPow/.Product->p
]/.p->Product;

rel = para3b3 / ( para3b/.l->l-1/.rules[[-1]]  /. ProdPow ) /. {
    Product->p
}//Simplify;

int[1] = Simplify[GammaIntegrate[Param[1],x1]]/.x2->1/.ss->1
int[ll_]:=ReplaceAll[rel,l->ll] int[ll-1]

int[2] / res2b /. ss->1//Simplify
int[3] / res3a /. ss->1//Simplify

A[l_] := Gamma[1 - l (1 - \[Epsilon])]/ Gamma[(l + 1) (1 - \[Epsilon])]

rel == Gamma[1 - \[Epsilon]]^2 (-A[l])/A[l - 1] // Simplify

sol[l_] := Gamma[1 - \[Epsilon]]^(2l-2) (-1)^(l+1) A[l]/A[1] int[1]

Simplify[sol[#]/int[#]]& /@ Range[10]
(*#####################################################}}}*)
(*###############################################################}}}*)
