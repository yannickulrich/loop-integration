(* vim: set foldmethod=marker *)
(* Init{{{*)
Get["~/Cs/MBPackage/MBPackage.wl"]
ILimit = ReplaceAll;
(*###############################################################}}}*)
(* The integral{{{*)
(** Part 1: integration{{{*)
(* Let us consider the following two-loop integral *)
props = {k1^2, k1^2 + 2k1 p, k1^2 - 2k1 q};

para = GetParamRep[
    Thread[{props,{a,1,1}}],
    {p^2->m^2, q^2->m^2, p q->s/2-m^2}
] /. {
  Subscript[x, n_] :> ToExpression["x"<>ToString[n]]
} /. {
  a_^n_ :> Collect[a, m|s, Factor]^Simplify[n]
};

para1 = GammaIntegrate[para, x1];

(* We now use MB to split the scales *)
para1MB = ApplyMB[para1, A1_ s + A2_] /. {
  a_^n_ :> Factor[a]^n
} // PowerExpand;

(* We can now set \delta(...) = \delta(1-x_3) and solve the x3 integral *)
para13MB = para1MB /. x3 -> 1 /. {
  a_^n_ :> Collect[a, x2, Factor]^Simplify[n]
};
(* ... and solve x2 *)
para123MB = Simplify[GammaIntegrate[para13MB,x2]];
(*#####################################################}}}*)
(** Part 2: residues{{{*)
para123n = Simplify[
    GammaResidue[para123MB, {z1, n}],
    Element[a, Integers] && Element[n, Integers] && m>0
]

done = Sum[para123n, {n, 0, Infinity}];
(*#####################################################}}}*)
(** Part 2: resolve{{{*)
res = MBmerge@MBshiftContours[
    MBresolve[para123MB/.a->0,{\[Epsilon]->0}],
    {z1->1/2}
];
resep = res/.MBint[a_,b_]:>MBint[Normal[Series[a,{\[Epsilon],0,0}]],b];
(*#####################################################}}}*)
(** checks{{{*)
para == (
    (-1)^a (Gamma[1-ep] Gamma[a+ep])/Gamma[a] x1^(a-1) (x1+x2+x3)^(-2+a+2ep) / (m^2 x2^2 + (2m^2-s)x2 x3+m^2 x3^2)^(a+ep)
)/.ep->\[Epsilon]//Simplify

para1 == (
    (-1)^a Gamma[1-ep] Gamma[2-2a-2ep]Gamma[a+ep]/Gamma[2-a-2ep]
    (x2+x3)^(-2+2a+2ep) / (m^2(x2+x3)^2 -s x2 x3)^(a+ep)
)/.ep->\[Epsilon]//Simplify

para1MB == (
    (-1)^a(m^2)^(-a-ep) Gamma[1-ep]Gamma[2-2a-2ep]/Gamma[2-a-2ep]
        (-s/m^2)^z1 Gamma[-z1]Gamma[a+ep+z1] x2^z1 x3^z1 / (x2+x3)^(2+2z1)
)/.ep->\[Epsilon]//PowerExpand//Simplify

para123MB == (
    (-1)^a(m^2)^(-a-ep) Gamma[1-ep]Gamma[2-2a-2ep]/Gamma[2-a-2ep]
        (-s/m^2)^z1 Gamma[-z1]Gamma[a+ep+z1] Gamma[1+z1]^2/Gamma[2+2z1]
)/.ep->\[Epsilon]//PowerExpand//Simplify

Simplify[para123n == (
    (-1)^(a+1)(m^2)^(-a-ep)
    Gamma[1-ep]Gamma[2-2a-2ep]/Gamma[2-a-2ep]
    (s/m^2)^n
    Gamma[1+n]Gamma[a+n+ep]/Gamma[2+2n]
)/.ep->\[Epsilon], Element[a, Integers] && Element[n, Integers] && m>0]

Simplify[done == (
    (-1)^(a+1)(m^2)^(-a-ep) Gamma[1-ep]Gamma[2-2a-2ep]/Gamma[2-a-2ep]
        Gamma[a+ep] Hypergeometric2F1[1,a+ep, 3/2, s/4/m^2]
)/.ep->\[Epsilon], m>0]

(*#####################################################}}}*)
(*###############################################################}}}*)
(* The contrived example integral{{{*)
(** Part 1: integration{{{*)
para = GetParamRep[
  {
    {k1^2, 1},
    {(k1-p1)^2, 1},
    {(k1+p2)^2, 1},
    {(p1-k1-p4)^2, 1}
  },
  {
    p1^2 -> s,
    p2^2 -> 0,
    p3^2 -> s,
    p4^2 -> 0,
    p1 p2 -> 0,
    p3 p4 -> 0,
    p1 p3 -> s/2,
    p2 p4 ->-s/2,
    p1 p4 -> s/2,
    p2 p3 -> s/2
  } /. s->-ss
] /. {
  Subscript[x, n_] :> ToExpression["x"<>ToString[n]]
} /. {
  a_^n_ :> Factor[a]^Simplify[n]
} //. Powrep /. {
  a_^n_ :> Collect[a, x3]^n
};

paraMB = ApplyMB[para, A1_ x3 + A2_] //. Powrep;
para3 = GammaIntegrate[paraMB, x3];
para13 = GammaIntegrate[para3, x1];
para123 = GammaIntegrate[para13, x2];
para1234 = para123 /. x4 -> 1 // Simplify;
(*#####################################################}}}*)
(** Part 2: Barnes Lemma{{{*)
res = MBshiftContours[MBresolve[para1234, {\[Epsilon]->0}]];
res[[1,1]] == -Residue[para1234, {z1, -1-\[Epsilon]}]//Simplify

resEp = res /. {
  MBint[a_, b_]:>MBint[Normal[
    Series[ss^(2+\[Epsilon]) a,{\[Epsilon], 0, 1}]
  ],b]
} /. PolyGammaRep // Simplify;

resBarnesed = DoAllBarnes[resEp]/.PolyGammaRep//MBmerge//Simplify;

resBarnesed2 = resBarnesed /.{
  Gamma[-1 - z1]^2 Gamma[2 + z1] -> -Gamma[1 + z1] Gamma[-z1]Gamma[-1 - z1]
}
(*#####################################################}}}*)
(** Part 3: PSLQ{{{*)
resEp2 = Table[
  res[[2]] /. {
    MBint[a_, b_]:>MBint[Normal[
      SeriesCoefficient[ss^(2+\[Epsilon]) a,{\[Epsilon], 0, i}]
    ],b]
  } /. PolyGammaRep // Simplify,
  {i,0,4}
];

resEp2Num = SetPrecision[MyMBIntegrate[
  resEp2,
  PrecisionGoal -> 50, WorkingPrecision -> 100,  AccuracyGoal -> 50
], 50];

MyPSLQ[resEp2Num, {
  Zeta[2],
  Zeta[3],
  Zeta[4],
  Zeta[5],Zeta[2]Zeta[3],
  Zeta[6],Zeta[3]Zeta[3]
}]

(*#####################################################}}}*)
(** Part 3: PSLQ v2{{{*)
integrand=Gamma[1-ep]*Gamma[-ep]*Gamma[-1-ep-z]^2*Gamma[-z]*Gamma[1+z]*Gamma[2+ep+z]/Gamma[-2*ep]/Gamma[-ep-z];

integrandC = integrand/.{
  z -> -1/2 + I * v
};

num = Table[
  NIntegrate[
    SeriesCoefficient[
      integrandC,
      {ep,0,i},
    ],
    {v,-Infinity, Infinity},
    WorkingPrecision -> 100,
    PrecisionGoal -> 50,
    AccuracyGoal -> 50
  ],
  {i, 0, 4}
];
MyPSLQ[num, {
  Zeta[2],
  Zeta[3],
  Zeta[4],
  Zeta[5],Zeta[2]Zeta[3],
  Zeta[6],Zeta[3]Zeta[3]
}]
(*#####################################################}}}*)
(*###############################################################}}}*)
