#!/bin/sh
set -e

if [[ "$1" == "clean" ]]; then
    rm -f lecture-notes/*.{bib,log,aux,blg,out,toc,bbl,dvi,pdf}
    rm -f sheets/*.{log,out,aux}
    rm -f sheets/s*.pdf
    rm -rf public/
    exit 0
fi

if [ ! -f .git/refs/heads/root ]; then
    echo $CI_COMMIT_SHA > .git/refs/heads/root
fi

(
    cd lecture-notes
    pdflatex notes.tex
    cat *.aux | bibgen.sh > notes.bib
    cat extra.bib >> notes.bib
    bibtex notes
    pdflatex notes.tex
    pdflatex notes.tex
)

(
    cd sheets
    for i in sheet*.tex ; do
        pdflatex --shell-escape $i
        pdflatex --shell-escape $i
    done
)

rm -rf public/
mkdir public/
cp lecture-notes/notes.pdf public/
cp code/parameter.html public/
cp code/*stl public/
cp sheets/sheet*.pdf public/


cat README.md \
    | grep "/loop-integration/s" \
    | sed "s/.*\(s[hetol]*[0-9]\.pdf\).*/\1/g" \
    | while read f ; do
        cp sheets/$f public/
    done

cat <<EOF > public/index.html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style type="text/css">
            html * {
                font-family: Georgia, sans-serif;
            }
            #content{
                max-width: 35rem;
                margin: auto auto 2rem;
                padding: 1rem 1rem 0px;
            }
            #content a{
                text-decoration: none;
                border-bottom: 1px dotted rgb(0, 75, 107);
                color: rgb(0, 75, 107);
            }
            h1,h2,h3,h4,h5,h6 {
                font-weight: normal;
                margin: 30px 0px 10px 0px;
                padding: 0px;
                text-align: left;
            }

            h1 { font-size: 240%; margin-top: 0px; padding-top: 0px; }
            h2 { font-size: 180%; }

            td:first-child,th:first-child {
                border-left: none;
            }
            td, th {
                border-left: solid 1px;
                padding: 4px;
            }
            th {
                border-bottom: solid 1px;
            }
            table {
                border-collapse: collapse;
            }


        </style>
        <title>Practical guide to analytic loop integration</title>
    </head>
    <body>
        <div id="content">
EOF

markdown < README.md >> public/index.html

cat <<EOF >> public/index.html
        </div>
    </body>
</html>
EOF
