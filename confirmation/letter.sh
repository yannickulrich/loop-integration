#!/bin/bash

cat > /tmp/letter.tex << \EOF
\documentclass[12pt,a4paper]{article}
\usepackage[left=2.7cm,top=3.5cm,bottom=1.5cm,right=2.7cm]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}
\newcommand\email[1]{\href{mailto:#1}{\tt #1}}

\begin{document}
\thispagestyle{empty}

\setlength\parindent{0pt}
\setlength\parskip{0.7em}
\refstepcounter{section}
\sffamily
\begin{minipage}[t][8em]{0.8\textwidth}
{\Large Durham University}
\\[1em]
Dr Yannick Ulrich\\[0.2em]
Institute of Particle Physics Phenomenology\\[0.2em]
Durham University, South Road, Durham\\[0.2em]
United Kingdom\\[0.2em]
email: \email{yannick.ulrich@durham.ac.uk}
\end{minipage}\begin{minipage}{0.2\textwidth}
\includegraphics[width=\linewidth]{dur}
\end{minipage}
\\[6em]

{\bf To whom it may concern}

This is to confirm that
\begin{center}
\bf
\name\ (\affil)
\end{center}
participated in the hybrid course \emph{Practical guide to analytic loop integration} held at the University of Durham between 13 May -- 20 May 2022.


Yours sincerely,\\

\includegraphics[height=2em]{signature}

Yannick Ulrich\\
(course lecturer)
\end{document}
EOF

while IFS="|" read name affil fn ;
do
    (
        echo "\\def\\name{$name}\\def\\affil{$affil}" ;
        cat /tmp/letter.tex ;
    ) | pdflatex -jobname pgali-$fn
    rm pgali-$fn.{aux,log,out}
done < names.txt
